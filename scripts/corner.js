/**
* @class Corner
*
* @param	Object		parentdiv		"Elemento para asociar corner"
* @return	this									"Retorna el objeto"
*/
﻿function Corner(parentdiv, boundary)
{
	addTrackFunction("corner","Corner");
	if(document.getElementById('corner')) {document.getElementById('corner').parentNode.removeChild(document.getElementById('corner'))};
	this.elmRef = document.createElement('div');
	this.elmRef.id  = 'corner'+boundary.id;
	this.elmRef.className ='resizeElement';
	this.elmRef.style.position='absolute';
	this.elmRef.style.left= (/*parseInt(boundary.style.left) +*/ parseInt(boundary.style.width)-MINSTEP)+"px";
	this.elmRef.style.top= (/*parseInt(boundary.style.top) +*/ parseInt(boundary.style.height)-MINSTEP)+"px";
	this.elmRef.style.cursor='nw-resize';
	this.elmRef.style.fontSize=0;
	this.elmRef.style.width=10+"px";
	this.elmRef.style.height=10+"px";
	this.elmRef.style.zIndex=1000;
	this.elmRef.parentdiv=parentdiv;
	this.elmRef.style.backgroundColor="white";
	this.elmRef.style.border="solid black 1px";
	this.elmRef.parentdiv.appendChild(this.elmRef);
	return this.elmRef;
}
