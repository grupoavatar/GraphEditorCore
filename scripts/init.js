﻿ieb=navigator.appName=='Microsoft Internet Explorer';
iop=navigator.appName=='Opera';

squote="'";
dquote='"';

var gdmrks=0;
var rdmrks=0;
var currentColor = Colors.ColorFromRGB(64,128,128);
var alphaperct=0;
var coltype='F';
var xgrid=1;
var ygrid=1;
var undo=[];

var DRAWING = {};
var SELECTOR_CONST = 15;
var DRAWING_SHAPE = -1;

var midx={x:0,y:0};
var spritecentre;

var MINIVECT={xs:0, xe:100,ys:0, ye:0}; //vector direction of sprite vector
var SHAPES={};  // list of shapes
var SELECTED={}; //list of selected shapes and groups;
var SELECTEDSHAPE; //shape clicked on
var DELETED={}; //list of deleted shapes and groups;
var DELETES=[]; //array of DELETED
var GROUPS={}; //list of groups
var SCRW; //actual screen width
var SCRH; //actual screen height
var MCOUNT=0; //counter for id of marks
var BCOUNT=0; //counter for id of boundaries
var SCOUNT=0; //counter for shapes
var GCOUNT=0; //counter for groups
var ZPOS=1; //zindex for each shape as created or set to front
var ZNEG=-1; //zindex for shapes when zindex sent to back
var K=4*(Math.SQRT2-1)/3; //constant for circles using Bezier curve.
var ROTATIONRADIUS=100; //radius for rotation marker;
var DEFAULTKEYDOWN=document.onkeydown;
var BACKDROP; // object where backstage canvas is added for bezguides, gradient stops, rotation markers;
var BOXES; //array of class dialogue boxes;
var BOXNAME; //name of dialogue box, id with box dropped from end;
var DD=[]; //array of drag and drop elements for dialogue boxes;

var BARS=["right","bottom","rcorner"]; //array for edge classes;
var	ANIELS=["sc","sp","tr","tw","fm","ls","fl"];
var ED=[]; //array of drag and drop elements for element boxes;
var EDCOUNT=0; //counter for ED

var TRACKS = {};  //associative array for tracks
var SCENES = {}; //associative array for scenes
var SPRITES = {}; //associative array for sprites
var TWEENS = {}; //associative array for tweens
var FILMS = {}; //associative array for films

var NCOUNT=0; //counter for element shapes
var CURRENT=SHAPES; //pointer to current associative array of shapes;
var TWEENEDIT=false;
var CURRENTTWEEN; //current tween being edited;
var STOPCHECKING=false;
var ZBOX=10; //z-index for tool bars and dialogueboxes;
EXCANVASUSED = !document.createElement("canvas").getContext;
PNLTOP=0;
PNLLEFT=0;
PNLWIDTH=1000;
PNLHEIGHT=500;

INIANGLE = null;

if (typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function()
	{
		return this.replace(/^\s+|\s+$/g, '');
	}
}

if (!Array.indexOf)
{
	Array.prototype.indexOf = function(obj)
	{
		for (var i=0; i<this.length; i++)
		{
			if (this[i]==obj)
			{
				return i;
			}
		}
		return -1;
	}
}

String.prototype.repeat = str_rep;
var SPACES =" ".repeat(100);

/**
* @function str_rep
* Retorna cadena producto de repeticiones
* @param  Integer		n		"cantidad de repeticiones"
* @return	undefined			"No devuelve ningún valor"
*/
function str_rep(n)
{
	addTrackFunction("init","str_rep");
	var s = "", t = this.toString();
	while (--n >= 0)
	{
		s += t
	}
	return s;
}

/**
* @function setStage
* Asigna valor a las variables responsables de la dimensión del canvas
* @param  Integer		stagewidth		"tamaño del ancho"
* @param  Integer		stageheight		"tamaño de la altura"
* @return	undefined			"No devuelve ningún valor"
*/
function setStage(stagewidth,stageheight)
{
	addTrackFunction("init","setStage");
	var stages=["area"/*,"frame"*/];
	for (var i=0; i<stages.length; i++)
	{
		document.getElementById('stage'+stages[i]).style.visibility="visible";
	}
	document.getElementById('stagearea').style.visibility="visible";
	document.getElementById('shapestage').style.visibility="visible";
	PNLWIDTH = stagewidth;
	PNLHEIGHT = stageheight;

	SCRW=stagewidth;
	SCRH=stageheight;
}

/**
* @function toMoveEvent
* Evento estado 2 para seleccionar un shape
* Esta función solo se llama si es que ya existe un shape seleccionado
* Lo que ejecuta es capturar un evento y permitir que dibuje los limites o boundaries
* Y permita moverlos
* @return	undefined			"No devuelve ningún valor"
*/
function toMoveEvent(){
	document.getElementById("markerdrop").onmousemove=function(e) {
		addTrackFunction("init","markerdrop onmousemove2");
		// noBubble(e);
		checkToMove ( shiftdown(e),getPosition(e));
	}
	document.getElementById("frontmarkerdrop").onmousemove=function(e) {
		addTrackFunction("init","frontmarkerdrop onmousemove2");
		checkToMove(shiftdown(e),getPosition(e));
	}
	document.getElementById("boundarydrop").onmousemove=function(e) {
		addTrackFunction("init","boundarydrop onmousemove2");
		checkToMove(shiftdown(e),getPosition(e));
	}
}

/**
* @function clearAllMouseEvent
* Evento estado 3 para seleccionar un shape
* Esta función no permite elegir ningun shape
* solo se llama si es que el shape seleccionado
* se encuentra ejecutando alguna acción de arrastre o redimensionando
* @return	undefined			"No devuelve ningún valor"
*/
function clearAllMouseEvent(){
	document.getElementById("markerdrop").onmousemove=function(e) {}
	document.getElementById("frontmarkerdrop").onmousemove=function(e) {}
	document.getElementById("boundarydrop").onmousemove=function(e) {}	
}

/**
* @function checkToMove
* funcion del evento estado 2
* permite que dibuje los limites o boundaries y permita moverlos
* No se selecciona el shape
* @param		Bollean		shiftdown		"true: evento key shift activado"
* @param		Object		cursor			"Objeto con las coordenadas del cursor"
* @return		undefined		"No devuelve ningun valor"
*/
function checkToMove(shiftdown,cursor)
{
	addTrackFunction("init","checkToMove");
	var shape, foundshape;
	var shapefound=false;
	for (var name in CURRENT)
	{
		shape=CURRENT[name];
		if ((shape.tplftcrnr.x-4)<=cursor.x && cursor.x<=(shape.btmrgtcrnr.x+4) && (shape.tplftcrnr.y-4)<=cursor.y && cursor.y<=(shape.btmrgtcrnr.y+4))
		{
			if (!shapefound || (shapefound && shape.zIndex>foundshape.zIndex))
			{
				if (shape.open)
				{
					if (shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
				else
				{
					if (shape.isIn(cursor) || shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
			}
		}
	}
	if (shapefound)
	{
		if( foundshape.group == checkBoundary.last || 
				foundshape.group == checkToMove.last || 
				(drawEndSelector.isSelector && foundshape.group.name in drawEndSelector.SELECTED)|| 
				(shapecopy.isSelector && foundshape.group.name in shapecopy.SELECTED) ){
			// foundshape.group.drawBoundary();
			return;
		}
		if( checkToMove.last != undefined ){
			checkToMove.last.removeBoundary();
			checkToMove.last = undefined;
		}
		checkToMove.last = foundshape.group;
		// ungroup();
		if (shiftdown)
		{
			if (foundshape.group.name in SELECTED)
			{
				foundshape.group.removeBoundary();
				delete SELECTED[foundshape.group.name];
			}
			else
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				SELECTED[foundshape.group.name]=foundshape.group;
				SELECTEDSHAPE=foundshape;
			}
		}
		else
		{
			if (!(foundshape.group.name in SELECTED)) //if already drawn do not redraw
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				// SELECTED[foundshape.group.name]=foundshape.group;
				// SELECTEDSHAPE=foundshape;
			}
		}
	}else if( checkToMove.last != undefined ){
		checkToMove.last.removeBoundary();
		checkToMove.last = undefined;
	}
}
checkToMove.last = undefined;

/**
* @function toMoveEvent
* Evento estado 1 para seleccionar un shape
* Esta función se llama inicialmente
* Lo que ejecuta es capturar un evento y permitir que dibuje los limites o boundaries
* si los selecciona y permite moverlos
* @return	undefined			"No devuelve ningún valor"
*/
function addMouseEventToSelect(){
	// change
	ungroup();
	document.getElementById("markerdrop").onmousemove=function(e) {
		addTrackFunction("init","markerdrop onmousemove");
		noBubble(e);
		checkBoundary(shiftdown(e),getPosition(e));
	}
	document.getElementById("frontmarkerdrop").onmousemove=function(e) {
		addTrackFunction("init","frontmarkerdrop onmousemove");
		noBubble(e);
		checkBoundary(shiftdown(e),getPosition(e));
	}
	document.getElementById("boundarydrop").onmousemove=function(e) {
		addTrackFunction("init","boundarydrop onmousemove");
		noBubble(e);
		checkBoundary(shiftdown(e),getPosition(e));
	}
}

/**
* @function main
* Inicializa todos los eventos relacionados a los elementos del lienzo principal, además inicializa ciertos valores para la grilla.
* @return	undefined				"No devuelve ningún valor"
*/
function main()
{
	addTrackFunction("init","main");
	BODY=document.getElementsByTagName("body")[0];
	nuevos_cambios();
	keyDown();
	BACKDROP={};
	BACKDROP.zIndex=0;
	BACKDROP.addTo=addTo;
	BACKDROP.addTo(document.getElementById("backstage"));
	GRIDDROP={};
	GRIDDROP.zindex=0;
	GRIDDROP.width=30;
	GRIDDROP.height=30;
	GRIDDROP.addTo=addTo;
	GRIDDROP.addTo(document.getElementById("griddrop"));

	document.getElementById("griddrop").style.zIndex=0;
	document.getElementById("markerdrop").style.visibility="visible";
	// document.getElementById("markerdrop").onclick=function(e) {
	// 	addTrackFunction("init","markerdrop onclick");
	// 	noBubble(e);
	// 	checkBoundary(shiftdown(e),getPosition(e));
	// }
	// document.getElementById("frontmarkerdrop").onclick=function(e) {
	// 	addTrackFunction("init","frontmarkerdrop onclick");
	// 	noBubble(e);
	// 	checkBoundary(shiftdown(e),getPosition(e));
	// }
	// document.getElementById("boundarydrop").onclick=function(e) {
	// 	addTrackFunction("init","boundarydrop onclick");
	// 	noBubble(e);
	// 	checkBoundary(shiftdown(e),getPosition(e));
	// }
	document.getElementById("markerdrop").onmousedown=function(e) {
		addTrackFunction("init","markerdrop onmousedown");
		noBubble(e);
		checkSelector(shiftdown(e),getPosition(e));
	}
	document.getElementById("frontmarkerdrop").onmousedown=function(e) {
		addTrackFunction("init","frontmarkerdrop onmousedown");
		noBubble(e);
		checkSelector(shiftdown(e),getPosition(e));
	}
	document.getElementById("boundarydrop").onmousedown=function(e) {
		addTrackFunction("init","boundarydrop onmousedown");
		noBubble(e);
		checkSelector(shiftdown(e),getPosition(e));
	}
	addMouseEventToSelect();
	if (!ieb)
	{
		document.getElementsByTagName('body')[0].addEventListener('dragover', handleDragOver, false);
		document.getElementsByTagName('body')[0].addEventListener('drop', handleBodyDrop, false);
	}

	//rotation markers
	document.getElementById("rotateCentre").style.width=35+"px";
	document.getElementById("rotateCentre").style.height=35+"px";
	document.getElementById("rotateMove").style.width=35+"px";
	document.getElementById("rotateMove").style.height=35+"px";
	DDrtC=new YAHOO.util.DD("rotateCentre");
	DDrtC.onDrag=function()
	{updatecenter()};
	DDrtM=new YAHOO.util.DD("rotateMove");
	DDrtM.onDrag=function()
	{
		if (INIANGLE == null)
		{
			INIANGLE = Math.round(SELECTEDSHAPE.group.phi*180/Math.PI);
		}
		updateangle();
	};

	DDrtM.onMouseUp=function()
	{
		var actions = [];
		actions.push(new shapeRotateCommand(INIANGLE , Math.round(SELECTEDSHAPE.group.phi*180/Math.PI)));
		if (actions.length > 0 )
		{
			window.UndoRedo.executeCommand(actions);
		}
		INIANGLE = null;
	};

	document.getElementById("rotateCentre").onmouseover=function()
	{
		this.style.cursor="move";
		document.getElementById("frontmarkerdrop").onclick=function(e) {noBubble(e)};
	};
	document.getElementById("rotateCentre").onmouseout=function()
	{
		this.style.cursor="default";
		document.getElementById("frontmarkerdrop").onclick=function(e) {
			noBubble(e);
			checkBoundary(shiftdown(e),getPosition(e));
			BACKDROP.Canvas.ctx.clearRect(0,0,SCRW,SCRH);
			removeRotate();
			document.getElementById("frontmarkerdrop").style.visibility="hidden";
			document.getElementById("backstage").style.visibility="hidden";
			document.getElementById("boundarydrop").style.visibility="visible";
		}
		document.getElementById("frontmarkerdrop").onmousedown=function(e) {
			addTrackFunction("init","frontmarkerdrop onmousedown");
			noBubble(e);
			checkSelector(shiftdown(e),getPosition(e));
		}
	};

	document.getElementById("rotateMove").onmouseover=function()
	{
		this.style.cursor="move";
		document.getElementById("frontmarkerdrop").onclick=function(e) {noBubble(e)};
	};

	DDshslider=new YAHOO.util.DD('shslider');
	DDshslider.onDrag = function () {
		for (var groupName in SELECTED)
		{
			var group=SELECTED[groupName];
			var shapeNames=group.memberShapes();
			for (var name in shapeNames)
			{
				shape=shapeNames[name];
				shape.shadowBlur=parseInt(0);
				shape.draw();
			}
		}

	};

	DDeldrop=new YAHOO.util.DDTarget("eldrop","ELGROUP");
	DDtrackdrop=new YAHOO.util.DDTarget("trackdrop","TRGROUP");
	DDelfilmdrop=new YAHOO.util.DDTarget("filmbuildboard","ELGROUP");

	for (var i=0;i<ANIELS.length;i++)
	{
		for (var j=0;j<BARS.length;j++)
		{
			BOXNAME=BARS[j]+"bar"+ANIELS[i];
			ED[EDCOUNT]=new YAHOO.util.DD(BOXNAME);
			ED[EDCOUNT++].onDrag=function(e) {
				resize(this.id);
			}
		}
	}

	if (ieb)
	{
		DROPS=getElementsByClassName("theatre_style");
		for (var i=0; i<DROPS.length; i++)
		{
			DROPS[i].style.backgroundImage="url(assets/clear.png)";
		}
	}

	fixGradientImg();
}

/**
* @function nuevos_cambios
* @definir
* @return	undefined								"No devuelve ningún valor"
*/
function nuevos_cambios()
{
	addTrackFunction("init","nuevos_cambios");
	PNLTOP=0;
	PNLLEFT=0;

	var intervalSelector = setTimeout(function()
	{
		if (BACKDROP.Canvas)
		{
			clearInterval(intervalSelector);
		}
	},200);
}

/**
* @function isInputText
* Verifica si el shape seleccionado es un InputText y no esta en focus
* @param  Integer		stagewidth		"tamaño del ancho"
* @return	Boolean									"true: shape es InputText"
*/
function isInputText(members=null)
{
	if( members == null )
		members = SELECTED;
	var itHasText = false;
	for(var x in members){
		var element = members[x];
		if( element.elType == "_GROUP" ){
			if( itHasText = isInputText(element.members) ){
				break;
			}
		}else if( element.elType == "_SHAPE" ){
			if( ["text","text_italic","text_bold"].indexOf(element.type) >=0 ){
				itHasText = true;
				break;
			};
		}
	}
	return itHasText;
}

/**
* @function clear
* Remueve todos los elementos hijo
* @param  Object		stage		"Objeto padre"
* @return	undefined					"No devuelve ningún valor"
*/
function clear(stage)
{
	addTrackFunction("init","clear");
	while(stage.childNodes.length>0)
	{
		stage.removeChild(stage.firstChild)
	}
}

/**
* @function noBubble
* Interrumpe la secuencias de eventos
* @param  Object			e		"Evento"
* @return	undefined				"No devuelve ningún valor"
*/
function noBubble(e)
{
	addTrackFunction("init","noBubble");
	if (!e) var e = window.event;
	e.cancelBubble = true;
	if (e.stopPropagation) e.stopPropagation();
}

/**
* @function shiftdown
* Verifica si la tecla shift es presionada
* @param  Object			e		"Evento"
* @return	Boolean				"true: tecla shift es presionado"
*/
function shiftdown(e)
{
	addTrackFunction("init","shiftdown");

	e = e || window.event;
	return e.shiftKey;
}

/**
* @function shapeSelector
* Dispone el editor para dibujar el selector
* @param	Object			cursor		"Coordenadas del cursor"
* @return	undefined							"No devuelve ningún valor"
*/
function shapeSelector(cursor)
{
	var selector={};
	addTrackFunction("init","shapeSelector");
	var i=15;
	// var i=4;
	selector.open=shapes[i][0];
	selector.edit=shapes[i][1];
	selector.alt=shapes[i][2];
	selector.type=shapes[i][2];
	var shape=new Shape("Shape"+SCOUNT,"Shape"+(SCOUNT++),selector.open,selector.edit,selector.type,null);
	shape.group=new Group(GROUPS,"Group"+GCOUNT,"Group"+(GCOUNT++),shape);
	shape.zIndex=ZPOS++;
	document.getElementById("markerdrop").style.visibility="hidden";
	document.getElementById("frontmarkerdrop").style.visibility="hidden";
	clear(document.getElementById("markerdrop"));
	document.getElementById("boundarydrop").style.visibility="hidden";
	resetBoundary();
	BACKDROP.Canvas.ctx.clearRect(0,0,SCRW,SCRH);
	SELECTED={};
	BCOUNT=0;
	document.getElementById("backstage").style.visibility="hidden";

	document.getElementById("blockstage").style.visibility="hidden";
	shape.addTo(document.getElementById("shapestage"));
	shape.setPath(cursor);
}

/**
* @function getFlechaPoints
* Retorna los puntos para el dibujado de la flecha
* @param	Object		puntos		"Objeto con las coordenadas de 2 puntos"
* @return	Object							"Objeto con las coordenadas de cada punto de la flecha"
*/
function getFlechaPoints(puntos)
{
	addTrackFunction("init","getFlechaPoints");
	var distLastPoint = 10;
	var distMiddlePoint = 5;
	var x1 = puntos.point1.x;
	var y1 = puntos.point1.y;
	var x2 = puntos.point2.x;
	var y2 = puntos.point2.y;
	var difxs = x2-x1;
	var difys = y2-y1;
	var distancia = Math.sqrt( Math.pow(difxs,2) + Math.pow(difys,2) );
	if ( distancia!=0 )
	{
		var xmedio = x2 - distLastPoint*difxs/distancia;
		var ymedio = y2 - distLastPoint*difys/distancia;
		var x3 = xmedio + difys*distMiddlePoint/distancia;
		var y3 = ymedio - difxs*distMiddlePoint/distancia;
		var x4 = xmedio - difys*distMiddlePoint/distancia;
		var y4 = ymedio + difxs*distMiddlePoint/distancia;
	}else{
		var xmedio,ymedio,x3,y3,x4,y4;
		xmedio = x3 = x4 = x1;
		ymedio = y3 = y4 = y1;
	}
	return {
		pointmedio:{
			x:xmedio,
			y:ymedio
		},
		point1:{
			x:x1,
			y:y1
		},
		point2:{
			x:x2,
			y:y2
		},
		point3:{
			x:x3,
			y:y3
		},
		point4:{
			x:x4,
			y:y4
		}
	}
}

/**
* @function getCotaPoints
* Retorna los puntos para el dibujado de la cota
* @param	Object		puntos		"objeto con las coordenadas de 2 puntos"
* @return	Object							"Colección de puntos con sus coordenadas"
*/
function getCotaPoints(puntos)
{
	addTrackFunction("init","getCotaPoints");
	var distLastPoint = 10;
	var distMiddlePoint = 5;
	var x1 = puntos.point1.x;
	var y1 = puntos.point1.y;
	var x2 = puntos.point2.x;
	var y2 = puntos.point2.y;
	var difxs = x2-x1;
	var difys = y2-y1;
	var distancia = Math.sqrt( Math.pow(difxs,2) + Math.pow(difys,2) );
	if ( distancia!=0 )
	{
		var x3 = x1 + distMiddlePoint*difys/distancia;
		var y3 = y1 - distMiddlePoint*difxs/distancia;
		var x4 = x1 - distMiddlePoint*difys/distancia;
		var y4 = y1 + distMiddlePoint*difxs/distancia;

		var x5 = x2 + distMiddlePoint*difys/distancia;
		var y5 = y2 - distMiddlePoint*difxs/distancia;
		var x6 = x2 - distMiddlePoint*difys/distancia;
		var y6 = y2 + distMiddlePoint*difxs/distancia;
	}else{
		var x3,y3,x4,y4,x5,y5,x6,y6;
		x3 = x4 = x5 = x6 = x1;
		y3 = y4 = y5 = y6 = y1;
	}
	return {
		point1:{
			x:x1,
			y:y1
		},
		point2:{
			x:x2,
			y:y2
		},
		point3:{
			x:x3,
			y:y3
		},
		point4:{
			x:x4,
			y:y4
		},
		point5:{
			x:x5,
			y:y5
		},
		point6:{
			x:x6,
			y:y6
		}
	}
}

/**
* @function cancelEvent
* Cancela el evento
* @param  Object			e		"Evento"
* @return	undefined				"No devuelve ningún valor"
*/
function cancelEvent(e)
{
	addTrackFunction("init","cancelEvent");
	if (!e) e = window.event;
	if (e.preventDefault)
	{
		e.preventDefault();
	}
	else
	{
		e.returnValue = false;
	}
}

/**
* @function shapedelete
* Borra todos los Shapes seleccionados
* @return	undefined				"No devuelve ningún valor"
*/
function shapedelete()
{
	addTrackFunction("init","shapedelete");
	var i=0;
	for (var name in CURRENT)
	{
		i++;
	}
	if (i==1 && !(CURRENT===SHAPES))
	{
		return;
	}

	removeRotate();
	closeStops();
	var actions = [];
	for (var groupName in SELECTED)
	{
		DELETED[groupName]=SELECTED[groupName];
		var members=SELECTED[groupName].memberShapes();
		for (var shapeName in members)
		{
			var shape=members[shapeName];
			actions.push(new DeleteShapesCommand(shape));
		}

	}
	if (actions.length > 0 )
	{
		window.UndoRedo.executeCommand(actions);
	}
	clear(document.getElementById("markerdrop"));
	resetBoundary();
}

/**
* @function getPosition
* Retorna las coordenadas del cursor
* @param  Object		e		"Evento"
* @return	Object				"devuelve coordenadas del cursor"
*/
function getPosition(e) {
	e = e || window.event;
	var cursor = {x:0, y:0};
	if (e.clientX || e.clientY) {
		var de = document.getElementById("workArea");
		var viewportOffset = de.getBoundingClientRect();
		var top = viewportOffset.top;
		var left = viewportOffset.left;
		cursor.x = e.clientX-left;
		cursor.y = e.clientY-top;
	}
	else {
		var de = document.documentElement;
		var b = document.body;
		cursor.x = e.clientX +
		(de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
		cursor.y = e.clientY +
		(de.scrollTop || b.scrollTop) - (de.clientTop || 0);
	}
	return cursor;
}

/**
* @function drawGeneralShape
* Dispone al editor para inciar el dibujado
* @param  Integer		constShape		"Identificador de Shape"
* @return	undefined								"No devuelve ningún valor"
*/
function drawGeneralShape(constShape)
{
	addTrackFunction("init","drawGeneralShape");
	DRAWING_SHAPE = constShape;

	var shape=new Shape("Shape"+SCOUNT,"Shape"+(SCOUNT++),this.shapes[constShape][0],this.shapes[constShape][1],this.shapes[constShape][2],null);

	shape.group=new Group(GROUPS,"Group"+GCOUNT,"Group"+(GCOUNT++),shape);
	shape.zIndex=ZPOS++;
	document.getElementById("markerdrop").style.visibility="hidden";
	document.getElementById("frontmarkerdrop").style.visibility="hidden";
	clear(document.getElementById("markerdrop"));
	document.getElementById("boundarydrop").style.visibility="hidden";
	resetBoundary();
	BACKDROP.Canvas.ctx.clearRect(0,0,SCRW,SCRH);

	SELECTED={};
	BCOUNT=0;
	document.getElementById("backstage").style.visibility="hidden";
	document.getElementById("blockstage").style.visibility="hidden";
	shape.addTo(document.getElementById("shapestage"));

	if (shape.type == "selector") {
		shape.Canvas.style.cursor = "default";
	} else if (shape.type == "text" || shape.type == "text_italic" || shape.type == "text_bold") {
		shape.Canvas.style.cursor = "text";
	} else {
		shape.Canvas.style.cursor = "crosshair";
	}
	shape.Canvas.onmousedown=function(e)
	{
		if (e.button == 0)
		{
			this.shape.setPath(getPosition(e));
		}
	};
}

function editorAuthors()
{
	console.log(
						"Editor's Authors\n",
						"Renzo Segura Morales\n",
						"Neil Esteves Rosales\n",
						"Luis Fernández León\n",
						"Camila Chávez Aranguri\n",
						"Jhon King(Canvimation 2017)",
							);
}
