/**
* @function draw
* Limpia el canvas y redibuja el Shape
* @return		undefined		"No devuelve ningun valor"
*/
﻿function draw()
{
	addTrackFunction("draw","draw");

	this.Canvas.ctx.clearRect(-SCRW,-SCRH,2*SCRW,2*SCRH);
	
	var rule='rgba('
	for (var j=0;j<3;j++)
	{
		rule += this.strokeStyle[j]+',';
	}
	rule +=this.strokeStyle[j]+')';
	this.Canvas.ctx.strokeStyle=rule;
	this.Canvas.ctx.lineWidth = this.lineWidth;
	this.Canvas.ctx.lineCap = this.lineCap;
	this.Canvas.ctx.lineJoin = this.lineJoin;

	if(this.dotted){
		this.Canvas.ctx.setLineDash([this.linedasha, this.linedashb]);
	}
	else {
		this.Canvas.ctx.setLineDash([]);
	}

	this.Canvas.ctx.beginPath();
	var node=this.path.next;
	this.Canvas.ctx.moveTo(node.point.x,node.point.y);
	while (node.next.point.x !="end")
	{
		node=node.next;
		if (node.vertex=="L")
		{
			this.Canvas.ctx.lineTo(node.point.x,node.point.y);
		}
		else
		{
			this.Canvas.ctx.bezierCurveTo(node.ctrl1.x,node.ctrl1.y,node.ctrl2.x,node.ctrl2.y,node.point.x,node.point.y)
		}
	}
	if (!this.open)
	{
		this.Canvas.ctx.closePath()
	}

	if(this.type != "text" && this.type != "text_bold" && this.type != "text_italic"){
		this.Canvas.ctx.stroke();
		if (!this.open)
		{
			this.Canvas.ctx.shadowOffsetX = this.shadowOffsetX;
			this.Canvas.ctx.shadowOffsetY = this.shadowOffsetY;
			this.Canvas.ctx.shadowBlur = this.shadowBlur;
			var rule='rgba('
			for (var j=0;j<3;j++)
			{
				rule += this.shadowColor[j]+',';
			}
			rule +=this.shadowColor[j]+')';
			this.Canvas.ctx.shadowColor = rule;
			if (this.justfill)
			{
				var rule='rgba('
				for (var j=0;j<3;j++)
				{
					rule += this.fillStyle[j]+',';
				}
				rule +=this.fillStyle[j]+')';
				this.Canvas.ctx.fillStyle=rule;
			}
			else
			{
				if (this.linearfill)
				{
					var grad = this.Canvas.ctx.createLinearGradient(this.lineGrad[0],this.lineGrad[1],this.lineGrad[2],this.lineGrad[3]);
				}
				else
				{
					var grad = this.Canvas.ctx.createRadialGradient(this.radGrad[0],this.radGrad[1],this.radGrad[2],this.radGrad[3],this.radGrad[4],this.radGrad[5]);
				}
				var rule;
				for (var k=0; k<this.colorStops.length;k++)
				{
					rule='rgba('
					for (var j=1;j<4;j++)
					{
						rule += this.colorStops[k][j]+',';
					}
					rule +=this.colorStops[k][j]+')';
					grad.addColorStop(this.colorStops[k][0],rule);
				}
				this.Canvas.ctx.fillStyle=grad;
			}
			this.Canvas.ctx.fill();
		}
	}

	//Si es texto cambia posicionamiento, dimensiones y limpia el anterior input
	if(this.type == "text" || this.type == "text_bold" || this.type == "text_italic"){
		var self=this;
		this.textValue = (this.inputText)?this.inputText.value():"";
		this.widthInput = (Math.abs(this.btmrgtcrnr.x - this.tplftcrnr.x)>100)?Math.abs(this.btmrgtcrnr.x - this.tplftcrnr.x):100;
		this.inputText.width(this.widthInput);
		this.inputText.value(this.textValue);
		this.inputText.x(this.path.next.point.x);
		this.inputText.y(this.path.next.point.y);
		this.inputText.fontColor('rgba('+this.fillStyle[0]+','+this.fillStyle[1]+','+
			this.fillStyle[2]+','+this.fillStyle[3]+')');
		this.inputText.canvas().getContext('2d').clearRect(0, 0, PNLWIDTH, PNLHEIGHT);
		this.inputText.focus();
	}
}

/**
* @function drawBezGuides
* Dibuja los nodos point y ctrl del Shape
* @return		undefined		"No devuelve ningun valor"
*/
function drawBezGuides()
{
	addTrackFunction("draw","drawBezGuides");
	BACKDROP.Canvas.ctx.beginPath();
	BACKDROP.Canvas.ctx.clearRect(0,0,SCRW,SCRH);
	var node=this.path.next;
	node=node.next;
	while (node.point.x !="end")
	{
		if (node.vertex=="B")
		{
			BACKDROP.Canvas.ctx.moveTo(node.point.x,node.point.y);
			BACKDROP.Canvas.ctx.lineTo(node.ctrl2.x,node.ctrl2.y);
			BACKDROP.Canvas.ctx.moveTo(node.prev.point.x,node.prev.point.y);
			BACKDROP.Canvas.ctx.lineTo(node.ctrl1.x,node.ctrl1.y);
		}
		node=node.next;
	}
	BACKDROP.Canvas.ctx.lineWidth=2;
	BACKDROP.Canvas.ctx.strokeStyle='rgb(255,255,255)';
	BACKDROP.Canvas.ctx.stroke();
	BACKDROP.Canvas.ctx.beginPath();
	var node=this.path.next;
	node=node.next;
	while (node.point.x !="end")
	{
		if (node.vertex=="B")
		{
			BACKDROP.Canvas.ctx.moveTo(node.point.x,node.point.y);
			BACKDROP.Canvas.ctx.lineTo(node.ctrl2.x,node.ctrl2.y);
			BACKDROP.Canvas.ctx.moveTo(node.prev.point.x,node.prev.point.y);
			BACKDROP.Canvas.ctx.lineTo(node.ctrl1.x,node.ctrl1.y);
		}
		node=node.next;
	}
	BACKDROP.Canvas.ctx.lineWidth=1;
	BACKDROP.Canvas.ctx.strokeStyle='rgb(255,0,0)';
	BACKDROP.Canvas.ctx.stroke();
}
