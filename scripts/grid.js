/**
* @function gridOn
* Actualiza parametros de la grilla y llama al dibujado de ésta
* @param		Integer			width				"Valor de la distancia horizontal entre puntos visual de la grilla"
* @param		Integer			height			"Valor de la distancia vertical entre puntos visual de la grilla"
* @param		Array				gridColor		"Array con los colores para la grilla"
* @param		Integer			gridx				"Valor de la distancia horizontal entre puntos de alineación de la grilla"
* @param		Integer			gridy				"Valor de la distancia vertical entre puntos de alineación de la grilla"
* @return		undefined								"No devuelve ningun valor"
*/
function gridOn(width,height,gridColor,gridx,gridy)
{
	if(isNaN(width)) {width=30};
	if(isNaN(height)) {height=30};
	if(isNaN(gridx)) {gridx=5};
	if(isNaN(gridy)) {gridy=5};
	GRIDDROP.width=width;
	GRIDDROP.height=height;
	xgrid=gridx;
	ygrid=gridy;
	document.getElementById("griddrop").style.visibility="visible";
	drawgrid(gridColor);
}

/**
* @function gridOff
* Oculta la grilla
* @return		undefined			"No devuelve ningun valor"
*/
function gridOff()
{
	xgrid=1;
	ygrid=1;
	document.getElementById("griddrop").style.visibility="hidden";
}

/**
* @function drawgrid
* Dibuja la grilla
* @param		Array				gridColor		"Array con los colores para la grilla"
* @return		undefined								"No devuelve ningun valor"
*/
function drawgrid(gridColor)
{
	addTrackFunction("grid","drawgrid");
	GRIDDROP.Canvas.ctx.clearRect(0,0,SCRW,SCRH);
	GRIDDROP.Canvas.ctx.beginPath();

	GRIDDROP.Canvas.ctx.strokeStyle = 'rgba('+gridColor[0]+','+gridColor[1]+','+
	gridColor[2]+','+gridColor[3]+')';

	for (var x=0; x<SCRW; x +=GRIDDROP.width)
	{
		GRIDDROP.Canvas.ctx.moveTo(x,0);
		GRIDDROP.Canvas.ctx.lineTo(x,SCRH);
	}
	for (var y=0; y<SCRH; y +=GRIDDROP.height)
	{
		GRIDDROP.Canvas.ctx.moveTo(0,y);
		GRIDDROP.Canvas.ctx.lineTo(SCRW,y);
	}
	GRIDDROP.Canvas.ctx.stroke();
}
