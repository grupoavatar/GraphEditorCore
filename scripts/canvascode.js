/**
 * funcion para comprarar los Zindex's de dos figuras
 * se llama a la funcion para ordenar una lista de figuras de acuerdo al zindex
 * @param		Array			a		"nombre y zindex de figura a"
 * @param		Array			b		"nombre y zindex de figura b"
 * @return		Number                  "devuelve la diferencia entre los segundos elementos de cada arreglo"
 */
function compareZ(a,b)
{
	addTrackFunction("canvascode","compareZ");
	return a[1]-b[1];
}
