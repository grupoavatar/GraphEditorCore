﻿/**
* @function canvasBaseUrl
* Obtiene la imagen actual del lienzo en el formato PNG. Se pinta todas las imagenes en un canvas oculto, y luego se utiliza un metodo del canvas "toDataURL", que es propio del canvas de HTML
* @return		canvas.toDataURL()		"En el formato PNG, en base64-encoded image data de la imagen actual del lienzo"
*/
function canvasBaseUrl()
{
	addTrackFunction("export","canvasBaseUrl");
	var shapelist=[];
	var nameZ=[];
	for (var name in SHAPES)
	{
		nameZ=[];
		nameZ.push(name);
		nameZ.push(SHAPES[name].zIndex);
		shapelist.push(nameZ);
	}
	shapelist.sort(compareZ);

	var scriptText         = "";
	var scriptFunctionText = "";
	var boundariesPNG      = getBoundariestoBuildPNG();

	scriptFunctionText += SPACES.substr(0,9)+'function setcanvas(canvas)';
	scriptFunctionText += SPACES.substr(0,9)+'{';
	scriptFunctionText += SPACES.substr(0,12)+'if (canvas.getContext)';
	scriptFunctionText += SPACES.substr(0,12)+'{';
	scriptFunctionText += SPACES.substr(0,15)+'var ctx = canvas.getContext("2d");';
	scriptFunctionText += SPACES.substr(0,15)+'drawcanvas(ctx);';
	scriptFunctionText += SPACES.substr(0,12)+'}';
	scriptFunctionText += SPACES.substr(0,12)+'else';
	scriptFunctionText += SPACES.substr(0,12)+'{';
	scriptFunctionText += SPACES.substr(0,15)+'alert("Canvas NOT supported");';
	scriptFunctionText += SPACES.substr(0,12)+'}';
	scriptFunctionText += SPACES.substr(0,9)+'}';
	scriptFunctionText += SPACES.substr(0,9);
	scriptFunctionText += SPACES.substr(0,9)+'function drawcanvas(ctx)';
	scriptFunctionText += SPACES.substr(0,9)+'{';
	for (var i=0;i<shapelist.length;i++)
	{
		scriptFunctionText += exportshapetext(shapelist[i][0],boundariesPNG);
	}
	scriptFunctionText += ( SPACES.substr(0,15) + 'ctx.clearRect(0,0,'+(boundariesPNG.paddingX-5)+','+EDITOR_CANVAS.heightImage+');' + "\n" );
	scriptFunctionText += ( SPACES.substr(0,15) + 'ctx.clearRect(0,0,'+EDITOR_CANVAS.widthImage+','+(boundariesPNG.paddingY-5)+');' + "\n" );
	scriptFunctionText += ( SPACES.substr(0,15) + 'ctx.clearRect('+(EDITOR_CANVAS.widthImage-boundariesPNG.paddingX+5)+',0,'+(boundariesPNG.paddingX-5)+','+EDITOR_CANVAS.heightImage+');' + "\n" );
	scriptFunctionText += ( SPACES.substr(0,15) + 'ctx.clearRect(0,'+(EDITOR_CANVAS.heightImage-boundariesPNG.paddingY+5)+','+EDITOR_CANVAS.widthImage+','+(boundariesPNG.paddingY-5)+');' + "\n" );
	scriptFunctionText += SPACES.substr(0,9)+'}';
	var canvas = document.createElement('canvas');
	canvas.width  = EDITOR_CANVAS.widthImage;
	canvas.height = EDITOR_CANVAS.heightImage;
	canvas.style.zIndex   = 8;
	canvas.style.position = "absolute";

	var imageCanvas = document.getElementById("imageCanvasSavedLocal")
	imageCanvas.appendChild(canvas);

	eval(scriptFunctionText);
	setcanvas(canvas);
	return canvas.toDataURL();
}

/**
* @function saveFormatsImage
* Es la función que se utiliza para guardar el avance de la imagen, los cuales se guardan en formato png(base64-encoded) y en un formato texto, propio del proyecto
* @return		Object		formatosImagen		"Retorna 2 formatos del último avance, en los siguientes atributos canvasText(Texto) y toDataURL(base64-imagen-png)"
*/
function saveFormatsImage()
{
	var formatosImagen = {};
	ungroup();
	// Limpieza de figuras, que nunca se inizializaron
	for (var name in SHAPES)
	{
		shape=SHAPES[name];
		if( shape.tplftcrnr.x  == 0 &&
			shape.tplftcrnr.y  == 0 &&
			shape.btmrgtcrnr.x == 0 &&
			shape.btmrgtcrnr.y == 0 )
		{
			if( SHAPES[name].Canvas )
			{
				SHAPES[name].Canvas.remove();
				delete SHAPES[name].Canvas
			}
			if( SHAPES[name].group )
			{
				var group = GROUPS[SHAPES[name].group.name];
				delete GROUPS[ SHAPES[name].group.name ];
			}
			delete SHAPES[name];
		}
	}

	formatosImagen.canvasText = CanvasToText();
	formatosImagen.toDataURL = canvasBaseUrl();
	return formatosImagen;
}

function a(g,h)
{
	return 1;
}
