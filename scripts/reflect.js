/**
* @function fliphorz
* Da la vuelta al Shape, eje vertical
* @return		undefined					"No devuelve ningun valor"
*/
﻿function fliphorz()
{
	addTrackFunction("reflect","fliphorz");
	resetBoundary();
	var actions = [];
	for (var groupName in SELECTED)
	{
		var group=SELECTED[groupName];
		var mirrorY=group.left+group.width/2;
		var shapeNames=group.memberShapes();
		actions.push(new flipHorzCommand(shapeNames,group, mirrorY, Object.keys(SELECTED).length));
	}
	if (actions.length > 0)
	{
		window.UndoRedo.executeCommand(actions);
	}

	if (TWEENEDIT)
	{
		CURRENTTWEEN.translate.active=true;
		CURRENTTWEEN.setTweenTimeBox();
	}
}

/**
* @function flipvert
* Da la vuelta al Shape, eje horizontal
* @return		undefined					"No devuelve ningun valor"
*/
function flipvert()
{
	addTrackFunction("reflect","flipvert");
	resetBoundary();
	var actions = [];

	for (var groupName in SELECTED)
	{
		var group=SELECTED[groupName];
		var mirrorY=group.top+group.height/2;
		var shapeNames=group.memberShapes();
		actions.push(new flipVertCommand(shapeNames,group, mirrorY, Object.keys(SELECTED).length));

	}

	if (actions.length > 0)
	{
		window.UndoRedo.executeCommand(actions);
	}

	if (TWEENEDIT)
	{
		CURRENTTWEEN.translate.active=true;
		CURRENTTWEEN.setTweenTimeBox();
	}
}
