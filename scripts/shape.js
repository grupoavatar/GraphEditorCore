/**
* @function addTo
* Añade el canvas al objeto
* @param		Object		theatre		"objeto"
* @return		undefined						"No devuelve ningun valor"
*/
﻿function addTo(theatre)
{
	addTrackFunction("shape","addTo");
	this.Canvas = document.createElement('canvas');
	/*errase this absolute because with the new canvas it will put in the right position"*/
	this.Canvas.style.position='absolute';
	this.Canvas.style.left=PNLLEFT+"px";
	this.Canvas.style.top= PNLTOP+"px";
	this.Canvas.width=PNLWIDTH;
	this.Canvas.height=PNLHEIGHT;
	this.Canvas.style.zIndex=this.zIndex;
	theatre.appendChild(this.Canvas);

	if (this.Canvas.getContext)
	{
		this.Canvas.ctx = this.Canvas.getContext('2d');
	}
	else
	{
		this.Canvas=G_vmlCanvasManager.initElement(this.Canvas);
		this.Canvas.ctx = this.Canvas.getContext('2d');
	}

	this.Canvas.shape=this;
}

/**
* @class Shape
*
* @param  String		name			"Nombre del Shape"
* @param  String		title			"Titulo del Shape"
* @param  Boolean		open			"true: shape abierto"
* @param  Boolean		editable	"true: es editable"
* @param  String		type			"Nombre del tipo de Shape"
* @param  Collect		STORE			"Colección de Shapes"
* @return						this			"Retorna el Objeto"
*/
function Shape(name,title,open,editable,type,STORE = null)
{
	addTrackFunction("shape","Shape");
	this.title=title;
	this.name=name;
	this.open=open;
	this.editable=editable;
	this.type=type;
	var p=new Point(0,0);
	this.tplftcrnr=p; //coordinates of top left of boundary box;
	p=new Point(0,0);
	this.btmrgtcrnr=p; //coordinates of bottom right of boundary box;
	this.strokeStyle = LINECOLOR; //this.strokeStyle=[0,0,0,1];

	// opción 1: Sin color de relleno
	this.fillStyle= [0,0,0,0];
	// opción 2: Color random de la paleta de colores
	// var numFillColors = FILLCOLORS.length;
	// this.fillStyle=	COLOR[ FILLCOLORS[Math.floor(Math.random()*numFillColors)] ].slice(0);
	
	// Deseable de boundaries anchos
	// Descripcion: Ancho de línea mas grueso que el selector y que los boundaries
	// this.lineWidth = 3;
	this.lineWidth = 1;
	this.lineCap = "square";
	this.lineJoin = "miter"
	this.justfill=true;  //ordinary fill when true;
	this.linearfill=true; //linear when true, radial when false;
	this.lineGrad=[0,0,0,0];
	this.radGrad=[0,0,0,0,10,10];
	this.colorStops=[[0,0,0,0,0],[1,0,0,0,0]];
	this.stopn=0; //pointer to current stop point;
	this.shadow=false;
	this.shadowOffsetX = 15;
	this.shadowOffsetY = 15;
	this.shadowBlur = 0;
	this.shadowColor = [0, 0, 0, 0];
	this.zIndex=0;
	this.redozIndex=[]; // arreglo de los z-index,para ejecutar UndoRedo Command
	this.crnradius=10;
	this.arccentre=new Point(0,0);
	this.radius=10;
	p=new Point("end","end");
	this.path=new Node(p);
	this.path.next=this.path;
	this.path.prev=this.path;
	this.group;
	this.elType="_SHAPE";

	if (STORE != null)
	{
		STORE[this.name]=this;
	}

	//methods
	this.addTo=addTo;
	this.setPath=setPath;
	this.addNode=addNode;
	this.draw    = draw;
	this.drawEnd = drawEnd;
	this.drawGuide    = drawGuide;
	this.drawNext     = drawNext;
	this.drawBezGuides= drawBezGuides;
	this.setCorners= setCorners;
	this.fixCorners= fixCorners;
	this.inAgroup  = inAgroup;
	this.isOn=isOn;
	this.isIn=isIn;
	this.addAllMarks   = addAllMarks;
	this.ShapeToText   = ShapeToText;
	this.setRndRect    = setRndRect;

	if (this.type=="selector")
	{
		this.fillStyle = [255,255,255,0];
		this.lineWidth = 1; // selector
		this.drawEnd   = drawEndSelector;
		this.dotted    = true;
		this.linedasha = 5;
		this.linedashb = 5;
	} else if (this.type=="text" || this.type=="text_italic" || this.type=="text_bold")
	{
		this.fillStyle = LINECOLOR.slice(0);
		this.strokeStyle = [0,0,0,0];
		this.lineWidth = 0;
		this.inputText = null;
		this.textValue = "Ingrese texto";
		this.textPrevValue = null;
		this.focusState = 0;
		this.drawGuide = function(){};
		// Mejora 1. Texto con width de acuerdo a la longitud del texto:
		this.resizewidthinput = resizewidthinput;
	} else if (this.type =="flecha" || this.type=="cota")
	{
		this.fillStyle = LINECOLOR.slice(0);;
	} else if (this.type =="curve_dotted" || this.type=="freeform_dotted")
	{
		this.dotted    = true;
		this.linedasha = 5;
		this.linedashb = 5;
	} else {
		this.dotted    = false;
	}
	return this;
}

/**
* @class Point
*
* @param  Integer		x			"Valor en el eje X"
* @param  Integer		y			"Valor en el eje Y"
* @return						this	"Retorna el Objeto"
*/
function Point(x,y)
{
	addTrackFunction("shape","Point");
	this.x=x;
	this.y=y;

	//methods
	this.pointRotate=pointRotate;
}

/**
* @class Node
*
* @param  Object		point		"Objeto con las coordenadas del punto"
* @param  Object		ctrl1		"Objeto del punto ctrl1"
* @param  Object		ctrl2		"Objeto del punto ctrl2"
* @return						this		"Retorna el Objeto"
*/
function Node(point,ctrl1,ctrl2)
{
	addTrackFunction("shape","Node");
	this.point=point;
	if (arguments.length>1)
	{
		this.ctrl1=ctrl1;
		this.ctrl2=ctrl2;
		this.vertex="B";
	}
	else
	{
		this.ctrl1=new Point("non","non");
		this.ctrl2=new Point("non","non");
		this.vertex="L";
	}
	this.corner="corner";
	this.next="";
	this.prev="";
	this.shape;
	//methods
	this.setNode=setNode;
	this.addPointMark=addPointMark;
	this.addCtrl1Mark=addCtrl1Mark;
	this.addCtrl2Mark=addCtrl2Mark;
	this.addFullMarks=addFullMarks;
	this.removeMark=removeMark;
	this.updatePointNode=updatePointNode;
	this.updateCtrl1Node=updateCtrl1Node;
	this.updateCtrl2Node=updateCtrl2Node;
	this.insertNodeBefore=insertNodeBefore;
	this.removeNode=removeNode;
	this.getAngle=getAngle;
	this.getAngleTo=getAngleTo;
	this.rotate=rotate;
	this.scaleY=scaleY;
	this.translate=translate;
	this.copyNodeTo=copyNodeTo;
	this.setNodePathBox=setNodePathBox;
}

/**
* @function setPath
* Inicializa el dibujado del Shape
* @param		Object		cursor		"Coordenadas del cursor"
* @return		undefined						"No devuelve ningun valor"
*/
function setPath(cursor)
{
	addTrackFunction("shape","setPath");
	
	var point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round(cursor.y/ygrid)*ygrid);
	var node=new Node(point);
	this.addNode(node);
	this.tplftcrnr=new Point(node.point.x,node.point.y);
	this.btmrgtcrnr=new Point(node.point.x,node.point.y);
	var curshape=this;
	this.Canvas.onmousemove=function(e)
	{
		noBubble(e);
		DRAWING.position = getPosition(e);
		if ( this.shape.type !== "text" && this.shape.type !== "text_bold" && this.shape.type !== "text_italic")
		{
			this.shape.drawGuide(getPosition(e));
		}
	};
	this.Canvas.onmousedown=function(e)
	{};
	if ( !(this.type == "curve" || this.type == "freeform" || this.type == "curve_dotted" ||
		this.type == "freeform_dotted" || this.type == "text" || this.type == "text_bold" || this.type == "text_italic" ) )
	{
		this.Canvas.onmouseup=function(e)
		{this.shape.drawEnd(getPosition(e))};
	}

	DRAWING.shapeDrawing = this;

	switch (this.type)
	{
		case "line":
		point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
		node=new Node(point);
		this.addNode(node);
		break;

		case "arc":
		case "segment":
		case "sector":
		point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
		node=new Node(point);
		this.addNode(node);
		if (this.type=="sector"  || this.type=="segment")
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		if (this.type=="sector")
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "curve":
		case "curve_dotted":
		case "freeform":
		case "freeform_dotted":
		var start=this.path.next;
		point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
		var ctrl1=new Point(start.point.x + (point.x - start.point.x)/4, start.point.y+(point.y-start.point.y)/4);
		var ctrl2=new Point(point.x+(start.point.x-point.x)/4, point.y+(start.point.y-point.y)/4);
		node=new Node(point,ctrl1,ctrl2);
		this.addNode(node);
		var curshape=this;
		this.Canvas.onmousedown=function(e)
		{
			if( e.button == 2 ){ //right click
				drawEndOut(); // finaliza el dibujado
				return;
			}
			var absX = Math.abs(start.point.x - getPosition(e).x);
			var absY = Math.abs(start.point.y - getPosition(e).y);

			if (absX < 15 && absY <15)
			{
				if (this.shape.type=="freeform")
				{
					curshape.path.prev.removeNode();
					curshape.drawEnd(getPosition(e));
				} else {
					if ( this.shape.type == "curve_dotted" )
					{
						this.shape.type="freeform_dotted";
					} else{
						this.shape.type="freeform";
					}
					curshape.open = false;
					var inicio = start.point;
					curshape.path.next.removeNode();
					curshape.drawEnd(getPosition(inicio));
				}
			} else{
				this.shape.drawNext(getPosition(e))
			}
		};
		break;

		case "rectangle":
		for (var i=0;i<4;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "ellipse":
		for (var i=0;i<4;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "rounded_rectangle":
		for (var i=0;i<8;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "triangle":
		for (var i=0;i<3;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "right_triangle":
		for (var i=0;i<3;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "flecha":
		for (var i=0;i<7;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "cota":
		for (var i=0;i<9;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;

		case "text":
		case "text_italic":
		case "text_bold":
		var textStyle = {
			text:{
				fontWeight: 'normal',
				fontStyle: 'normal'
			},
			text_italic:{
				fontWeight: 'normal',
				fontStyle: 'italic'
			},
			text_bold:{
				fontWeight: 'bold',
				fontStyle: 'normal'
			}
		};

		for (var i=0;i<4;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}

		var endPoint = {
			x : node.point.x + STANDARFONTSIZE,
			y : node.point.y + 51
		};
		this.drawEnd(endPoint);
		var self=this;
		var inputText = new CanvasInput({
			canvas: self.Canvas,
			maxlength: "ORÁCULO MATEMÁGICO".length,
			x : node.point.x,
			y : node.point.y,
			fontSize: STANDARFONTSIZE,
			fontFamily: 'Arial',
			fontColor: 'rgba('+this.fillStyle[0]+','+this.fillStyle[1]+','+
			this.fillStyle[2]+','+this.fillStyle[3]+')',
			fontWeight: textStyle[this.type].fontWeight,
			backgroundColor: 'rgba(0, 0, 0, 0)',
			fontStyle: textStyle[this.type].fontStyle,
			width: 300,
			padding: 8,
			borderWidth: 0,
			borderColor: 'rgba(0, 0, 0, 0)',
			borderRadius: 0,
			boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			innerShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			value: this.textValue,
			// Mejora 1. Texto con width de acuerdo a la longitud del texto:
			shape: this,
			onfocus: function()
			{
				self.focusState = 1;
				self.textPrevValue = this.value();
			},
			onblur: function()
			{
				self.focusState = 0;
				self.textValue = this.value();
				if (self.textPrevValue != null && self.textPrevValue != this.value())
				{
					var actions = [];
					actions.push(new editInputTextCommand(self));

					if (actions.length > 0 )
					{
						window.UndoRedo.executeCommand(actions);
					}
					self.textPrevValue = null;
				}
			}
		});
		this.inputText = inputText;//asigna el objeto al atributo del Shape
		// Mejora 1. Texto con width de acuerdo a la longitud del texto:
		this.inputText.value(this.textValue);
		this.draw();
		this.inputText.focus();//se posiciona en el texto
		break;

		case "selector":
		for (var i=0;i<4;i++)
		{
			point=new Point(Math.round(cursor.x/xgrid)*xgrid,Math.round((cursor.y)/ygrid)*ygrid);
			node=new Node(point);
			this.addNode(node);
		}
		break;
	}
}

/**
* @function addNode
* Agrega nodo al shape
* @param		Object			node		"Objeto node"
* @return		undefined						"No devuelve ningun valor"
*/
function addNode(node)
{
	addTrackFunction("shape","addNode");
	var path=this.path;
	node.next=path;
	node.prev=path.prev;
	path.prev.next=node;
	path.prev=node;
	node.shape=this;
}

/**
* @function drawGuide
* Dibuja una linea que sirve como guía en el dibujado
* @param		Object		cursor		"Coordenadas del cursor"
* @return		undefined						"No devuelve ningun valor"
*/
function drawGuide(cursor)
{
	addTrackFunction("shape","drawGuide");
	cursor.x=Math.round(cursor.x/xgrid)*xgrid;
	cursor.y=Math.round(cursor.y/ygrid)*ygrid;
	this.btmrgtcrnr.x=cursor.x;
	this.btmrgtcrnr.y=cursor.y;
	switch (this.type)
	{
		case "line":
		node=this.path.prev;
		node.setNode(cursor);
		break;

		case "arc":
		case "segment":
		case "sector":
		var start=this.path.next;
		var lx=cursor.x-start.point.x;
		var ly=cursor.y-start.point.y;
		var c1=new Point(start.point.x+lx*K,start.point.y);
		var c2=new Point(start.next.point.x,start.next.point.y-ly*K);
		start.next.setNode(cursor,c1,c2);
		if (this.type=="sector")
		{
			var p=new Point(start.point.x,start.next.point.y);
			start.next.next.setNode(p);
		}
		break;

		case "curve":
		case "curve_dotted":
		case "freeform":
		case "freeform_dotted":
		node=this.path.prev;
		node.setNode(cursor);
		var prev=node.prev;
		var ctrl1=new Point(prev.point.x+(node.point.x-prev.point.x)/4, prev.point.y+(node.point.y-prev.point.y)/4);
		var ctrl2=new Point(node.point.x+(prev.point.x-node.point.x)/4, node.point.y+(prev.point.y-node.point.y)/4);
		node.setNode(cursor,ctrl1,ctrl2);
		break;

		case "rectangle":
		var start=this.path.next;
		var p=new Point(cursor.x,start.point.y);
		node=start.next;
		node.setNode(p);
		node=node.next;
		node.setNode(cursor);
		node=node.next;
		p.x=start.point.x;
		p.y=cursor.y;
		node.setNode(p);
		node=node.next;
		p.x=start.point.x;
		p.y=start.point.y;
		node.setNode(p);
		break;

		case "ellipse":
		var lx=(cursor.x-this.tplftcrnr.x)/2;
		var ly=(cursor.y-this.tplftcrnr.y)/2;
		var midx=this.tplftcrnr.x+lx;
		var midy=this.tplftcrnr.y+ly;

		var p=new Point(midx,this.tplftcrnr.y);
		var c1=new Point(p.x+lx*K,p.y);
		node=this.path.next;//top
		node.setNode(p); //top point
		node=node.next;//right

		p.x=cursor.x;
		p.y=midy;
		var c2=new Point(p.x,p.y-ly*K);
		node.setNode(p,c1,c2); //right point
		c1.x=p.x;
		c1.y=p.y+ly*K;

		p.x=midx;
		p.y=cursor.y;
		c2.x=p.x+lx*K;
		c2.y=p.y;
		node=node.next;//bottom
		node.setNode(p,c1,c2);  //bottom point
		c1.x=p.x-lx*K;
		c1.y=p.y;

		p.x=this.tplftcrnr.x;
		p.y=midy;
		c2.x=p.x;
		c2.y=p.y+ly*K;
		node=node.next;//left
		node.setNode(p,c1,c2); //left point
		c1.x=p.x;
		c1.y=p.y-ly*K;

		p.x=midx
		p.y=this.tplftcrnr.y;
		c2.x=p.x-lx*K;
		c2.y=p.y;
		node=node.next;//top
		node.setNode(p,c1,c2);//back at top
		break;

		case "rounded_rectangle":
		this.setRndRect();
		break;

		case "triangle":
		var start=this.path.next;
		p=new Point(cursor.x,cursor.y);
		node=start.next;
		node.setNode(p);
		var dx=cursor.x-start.point.x;
		p.x=start.point.x-dx;
		node=node.next;
		node.setNode(p);
		node=node.next;
		p.x=start.point.x;
		p.y=start.point.y;
		node.setNode(p);
		break;

		case "right_triangle":
		var start=this.path.next;
		p=new Point(cursor.x,cursor.y);
		node=start.next;
		node.setNode(p);
		p.x=start.point.x;
		node=node.next;
		node.setNode(p);
		node=node.next;
		p.x=start.point.x;
		p.y=start.point.y;
		node.setNode(p);
		break;

		case "selector":
		var start=this.path.next;
		var p=new Point(cursor.x,start.point.y);
		node=start.next;
		node.setNode(p);
		node=node.next;
		node.setNode(cursor);
		node=node.next;
		p.x=start.point.x;
		p.y=cursor.y;
		node.setNode(p);
		node=node.next;
		p.x=start.point.x;
		p.y=start.point.y;
		node.setNode(p);
		break;

		case "flecha":
		var start=this.path.next;
		var puntos = getFlechaPoints({point1:start.point,point2:cursor});
		var p=new Point(puntos.pointmedio.x,puntos.pointmedio.y);
		node=start.next;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point3.x;
		p.y=puntos.point3.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point2.x;
		p.y=puntos.point2.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point4.x;
		p.y=puntos.point4.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.pointmedio.x;
		p.y=puntos.pointmedio.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point1.x;
		p.y=puntos.point1.y;
		node.setNode(p);

		// Cerrando
		node=node.next;
		p.x=puntos.pointmedio.x;
		p.y=puntos.pointmedio.y;
		node.setNode(p);
		break;

		case "cota":
		var start=this.path.next;
		var puntos = getCotaPoints({point1:start.point,point2:cursor});
		var p=new Point(puntos.point3.x,puntos.point3.y);
		node=start.next;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point4.x;
		p.y=puntos.point4.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point1.x;
		p.y=puntos.point1.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point2.x;
		p.y=puntos.point2.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point6.x;
		p.y=puntos.point6.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point5.x;
		p.y=puntos.point5.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point2.x;
		p.y=puntos.point2.y;
		node.setNode(p);
		node=node.next;
		p.x=puntos.point1.x;
		p.y=puntos.point1.y;
		node.setNode(p);
		break;
	}
	this.draw();
}

/**
* @function setNode
* Asigna valor al objeto node
* @param  	Object		point		"Objeto con las coordenadas del punto"
* @param  	Object		ctrl1		"Objeto del punto ctrl1"
* @param  	Object		ctrl2		"Objeto del punto ctrl2"
* @return		undefined					"Retorna el Objeto"
*/
function setNode(point,ctrl1,ctrl2)
{
	addTrackFunction("shape","setNode");
	this.point.x=point.x;
	this.point.y=point.y;
	if (arguments.length>1)
	{
		this.ctrl1.x=ctrl1.x;
		this.ctrl1.y=ctrl1.y;
		this.ctrl2.x=ctrl2.x;
		this.ctrl2.y=ctrl2.y;
		this.vertex="B";
	}
}

/**
* @function drawEnd
* Finaliza el dibujado del Shape
* @param		Object		cursor		"Coordenadas del cursor"
* @return		undefined						"No devuelve ningun valor"
*/
function drawEnd(cursor)
{
	addTrackFunction("shape","drawEnd");
	cursor.x=Math.round(cursor.x/xgrid)*xgrid;
	cursor.y=Math.round(cursor.y/ygrid)*ygrid;
	this.btmrgtcrnr=cursor;
	document.getElementById("markerdrop").style.visibility="visible";
	if (this.editable)
	{
		document.getElementById("backstage").style.visibility="visible";
		var node=this.path.next;
		node.addPointMark();
		node=node.next;
	};
	this.Canvas.style.cursor="default";
	this.Canvas.onmousemove=function()
	{};
	this.Canvas.onmouseup=function()
	{};
	switch (this.type)
	{
		case "line":
		node.addPointMark();
		var start=this.path.next;
		var last=this.path.prev;
		this.tplftcrnr.x=Math.min(start.point.x,last.point.x);
		this.tplftcrnr.y=Math.min(start.point.y,last.point.y);
		this.btmrgtcrnr.x=Math.max(start.point.x,last.point.x);
		this.btmrgtcrnr.y=Math.max(start.point.y,last.point.y);
		break;

		case "arc":
		case "segment":
		case "sector":
		node.addPointMark();
		this.arcwidth=this.btmrgtcrnr.x-this.tplftcrnr.x;
		this.archeight=this.btmrgtcrnr.y-this.tplftcrnr.y;
		this.arccentre=new Point(this.tplftcrnr.x,this.btmrgtcrnr.y);
		var start=this.path.next;
		var arcend=start.next;
		if (this.arcwidth*this.archeight<0)//swap nodes so that going clockwise start node is before last node
		{
			this.arcwidth=Math.abs(this.arcwidth);
			this.archeight=Math.abs(this.archeight);

			var sp=new Point(last.point.x,last.point.y);
			var lp=new Point(start.point.x,start.point.y);
			var cs1=new Point(last.ctrl2.x,last.ctrl2.y);
			var cs2=new Point(last.ctrl1.x,last.ctrl1.y);
			start.setNode(sp);
			arcend.setNode(lp,cs1,cs2);
			start.removeMark();
			arcend.removeMark(0);
			start.addPointMark();
			arcend.addPointMark();
		}
		point=new Point(start.point.x,start.point.y);
		this.bnode=new Node(point);//node for 90 degrees
		arcend.insertNodeBefore(this.bnode);
		point=new Point(start.point.x,start.point.y);
		this.lnode=new Node(point);//node for 180 degrees
		arcend.insertNodeBefore(this.lnode);
		point=new Point(start.point.x,start.point.y);
		this.tnode=new Node(point);//node for 270 degrees
		arcend.insertNodeBefore(this.tnode);
		this.arcwidth=Math.abs(this.arcwidth);//possibility of both being negative
		this.archeight=Math.abs(this.archeight);
		this.radius=this.arcwidth;
		this.setCorners();
		break;

		case "freeform":
		case "freeform_dotted":
		var last=this.path.prev;
		node=this.path.next;
		var point=new Point(node.point.x,node.point.y);
		var ctrl2=new Point(node.point.x+(last.point.x-node.point.x)/4, node.point.y+(last.point.y-node.point.y)/4);
		var ctrl1=new Point(last.point.x+(node.point.x-last.point.x)/4, last.point.y+(node.point.y-last.point.y)/4);
		var closenode= new Node(point,ctrl1,ctrl2);
		this.addNode(closenode);
		node.removeMark();
		node=node.next;
		while (node.point.x != "end")
		{
			node.addFullMarks();
			node=node.next;
		}
		this.draw();
		this.drawBezGuides();
		DEFAULTKEYDOWN;
		this.setCorners();
		break;

		case "curve":
		case "curve_dotted":
		if (!this.open)
		{
			var last=this.path.prev;
			node=this.path.next;
			var point=new Point(node.point.x,node.point.y);
			var ctrl2=new Point(node.point.x+(last.point.x-node.point.x)/4, node.point.y+(last.point.y-node.point.y)/4);
			var ctrl1=new Point(last.point.x+(node.point.x-last.point.x)/4, last.point.y+(node.point.y-last.point.y)/4);
			var closenode= new Node(point,ctrl1,ctrl2);
			this.addNode(closenode);
			node.removeMark();
			node=node.next;
		} else{
			node=this.path.next;
			node.removeMark();
		}
		while (node.point.x != "end")
		{
			node.addFullMarks();
			node=node.next;
		}
		this.draw();
		this.drawBezGuides();
		DEFAULTKEYDOWN
		this.setCorners();
		break;

		case "triangle":
		this.tplftcrnr.x -= cursor.x-this.tplftcrnr.x;
		break;

		case "flecha":
		this.draw();
		this.drawBezGuides();
		DEFAULTKEYDOWN;
		this.setCorners();
		break;

		case "cota":
		this.draw();
		this.drawBezGuides();
		DEFAULTKEYDOWN;
		this.setCorners();
		break;

		case "text":
		case "text_bold":
		case "text_italic":
		var start=this.path.next;
		var p=new Point(cursor.x,start.point.y);
		node=start.next;
		node.setNode(p);
		node=node.next;
		node.setNode(cursor);
		node=node.next;
		p.x=start.point.x;
		p.y=cursor.y;
		node.setNode(p);
		node=node.next;
		p.x=start.point.x;
		p.y=start.point.y;
		node.setNode(p);
		break;
	}
	this.fixCorners();
	// rehaciendo el algoritmo en caso el shape tenga dimensiones muy pequeñas
	if( ( (this.btmrgtcrnr.x - this.tplftcrnr.x ) < 10 ) || ( (this.btmrgtcrnr.y - this.tplftcrnr.y) < 10 )  ){
		if( ( (this.btmrgtcrnr.x - this.tplftcrnr.x ) < 10 ) && ( (this.btmrgtcrnr.y - this.tplftcrnr.y) < 10 )  ){
			STANDARWIDTHHEIGHT = 90
			addX = STANDARWIDTHHEIGHT;
			addY = STANDARWIDTHHEIGHT;
		}else if( (this.btmrgtcrnr.x - this.tplftcrnr.x ) < 10 ) {
			addX = 10;
			addY = this.btmrgtcrnr.y - this.tplftcrnr.y;
		}else if( (this.btmrgtcrnr.y - this.tplftcrnr.y ) < 10 ) {
			addX = this.btmrgtcrnr.x - this.tplftcrnr.x;
			addY = 10;
		}
		if( this.type == "rectangle" ||  this.type == "triangle"  ||  this.type == "ellipse" ||  this.type == "rounded_rectangle" || 
			  this.type == "flecha" || this.type == "cota" ){
			this.drawGuide({x:this.tplftcrnr.x+addX,y:this.tplftcrnr.y+addY});
			this.drawEnd({x:this.tplftcrnr.x+addX,y:this.tplftcrnr.y+addY});
			return;
		}else if( this.type == "freeform" ||  this.type == "curve_dotted"  ||  this.type == "curve" ){
			DELETED[this.group.name]=this.group;
			var command = new DeleteShapesCommand(this);
			command.execute();
			return;
		}
	}
	if (this.group!=undefined && this.group!=null)
	{
		this.group.left= this.tplftcrnr.x;
		this.group.top= this.tplftcrnr.y;
		this.group.width=this.btmrgtcrnr.x-this.tplftcrnr.x;
		this.group.height=this.btmrgtcrnr.y-this.tplftcrnr.y;
		this.group.centreOfRotation.x=this.group.left+this.group.width/2;
		this.group.centreOfRotation.y=this.group.top+this.group.height/2;
	}
	if (!this.editable && this.type!="selector" &&
		this.type!="text" && this.type!="text_bold" && this.type!="text_italic" )
	{
		document.getElementById("boundarydrop").style.visibility="visible";
		if (this.group!=undefined && this.group!=null)
		{
			this.group.drawBoundary(this.type, Object.keys(SELECTED).length + 1);
			SELECTED[this.group.name]=this.group;
		}
	}
	SELECTEDSHAPE=this;
	var actions = [];
	actions.push(new CreateShapeCommand(this));
	if (actions.length > 0 )
	{
		window.UndoRedo.executeCommand(actions);
	}
	DRAWING = {};
	if (DRAWING_SHAPE > 0)
	{
		self = this;
		if (DRAWING_SHAPE == 11 || DRAWING_SHAPE == 21 || DRAWING_SHAPE == 22){
			setTimeout(function()
			{
				self.inputText.focus();
			}, 500);
		} else {
			drawGeneralShape(DRAWING_SHAPE);
		}
	}
}

/**
* @function fixCorners
* Actualiza los valores de los puntos top y bot
* @return		undefined						"No devuelve ningun valor"
*/
function fixCorners()
{
	addTrackFunction("shape","fixCorners");
	if( this.type == "cota" || this.type == "flecha" ){
		
		var tx=this.tplftcrnr.x;
		var ty=this.tplftcrnr.y;
		var bx=this.btmrgtcrnr.x;
		var by=this.btmrgtcrnr.y;
		this.tplftcrnr.x=Math.min(tx,bx);
		this.tplftcrnr.y=Math.min(ty,by);
		this.btmrgtcrnr.x=Math.max(tx,bx);
		this.btmrgtcrnr.y=Math.max(ty,by);
	}else{
		var tx=this.tplftcrnr.x;
		var ty=this.tplftcrnr.y;
		var bx=this.btmrgtcrnr.x;
		var by=this.btmrgtcrnr.y;
		this.tplftcrnr.x=Math.min(tx,bx);
		this.tplftcrnr.y=Math.min(ty,by);
		this.btmrgtcrnr.x=Math.max(tx,bx);
		this.btmrgtcrnr.y=Math.max(ty,by);
	}
}

/**
* @function setRndRect
* Actualiza los valores de bottom right corner del shape
* @return		undefined						"No devuelve ningun valor"
*/
function setRndRect() //bottom right corner
{
	addTrackFunction("shape","setRndRect");
	var RRwidth=this.btmrgtcrnr.x-this.tplftcrnr.x;
	var RRheight=this.btmrgtcrnr.y-this.tplftcrnr.y;
	var dx=RRwidth/Math.abs(RRwidth);
	var dy=RRheight/Math.abs(RRheight);
	if (Math.abs(RRwidth)<20)
	{
		this.btmrgtcrnr.x=this.tplftcrnr.x+20*dx;
		RRwidth=this.btmrgtcrnr.x-this.tplftcrnr.x;
	}
	if (Math.abs(RRheight)<20)
	{
		this.btmrgtcrnr.y=this.tplftcrnr.y+20 *dy;
		RRheight=this.btmrgtcrnr.y-this.tplftcrnr.y;
	}
	var brc=this.btmrgtcrnr;
	var start=this.path.next;
	p=new Point(this.tplftcrnr.x+this.crnradius*dx,this.tplftcrnr.y);
	start.setNode(p);// top left
	node=start.next;
	p.x=brc.x-this.crnradius*dx;
	p.y=start.point.y;
	node.setNode(p);// top right
	var c1=new Point(p.x+this.crnradius*dx*K,p.y);
	p.x=brc.x;
	p.y+=this.crnradius*dy;
	var c2=new Point(p.x,p.y-this.crnradius*dy*K);
	node=node.next;
	node.setNode(p,c1,c2);//right top
	p.y=brc.y-this.crnradius*dy;
	node=node.next;
	node.setNode(p);//right bottom
	c1.x=p.x;
	c1.y=p.y+this.crnradius*dy*K;
	p.x=brc.x-this.crnradius*dx;
	p.y=brc.y;
	c2.x=p.x+this.crnradius*dx*K;
	c2.y=p.y;
	node=node.next;
	node.setNode(p,c1,c2);//bottom right
	p.x=start.point.x;
	node=node.next;
	node.setNode(p);//bottom left
	c1.x=p.x-this.crnradius*dx*K;
	c1.y=p.y;
	p.x=this.tplftcrnr.x
	p.y=p.y-this.crnradius*dy;
	c2.x=p.x;
	c2.y=p.y+this.crnradius*dy*K;
	node=node.next;
	node.setNode(p,c1,c2);//left bottom
	p.y=start.point.y+this.crnradius*dy;
	node=node.next;
	node.setNode(p);//left top
	c1.x=p.x;
	c1.y=p.y-this.crnradius*dy*K;
	p.x=start.point.x;
	p.y=start.point.y;
	c2.x=p.x-this.crnradius*dx*K;
	c2.y=p.y;
	node=node.next;
	node.setNode(p,c1,c2);//top left again
}

/**
* @function drawNext
* Instancia el nuevo nodo del shape en el dibujado
* @param		Object		cursor		"Coordenadas del cursor"
* @return		undefined						"No devuelve ningun valor"
*/
function drawNext(cursor)
{
	addTrackFunction("shape","drawNext");
	cursor.x=Math.round(cursor.x/xgrid)*xgrid;
	cursor.y=Math.round(cursor.y/ygrid)*ygrid;
	this.path.prev.setNode(cursor);
	var node=new Node(cursor);
	this.addNode(node);
}

/**
* @function removeNode
* Remueve el nodo del shape en el dibujado
* @return		undefined						"No devuelve ningun valor"
*/
function removeNode()
{
	addTrackFunction("shape","removeNode");
	this.prev.next=this.next;
	this.next.prev=this.prev;
}

/**
* @function setCorners
* Actualiza valores de los vertices del Shape (top, bottom)
* @return		undefined						"No devuelve ningun valor"
*/
function setCorners() //sets boundary for individual shapes;
{
	addTrackFunction("shape","setCorners");
	var step=100;
	var x,y;
	node=this.path.next;
	var mxx=node.point.x;
	var mnx=node.point.x;
	var mxy=node.point.y;
	var mny=node.point.y;
	node=node.next;
	while (node.point.x!="end")
	{
		if (node.vertex=="L")
		{
			x = node.point.x;
			y = node.point.y;
			if (x>mxx)
				{mxx=x};
			if (x<mnx)
				{mnx=x};
			if (y>mxy)
				{mxy=y};
			if (y<mny)
				{mny=y};
		}
		else
		{
			for (var i=0;i<=step; i++)
			{
				t=i/step;
				x = (1-t)*(1-t)*(1-t)*node.prev.point.x + 3*(1-t)*(1-t)*t*node.ctrl1.x + 3*(1-t)*t*t*node.ctrl2.x + t*t*t*node.point.x;
				y = (1-t)*(1-t)*(1-t)*node.prev.point.y + 3*(1-t)*(1-t)*t*node.ctrl1.y + 3*(1-t)*t*t*node.ctrl2.y + t*t*t*node.point.y;
				if (x>mxx)
					{mxx=x};
				if (x<mnx)
					{mnx=x};
				if (y>mxy)
					{mxy=y};
				if (y<mny)
					{mny=y};
			}
		}
		node=node.next;
	}
	this.tplftcrnr.x=mnx;
	this.tplftcrnr.y=mny;
	this.btmrgtcrnr.x=mxx;
	this.btmrgtcrnr.y=mxy;
	this.group.left=this.tplftcrnr.x;
	this.group.top=this.tplftcrnr.y;
	this.group.width=this.btmrgtcrnr.x-this.tplftcrnr.x;
	this.group.height=this.btmrgtcrnr.y-this.tplftcrnr.y;
}

/**
* @function drawEndSelector
* Finaliza el dibujado del Selector
* @param		Object		cursor		"Coordenadas del cursor"
* @return		undefined						"No devuelve ningun valor"
*/
function drawEndSelector(cursor)
{
	addTrackFunction("shape","drawEndSelector");
	// if( shapecopy.SELECTED ){
	// 	for( var nameGroup in shapecopy.SELECTED ){
	// 		shapecopy.SELECTED[nameGroup].removeBoundary();
	// 		delete shapecopy.SELECTED[nameGroup];
	// 	}
	// 	shapecopy.isSelector = false;
	// }
	cursor.x=Math.round(cursor.x/xgrid)*xgrid;
	cursor.y=Math.round(cursor.y/ygrid)*ygrid;
	this.btmrgtcrnr=cursor;
	document.getElementById("markerdrop").style.visibility="visible";
	if (this.editable)
	{

		document.getElementById("backstage").style.visibility="visible";
		var node=this.path.next;
		node.addPointMark();
		node=node.next;
	};
	this.Canvas.style.cursor="default";
	this.Canvas.onmousemove=function()
	{};
	this.Canvas.onmouseup=function()
	{};
	closeStops();
	removeRotate();

	if (!shiftdown)
	{
		SELECTED={};
		BCOUNT=0;
		clear(document.getElementById("markerdrop"));
		resetBoundary();
	}
	var shape, foundshape;
	var shapefound=false;

	if ( this.tplftcrnr.x > this.btmrgtcrnr.x ){
		aux = this.btmrgtcrnr.x;
		this.btmrgtcrnr.x = this.tplftcrnr.x;
		this.tplftcrnr.x = aux;
	}
	if ( this.tplftcrnr.y > this.btmrgtcrnr.y ){
		aux = this.btmrgtcrnr.y;
		this.btmrgtcrnr.y = this.tplftcrnr.y;
		this.tplftcrnr.y = aux;
	}
	var puntos = [
	{x:this.tplftcrnr.x,y:this.tplftcrnr.y},
	{x:this.btmrgtcrnr.x,y:this.tplftcrnr.y},
	{x:this.btmrgtcrnr.x,y:this.btmrgtcrnr.y},
	{x:this.tplftcrnr.x,y:this.btmrgtcrnr.y}
	];
	var arrShapesAux = [];
	for (var name in CURRENT)
	{
		shape=CURRENT[name];

		var shapePuntos = [
		{x:shape.tplftcrnr.x,y:shape.tplftcrnr.y},
		{x:shape.btmrgtcrnr.x,y:shape.tplftcrnr.y},
		{x:shape.btmrgtcrnr.x,y:shape.btmrgtcrnr.y},
		{x:shape.tplftcrnr.x,y:shape.btmrgtcrnr.y}
		];
		var ver = true;

		// Todos los puntos del shape dentro del selector
		for (var i = 0; i < shapePuntos.length; i++)
		{
			verMayorTL = (shapePuntos[i].x>=puntos[0].x && shapePuntos[i].y>=puntos[0].y);
			verMenorBR = (shapePuntos[i].x<=puntos[2].x && shapePuntos[i].y<=puntos[2].y);
			ver = ( ver && ( verMenorBR && verMayorTL ) );
		};

		if (shape!=this && ver )
		{
			foundshape = shape;
			if (foundshape.group.name in SELECTED)
			{
				foundshape.group.removeBoundary();
				delete SELECTED[foundshape.group.name];
			}
			else
			{
				arrShapesAux.push(foundshape.group);
				SELECTED[foundshape.group.name]=foundshape.group;
				SELECTEDSHAPE=foundshape;
				drawEndSelector.SELECTED[foundshape.group.name]=foundshape.group;
			}
		}
	}
	if (arrShapesAux.length > 0 ){
		for (var index in arrShapesAux){
			arrShapesAux[index].drawBoundary(arrShapesAux[index].members[0].type, Object.keys(SELECTED).length);
		}
		drawEndSelector.isSelector = true;
		toMoveEvent();
		groupJoin();
	}else{
		addMouseEventToSelect();
	}

	this.Canvas.remove();
	for (var name in CURRENT)
	{
		shape=CURRENT[name];
		if (shape==this){
			CURRENT[name].Canvas.remove();
			delete CURRENT[name].Canvas
			var group = GROUPS[CURRENT[name].group.name];
			delete GROUPS[ CURRENT[name].group.name ];
			delete CURRENT[name];
		}
	}
	var workArea = document.getElementById("workArea");
	fireEvent(workArea,"click");
	DRAWING = {};
}
// parameter to check if checkBoundary find a shape
// initialize with undefined
drawEndSelector.isSelector = false;
drawEndSelector.SELECTED={};

/**
* @function fireEvent
* Lanza el evento automáticamente
* @param		Object		element			"Elemento a asignar evento"
* @param		String		nameEvent		"nombre del evento"
* @return		undefined							"No devuelve ningun valor"
*/
function fireEvent(element, nameEvent)
{
	var event; // The custom event that will be created
	if (document.createEvent)
	{
		event = document.createEvent("HTMLEvents");
		event.initEvent(nameEvent, true, true);
	} else {
		event = document.createEventObject();
		event.eventType = nameEvent;
	}

	event.eventName = nameEvent;

	if (document.createEvent)
	{
		element.dispatchEvent(event);
	} else {
		element.fireEvent("on" + event.eventType, event);
	}
}

/**
* @function sendback
* Envia al Shape a la última posición del conjunto de capas
* @return		undefined							"No devuelve ningun valor"
*/
function sendback()
{
	addTrackFunction("shape","sendback");
	var name;
	var temps;
	var shapelist=[];
	for (var group in SELECTED)
	{
		var members=SELECTED[group].memberShapes();
		for (var shape in members)
		{
			temps=[];
			temps[0]=members[shape].name;
			temps[1]=members[shape].zIndex;
			shapelist.push(temps);
		}
	}
	var actions = [];
	shapelist.sort(zindn);
	actions.push(new ChangeZIndexCommand(shapelist,"-1"));
	if (actions.length > 0)
	{
		window.UndoRedo.executeCommand(actions);
	}

	function zindn(a,b)
	{
		addTrackFunction("shape","zindn");
		return b[1]-a[1]
	}
}

/**
* @function bringfront
* Trae al Shape a la primera posición del conjunto de capas
* @return		undefined							"No devuelve ningun valor"
*/
function bringfront()
{
	addTrackFunction("shape","bringfront");
	var name;
	var temps;
	var shapelist=[];
	for (var group in SELECTED)
	{
		var members=SELECTED[group].memberShapes();
		for (var shape in members)
		{
			temps=[];
			temps[0]=members[shape].name;
			temps[1]=members[shape].zIndex;
			shapelist.push(temps);
		}
	}
	var actions = [];
	shapelist.sort(zindp);
	actions.push(new ChangeZIndexCommand(shapelist,"+1"));

	if (actions.length > 0)
	{
		window.UndoRedo.executeCommand(actions);
	}
}

/**
* @function arctan
* Retorna ángulo de la linea (0,0) a (x,y) desde la  como abscisa x
* @param		Integer		y		"valor en la abscisa y"
* @param		Integer		x		"valor en la abscisa x"
* @return		Integer				"valor del ángulo"
*/
function arctan(y,x)
{
	addTrackFunction("shape","arctan");
	if (x==0)
	{
		if (y>0)
		{
			return Math.PI/2;
		}
		else
		{
			return 3*Math.PI/2;
		}
	}

	if (y==0)
	{
		if (x>0)
		{
			return 0;
		}
		else
		{
			return Math.PI;
		}
	}

	var theta = Math.atan(Math.abs(y/x));

	if (x>0)
	{
		if (y<0)
			{theta = 2*Math.PI-theta};
	}
	else
	{
		if (y>0)
		{
			theta = Math.PI-theta;
		}
		else
		{
			theta += Math.PI;
		}
	}

	return theta;
}

/**
* @function pointRotate
* Retorna el nuevo angulo en la rotación
* @param		Integer		theta		"valor del angulo"
* @return		Object						"Objeto con coordenadas del punto en la rotación"
*/
function pointRotate(theta)  //rotate p about origin through angle theta
{
	addTrackFunction("shape","pointRotate");
	var px=this.x*Math.cos(theta)-this.y*Math.sin(theta);
	var py=this.x*Math.sin(theta)+this.y*Math.cos(theta);
	var p=new Point(px,py);
	return p;
}

/**
* @function shapecopy
* Realiza duplicado de Shape
* @param		Integer		offset		"valor escalar para nuevo punto de origen del Shape"
* @return		undefined						"No devuelve valor"
*/
function shapecopy(offset)
{
	addTrackFunction("shape","shapecopy");
	ungroup();
	// if( drawEndSelector.SELECTED ){
	// 	for( var nameGroup in drawEndSelector.SELECTED ){
	// 		drawEndSelector.SELECTED[nameGroup].removeBoundary();
	// 		delete drawEndSelector.SELECTED[nameGroup];
	// 	}
	// 	drawEndSelector.isSelector = false;
	// }
	var name;
	var temps;
	var shapelist=[];
	justSelectedBoundaries();
	SELECTED={};
	var numBoundaries = 0;
	var numselected = 0;
	for (var i=0;i<document.getElementById("boundarydrop").childNodes.length;i++)  //child nodes are drawn boundaries
	{
		var group=document.getElementById("boundarydrop").childNodes[i].group;
		var groupcopy=copyGroup(group,offset,document.getElementById("shapestage"),SHAPES,GROUPS);
		var members=groupcopy.memberShapes();
		for (var shape in members)
		{
			members[shape].group=groupcopy;
			temps=[];
			temps[0]=members[shape].name;
			temps[1]=members[shape].zIndex;
			shapelist.push(temps);
		}
		SELECTED[groupcopy.name]=groupcopy;
		SELECTEDSHAPE=members[shape];
		shapecopy.SELECTED[groupcopy.name]=groupcopy;
		numBoundaries++;
	}
	resetBoundary();
	for (var name in SELECTED)
	{
		SELECTED[name].drawBoundary(SELECTED[name].type, Object.keys(SELECTED).length);
		shapecopy.isSelector = true;
		numselected++;
	}
	shapelist.sort(zindp);

	var actions = [];
	for (var i=0; i<shapelist.length;i++)
	{
		name=shapelist[i][0];
		SHAPES[name].zIndex=ZPOS++;
		SHAPES[name].Canvas.style.zIndex=SHAPES[name].zIndex;
		actions.push(new CreateShapeCommand(SHAPES[name]));
	}
	if( shapelist.length > 1 )
		groupJoin();
	if (actions.length > 0 ){
		window.UndoRedo.executeCommand(actions);
	}
}
shapecopy.isSelector = false;
shapecopy.SELECTED={};

/**
* @function makeCopy
* Realiza copia de shapes
* @param  	String		shape				"Objeto Shape"
* @param		Integer		offset			"valor escalar para nuevo punto de origen del Shape"
* @param		Integer		theatre			"Objeto"
* @param  	Collect		STORE				"Colección de Shapes"
* @param		Object		groupcopy		"grupo de shapes para copia"
* @return		Object								"Objeto de la copia"
*/
function makeCopy(shape,offset,theatre,STORE,groupcopy)
{
	addTrackFunction("shape","makeCopy");
	var p,n,c1,c2;
	if (theatre.id=="shapestage")
	{
		var name="Shape"+(SCOUNT++);
	}
	else
	{
		var name="SUBSH"+(NCOUNT++);
	}
	var copy=new Shape(name,shape.title,shape.open,shape.editable,shape.type,STORE);
	if (groupcopy!=undefined && groupcopy!=null)copy.group=groupcopy;
	copy.lineWidth  = shape.lineWidth ;
	copy.lineCap  = shape.lineCap ;
	copy.lineJoin  = shape.lineJoin ;
	copy.justfill = shape.justfill;
	copy.linearfill = shape.linearfill;
	copy.stopn = shape.stopn;
	copy.shadow = shape.shadow;
	copy.shadowOffsetX  = shape.shadowOffsetX ;
	copy.shadowOffsetY  = shape.shadowOffsetY ;
	copy.shadowBlur  = shape.shadowBlur ;
	copy.zIndex = shape.zIndex;
	copy.crnradius = shape.crnradius;
	p=new Point(shape.tplftcrnr.x+offset,shape.tplftcrnr.y+offset);
	copy.tplftcrnr=p; //coordinates of top left of boundary box;
	p=new Point(shape.btmrgtcrnr.x+offset,shape.btmrgtcrnr.y+offset);
	copy.btmrgtcrnr=p; //coordinates of bottom right of boundary box;
	copy.lineWidth=shape.lineWidth;
	copy.radius=shape.radius;
	for (var i=0; i<4;i++)
	{
		copy.fillStyle[i]=shape.fillStyle[i];
		copy.strokeStyle[i]=shape.strokeStyle[i];
		copy.lineGrad[i]=shape.lineGrad[i]+offset;
		copy.shadowColor[i]=shape.shadowColor[i];
	}
	copy.radGrad[0]=shape.radGrad[0]+offset;
	copy.radGrad[1]=shape.radGrad[1]+offset;
	copy.radGrad[2]=shape.radGrad[2];
	copy.radGrad[3]=shape.radGrad[3]+offset;
	copy.radGrad[4]=shape.radGrad[4]+offset;
	copy.radGrad[5]=shape.radGrad[5];
	copy.colorStops=[];
	for (var i=0;i<shape.colorStops.length;i++)
	{
		copy.colorStops[i]=[];
		for (var j=0;j<shape.colorStops[i].length;j++)
		{
			copy.colorStops[i][j]=shape.colorStops[i][j];
		}
	}
	var node=shape.path.next;
	while (node.point.x!="end")
	{
		p=new Point(node.point.x+offset,node.point.y+offset);
		if (node.ctrl1.x=="non")
		{
			c1=new Point("non","non");;
		}
		else
		{
			c1=new Point(node.ctrl1.x+offset,node.ctrl1.y+offset);
		}
		if (node.ctrl2.x=="non")
		{
			c2=new Point("non","non");;
		}
		else
		{
			c2=new Point(node.ctrl2.x+offset,node.ctrl2.y+offset);
		}
		n=new Node(p,c1,c2);
		n.corner=node.corner;
		n.vertex=node.vertex;
		n.shape=copy;
		copy.addNode(n);
		node=node.next;
	}
	switch (copy.type)
	{
		case "arc":
		case "sector":
		case "segment":
		node=copy.path.next;
		copy.bnode=node.next;
		copy.lnode=node.next.next;
		copy.tnode=node.next.next.next;
		copy.arcwidth=shape.arcwidth;
		copy.archeight=shape.archeight;
		copy.radius=shape.radius;
		copy.arccentre.x=shape.arccentre.x+offset;
		copy.arccentre.y=shape.arccentre.y+offset;
		break;

		case "text":
		case "text_bold":
		case "text_italic":
		// Mejora 1. Texto con posicionamiento correcto en el PNG
		// Copia con los mismos parámetros que el original
			var textStyle = {
				text:{
					fontWeight: 'normal',
					fontStyle: 'normal'
				},
				text_italic:{
					fontWeight: 'normal',
					fontStyle: 'italic'
				},
				text_bold:{
					fontWeight: 'bold',
					fontStyle: 'normal'
				}
			};
			copy.addTo(theatre);
			// copy.drawEnd(copy.path.prev.prev.prev.point);
			var self=copy;
			copy.textValue = shape.inputText.value();
			copy.widthInput = self.widthInput;
			var inputText = new CanvasInput({
				canvas: copy.Canvas,
				maxlength: "ORÁCULO MATEMÁGICO".length,
				x : copy.path.next.point.x,
				y : copy.path.next.point.y,
				fontSize: STANDARFONTSIZE,
				fontFamily: 'Arial',
				fontColor: 'rgba('+copy.fillStyle[0]+','+copy.fillStyle[1]+','+
				copy.fillStyle[2]+','+copy.fillStyle[3]+')',
				fontWeight: textStyle[copy.type].fontWeight,
				backgroundColor: 'rgba(0, 0, 0, 0)',
				fontStyle: textStyle[copy.type].fontStyle,
				width: 300,
				padding: 8,
				borderWidth: 0,
				borderColor: 'rgba(0, 0, 0, 0)',
				borderRadius: 0,
				boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
				innerShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
				value: copy.textValue,
				// Mejora 1. Texto con width de acuerdo a la longitud del texto:
				shape: copy,
				onfocus: function()
				{
					self.focusState = 1;
					self.textPrevValue = this.value();
				},
				onblur: function()
				{
					self.focusState = 0;
					self.textValue = this.value();
					if (self.textPrevValue != null && self.textPrevValue != this.value())
					{
						var actions = [];
						actions.push(new editInputTextCommand(self));

						if (actions.length > 0 )
						{
							window.UndoRedo.executeCommand(actions);
						}
						self.textPrevValue = null;
					}
				}
			});
			copy.inputText = inputText;
		break;

		/*copy.addTo(theatre);
		// copy.drawEnd(copy.path.prev.prev.prev.point);
		var self=copy;
		copy.textValue=shape.inputText.value();
		copy.widthInput = (Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x)>100)?Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x):100;
		var inputText = new CanvasInput({
			canvas: copy.Canvas,
			maxlength: 20,
			x : copy.path.next.point.x,
			y : copy.path.next.point.y,
			fontSize: 18,
			fontFamily: 'Arial',
			fontWeight: 'normal',
			fontStyle: 'normal',
			fontColor: 'rgba('+copy.fillStyle[0]+','+copy.fillStyle[1]+','+
			copy.fillStyle[2]+','+copy.fillStyle[3]+')',
			backgroundColor: 'rgba(0, 0, 0, 0)',
			width: copy.widthInput,
			padding: 8,
			borderWidth: 0,
			borderColor: 'rgba(0, 0, 0, 0)',
			borderRadius: 0,
			boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			innerShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			value: copy.textValue,
		});
		copy.inputText = inputText;//asigna el objeto al atributo del Shape
		break;
		case "text_bold":
		copy.addTo(theatre);
		copy.drawEnd(copy.path.prev.prev.prev.point);
		var self=copy;
		copy.textValue=shape.inputText.value();
		copy.widthInput = (Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x)>100)?Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x):100;
		var inputText = new CanvasInput({
			canvas: copy.Canvas,
			maxlength: 20,
			x : copy.path.next.point.x,
			y : copy.path.next.point.y,
			fontSize: 18,
			fontFamily: 'Arial',
			fontColor: 'rgba('+copy.fillStyle[0]+','+copy.fillStyle[1]+','+
			copy.fillStyle[2]+','+copy.fillStyle[3]+')',
			fontWeight: 'bold',
			fontStyle: 'normal',
			backgroundColor: 'rgba(0, 0, 0, 0)',
			width: copy.widthInput,
			padding: 8,
			borderWidth: 0,
			borderColor: 'rgba(0, 0, 0, 0)',
			borderRadius: 0,
			boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			innerShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			value: copy.textValue,
			onfocus: function(){
				self.focusState = 1;
				self.textPrevValue = this.value();
			},
			onblur: function(){
				self.focusState = 0;
				self.textValue = this.value();
				if (self.textPrevValue != null && self.textPrevValue != this.value()){
					var actions = [];
					actions.push(new editInputTextCommand(self));

					if (actions.length > 0 ){
						window.UndoRedo.executeCommand(actions);
					}
					self.textPrevValue = null;
				}
			}
		});
		copy.inputText = inputText;//asigna el objeto al atributo del Shape
		break;
		case "text_italic":
		copy.addTo(theatre);
		copy.drawEnd(copy.path.prev.prev.prev.point);
		var self=copy;
		copy.textValue=shape.inputText.value();
		copy.widthInput = (Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x)>100)?Math.abs(copy.btmrgtcrnr.x - copy.tplftcrnr.x):100;
		var inputText = new CanvasInput({
			canvas: copy.Canvas,
			maxlength: 20,
			x : copy.path.next.point.x,
			y : copy.path.next.point.y,
			fontSize: 18,
			fontFamily: 'Arial',
			fontColor: 'rgba('+copy.fillStyle[0]+','+copy.fillStyle[1]+','+
			copy.fillStyle[2]+','+copy.fillStyle[3]+')',
			backgroundColor: 'rgba(0, 0, 0, 0)',
			fontWeight: 'normal',
			fontStyle: 'italic',
			width: copy.widthInput,
			padding: 8,
			borderWidth: 0,
			borderColor: 'rgba(0, 0, 0, 0)',
			borderRadius: 0,
			boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			innerShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
			value: copy.textValue,
			onfocus: function(){
				self.focusState = 1;
				self.textPrevValue = this.value();
			},
			onblur: function(){
				self.focusState = 0;
				self.textValue = this.value();
				if (self.textPrevValue != null && self.textPrevValue != this.value()){
					var actions = [];
					actions.push(new editInputTextCommand(self));

					if (actions.length > 0 ){
						window.UndoRedo.executeCommand(actions);
					}
					self.textPrevValue = null;
				}
			}
		});
		copy.inputText = inputText;//asigna el objeto al atributo del Shape
		break;*/
	}
	if (copy.type!="text" && copy.type!="text_bold" && copy.type!="text_italic")
		copy.addTo(theatre);
	return copy;
}

/**
* @function zindp
* Posición en la lista de capas de shape
* @param		Array		a			""
* @param		Array		b			""
* @return		Integer					"Objeto de la copia"
*/
function zindp(a,b)
{
	addTrackFunction("shape","zindp");
	return a[1]-b[1]
}

/**
* @function insertNodeBefore
* Inserta nodo antes de otro
* @param		Object		node			"Nuevo nodo a insertar"
* @return		undefined					"No devuelve valor"
*/
function insertNodeBefore(node)
{
	addTrackFunction("shape","insertNodeBefore");
	node.prev=this.prev;
	node.next=this;
	this.prev.next=node;
	this.prev=node;
}

/**
* @function getAngle
* Retorna valor del angulo actual del Shape
* @return		Integer							"Valor del ángulo del Shape"
*/
function getAngle() //angle from current centre
{
	addTrackFunction("shape","getAngle");
	return arctan(this.point.y,this.point.x);
}

/**
* @function getAngleTo
* Retorna angulo relativo al centro actual del Shape
* @param		Object			node		"Nodo referencial"
* @return		undefined						"No devuelve valor"
*/
function getAngleTo(node)  //relative to current centre
{
	addTrackFunction("shape","getAngleTo");
	var theta = node.getAngle()-this.getAngle();
	if (theta<0)
	{
		theta+=2*Math.PI;
	}
	return theta;
}

/**
* @function rotate
* Actualiza valores de posición de los Marks(point,ctrl1,ctrl2) en la rotación
* @param		Integer			theta		"Valor del ángulo"
* @return		undefined						"No devuelve valor"
*/
function rotate(theta) //about origin
{
	addTrackFunction("shape","rotate");

	var px=this.point.x*Math.cos(theta)-this.point.y*Math.sin(theta);
	var py=this.point.x*Math.sin(theta)+this.point.y*Math.cos(theta);
	this.point.x=px;
	this.point.y=py;
	if (this.ctrl1.x!="non")
	{
		var c1x=this.ctrl1.x*Math.cos(theta)-this.ctrl1.y*Math.sin(theta);
		var c1y=this.ctrl1.x*Math.sin(theta)+this.ctrl1.y*Math.cos(theta);
		var c2x=this.ctrl2.x*Math.cos(theta)-this.ctrl2.y*Math.sin(theta);
		var c2y=this.ctrl2.x*Math.sin(theta)+this.ctrl2.y*Math.cos(theta);
		this.ctrl1.x=c1x;
		this.ctrl1.y=c1y;
		this.ctrl2.x=c2x;
		this.ctrl2.y=c2y;
	}
}

/**
* @function scaleY
* Actualiza valores a los Maks(point,ctrl1,ctrl2) en el eje de la abscisa Y
* @param		Integer			s		"Valor de la escala"
* @return		undefined						"No devuelve valor"
*/
function scaleY(s)
{
	addTrackFunction("shape","scaleY");
	this.point.y=s*this.point.y;
	if (this.ctrl1.x!="non")
	{
		this.ctrl1.y=s*this.ctrl1.y;
		this.ctrl2.y=s*this.ctrl2.y;
	}
}

/**
* @function translate
* Actualiza valores de posición de los Marks(point,ctrl1,ctrl2) en la traslación del Shape
* @param		Integer			x		"Valor en la abscisa x"
* @param		Integer			y		"Valor en la abscisa y"
* @return		undefined						"No devuelve valor"
*/
function translate(x,y)
{
	addTrackFunction("shape","translate");
	this.point.x-=x;
	this.point.y-=y;
	if (this.ctrl1.x!="non")
	{
		this.ctrl1.x-=x;
		this.ctrl1.y-=y;
		this.ctrl2.x-=x;
		this.ctrl2.y-=y;
	}
}

/**
* @function copyNodeTo
* Copia valores del nodo al objeto(this)
* @param		Object			node		"Objecto Node a copiar"
* @return		undefined						"No devuelve valor"
*/
function copyNodeTo(node)
{
	addTrackFunction("shape","copyNodeTo");
	node.point.x=this.point.x;
	node.point.y=this.point.y;
	node.ctrl1.x=this.ctrl1.x;
	node.ctrl1.y=this.ctrl1.y;
	node.ctrl2.x=this.ctrl2.x;
	node.ctrl2.y=this.ctrl2.y;
	node.vertex=this.vertex;
	node.corner=this.corner;
	node.shape=this.shape;
}

//eliminar
function setNodePathBox()
{
	addTrackFunction("shape","setNodePathBox");
	document.getElementById("twnpbutton").OKnode=this;
	document.getElementById("tweditline").value=this.nodepath.nodeTweening.twtime;
	document.getElementById("twrepeditline").value=this.nodepath.nodeTweening.repeat;
	document.getElementById("twyoeditline").checked=this.nodepath.nodeTweening.yoyo;
	document.getElementById("twacteditline").checked=this.nodepath.nodeTweening.active;
	document.getElementById("tweenpathsbox").style.visibility="visible";
}

/**
* @function addAllMarks
* Agrega todos los nodos al vertice (point,ctrl1,ctrl2)
* @return		undefined				"No devuelve valor"
*/
function addAllMarks()
{
	addTrackFunction("shape","addAllMarks");
	node=this.path.next;
	if (this.type=="curve" || this.type=="curve_dotted")
	{
		node.addPointMark();
	}
	node=node.next;
	while (node.point.x!="end")
	{
		if (node.vertex=="L")
		{
			node.addPointMark();
		}
		else
		{
			node.addFullMarks();
		}
		node=node.next;
	}
}

/**
* @function getTypeTool
* Retorna identifador de tipo de menu de acuerdo a la cantidad de shapes seleccionados
* @return		Integer				"identificador de grupo de menu"
*/
function getTypeTool()
{
	var result = 0;

	if ( Object.keys(SELECTED).length == 1)
	{
		Object.keys(SELECTED).forEach(function (key)
		{
			result = getShapeGroup( SELECTED[key].members[0] );
			return;
		});
		return result;
	}
	else if (Object.keys(SELECTED).length > 1)
	{
		return 5;
	}
	else {
		return result;
	}
}

/**
* @function getShapeGroup
* Retorna identifador de grupo shapes seleccionado
* @return		Integer				"identificador de grupo"
*/
function getShapeGroup(shape)
{
	var type = shape.type;
	var open = shape.open;

	if ( type=='rectangle' ||
		type=='ellipse' ||
		type=='rounded_rectangle' ||
		type=='triangle'){
		return 1;
	}
	else if ( 	(type=='curve' && open==false) ||
		(type=='curve_dotted' && open==false) ||
		(type=='freeform_dotted' && open==false) ||
		(type=='freeform' && open==false))
	{
		return 2;
	}
	else if ( 	(type=='curve' && open==true) ||
		(type=='curve_dotted' && open==true) ||
		(type=='freeform_dotted' && open==true) ||
		(type=='freeform' && open==true) )
	{
		return 3;
	}
	else if (	type=='text' ||
		type=='text_italic' ||
		type=='text_bold')
	{
		return 4;
	}
	else if (type=='flecha' ||
		type=='cota'){
		return 6;
	}
	else {
		return 5;
	}
}

/**
* @function drawEndOut
* Finaliza el dibujado al hacer clic fuera del área de trabajo
* @return		undefined				"No devuelve Valor"
*/
function drawEndOut()
{
	addTrackFunction("shape","drawEndOut");
	if (Object.keys(DRAWING).length > 0){
		if ( !(DRAWING.shapeDrawing.type=="curve" || DRAWING.shapeDrawing.type=="freeform" ||
			DRAWING.shapeDrawing.type=="curve_dotted" || DRAWING.shapeDrawing.type=="freeform_dotted" || DRAWING.shapeDrawing.type=="text" ||
			DRAWING.shapeDrawing.type=="text_bold" || DRAWING.shapeDrawing.type=="text_italic" ) ){
			DRAWING.shapeDrawing.drawEnd(DRAWING.position);
		}
		else if (DRAWING.shapeDrawing.type=="curve" || DRAWING.shapeDrawing.type=="curve_dotted" ||
			DRAWING.shapeDrawing.type=="freeform" || DRAWING.shapeDrawing.type=="freeform_dotted")
		{
			DRAWING.shapeDrawing.path.prev.removeNode();
			DRAWING.shapeDrawing.drawEnd(DRAWING.position);
			DRAWING.shapeDrawing = null;
		}
	}
	DRAWING = {};
	removeRotate();
}

/**
* @function selector
* Dispone editor para hacer selección
* @return		undefined				"No devuelve Valor"
*/
function selector()
{
	addTrackFunction("init","selector");
	document.getElementById("markerdrop").style.visibility="visible";
}

/**
* @function resizewidthinput
* Mejora 1. Texto con width de acuerdo a la longitud del texto:
* Método solo para Shape de tipo text, text_bold, text_italic
* Dibuja el nuevo ancho de la caja de texto de acuerdo al widthInput, 
* se refleja en los boundaries de grupo y del shape, también en los nodos del shape
* Además actualiza ciertos parámetros del Shape
* Solo es utilizado por el CanvasInput.js/widthUpdate()
* Con respecto al código se basó en los códigos de:
* boundary.rh.DD.onMouseDrag y boundary.rh.DD.onMouseUp
* Ya que hacen la misma función solo que esta es más específica
* @param		Integer		widthInput		"Valor numérico del width de la caja de texto"
* @return		undefined				"No devuelve Valor"
*/
function resizewidthinput(widthInput){

	var group = this.group;
	var shape = this;
	var scale = (widthInput)/group.width;
	var boundary=group.boundary;

	// Actualización de boundaries
	if( group.boundary != undefined ){
		justSelectedBoundaries();
		for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++)
		{
			boundary=document.getElementById("boundarydrop").childNodes[i];
			boundary.style.width=Math.round(boundary.group.width*scale/xgrid)*xgrid+"px";
		}
	}
	
	// Actualización de ejes
	shape.btmrgtcrnr.x=group.left+(shape.btmrgtcrnr.x-group.left)*scale;
	shape.arcwidth*=scale;
	shape.radius*=scale;
	shape.arccentre.x=group.left+(shape.arccentre.x-group.left)*scale;
	node=shape.path.next;
	
	// Actualización de los nodos
	while(node.point.x!="end")
	{
		node.point.x=group.left+(node.point.x-group.left)*scale;
		if(node.ctrl1.x!="non")// will also scale when curve has been set as straight line
		{
			node.ctrl1.x=group.left+(node.ctrl1.x-group.left)*scale;
			node.ctrl2.x=group.left+(node.ctrl2.x-group.left)*scale;
		}
		node=node.next;
	}
	addTrackFunction("shape","drawText");
	this.Canvas.ctx.clearRect(-SCRW,-SCRH,2*SCRW,2*SCRH);

	// Redibujar en el canvas
	var rule='rgba('
	for (var j=0;j<3;j++)
	{
		rule += this.strokeStyle[j]+',';
	}
	rule +=this.strokeStyle[j]+')';
	this.Canvas.ctx.strokeStyle=rule;
	this.Canvas.ctx.lineWidth = this.lineWidth;
	this.Canvas.ctx.lineCap = this.lineCap;
	this.Canvas.ctx.lineJoin = this.lineJoin;

	if(this.dotted){
		this.Canvas.ctx.setLineDash([this.linedasha, this.linedashb]);
	}
	else {
		this.Canvas.ctx.setLineDash([]);
	}

	this.Canvas.ctx.beginPath();
	var node=this.path.next;
	this.Canvas.ctx.moveTo(node.point.x,node.point.y);
	while (node.next.point.x !="end")
	{
		node=node.next;
		if (node.vertex=="L")
		{
			this.Canvas.ctx.lineTo(node.point.x,node.point.y);
		}
		else
		{
			this.Canvas.ctx.bezierCurveTo(node.ctrl1.x,node.ctrl1.y,node.ctrl2.x,node.ctrl2.y,node.point.x,node.point.y)
		}
	}
	if (!this.open)
	{
		this.Canvas.ctx.closePath()
	}
	group.update(group.left-PNLLEFT,group.top-PNLTOP,0,0,scale,1);

	// Actualización de parámetros
	this.textValue = (this.inputText)?this.inputText.value():"";
	this.widthInput = widthInput;
}