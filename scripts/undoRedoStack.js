/**
* @class CommandManager
*
* @param	Integer		max_undo		"Dimensión de la pila"
* @return	this									"Retorna el objeto"
*/
CommandManager = function(max_undo)
{
	max_undo = max_undo || 30;

	this.undo_stack = [];
	this.redo_stack = [];

	this.executeCommand = function(cmd)
	{
		if (cmd.length > 0)
		{
			for (var i = 0; i < cmd.length; i++)
			{
				cmd[i].execute();
			}
		}

		if (this.undo_stack.length >= max_undo)
		{
			this.undo_stack.shift();
		}

		this.undo_stack.push(cmd);
		this.redo_stack = [];
		fireAlert();
	}

	this.undoCommand = function()
	{
		var cmd = this.undo_stack.pop();

		if (cmd)
		{
			if (cmd.length > 0 )
			{
				resetBoundary();
				for (var i = 0; i < cmd.length; i++)
				{
					cmd[i].undo();
				}
				this.redo_stack.push(cmd);
			}
		}
		fireAlert();
	}

	this.redoCommand = function()
	{
		var cmd = this.redo_stack.pop();

		if (cmd)
		{
			if (cmd.length > 0 )
			{
				resetBoundary();
				for (var i = 0; i < cmd.length; i++)
				{
					cmd[i].redo();
				}
				this.undo_stack.push(cmd);
			}
		}
		fireAlert();
	}
}

/**
* @function resetUndoRedo
* Reinicia las pilas de acciones
* @return		undefined		"No devuelve ningun valor"
*/
function resetUndoRedo()
{
	addTrackFunction("undoRedoStack","resetUndoRedo");
	UndoRedo = new CommandManager();
}

/**
* @function fireAlert
* Oculta o muestra mensaje de advertencia si algún nodo de alguna figura está fuera del área de trabajo
* @return		undefined		"No devuelve ningun valor"
*/
function fireAlert()
{
	if ( checkNodeAlert() )
	{
		document.getElementById("warningEditor").style.visibility = "visible";
	}
	else {
		document.getElementById("warningEditor").style.visibility = "hidden";
	}
}

/**
* @function undoFunction
* Ejecuta la función undoCommand() del objeto UndoRedo;
* @return		undefined		"No devuelve ningun valor"
*/
function undoFunction()
{
	addTrackFunction("eventKey","undoFunction");
	ungroup();
	window.UndoRedo.undoCommand();
}

/**
* @function redoFunction
* Ejecuta la función redoCommand() del objeto UndoRedo;
* @return		undefined		"No devuelve ningun valor"
*/
function redoFunction()
{
	addTrackFunction("eventKey","redoFunction");
	ungroup();
	window.UndoRedo.redoCommand();
}

/**
* @function emptyRedoStack
* Verifica si la pila de acciones Redo está vacía o no
* @return		Bollean		"True:pila vacía, False: pila con elementos"
*/
function emptyRedoStack()
{
	return window.UndoRedo.redo_stack.length == 0;
}

/**
* @function emptyUndoStack
* Verifica si la pila de acciones Undo está vacía o no
* @return		Bollean		"True:pila vacía, False: pila con elementos"
*/
function emptyUndoStack()
{
	return window.UndoRedo.undo_stack.length == 0;
}

/**
* @function emptyStack
* Verifica si las pilas de acciones estan vacías o no
* @return		Boolean		"True:pilas vacía, False: pilas con elementos"
*/
function emptyStack()
{
	if (window.UndoRedo.undo_stack.length == 0 && window.UndoRedo.redo_stack.length == 0 )
	{
		return true;
	}
	else return false;
}

/**
* @function lengthUndoStack
* Obtiene el tamaño de la pila de acciones Undo
* @return		Integer		"Dimensión de la pila de acciones Undo"
*/
function lengthUndoStack()
{
	return window.UndoRedo.undo_stack.length;
}

var UndoRedo = new CommandManager();
