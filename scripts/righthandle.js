/**
* @class RightH
*
* @param		Object			parentdiv		"Elemento para asociar RightH"
* @return		undefined								"No devuelve ningun valor"
*/
function RightH(parentdiv, boundary)
{
	addTrackFunction("righthandle","RightH");

	if (document.getElementById('righth'))
	{
		document.getElementById('righth').parentNode.removeChild(document.getElementById('righth'))
	};

	this.elmRef = document.createElement('div');
	this.elmRef.id  = 'righth'+boundary.id;
	this.elmRef.className ='resizeElement';
	this.elmRef.style.position='absolute';
	this.elmRef.style.left= (/*parseInt(boundary.style.left) +*/ parseInt(boundary.style.width)-MINSTEP)+"px";
	this.elmRef.style.top= (/*parseInt(boundary.style.top) +*/ parseInt(boundary.style.height)/2-MINSTEP)+"px";
	this.elmRef.style.cursor='e-resize';
	this.elmRef.style.fontSize=0;
	this.elmRef.style.width=10+"px";
	this.elmRef.style.height=10+"px";
	this.elmRef.style.zIndex=1000;
	this.elmRef.parentdiv=parentdiv;
	this.elmRef.style.backgroundColor="white";
	this.elmRef.style.border="solid black 1px";
	this.elmRef.parentdiv.appendChild(this.elmRef);
	return this.elmRef;
}
