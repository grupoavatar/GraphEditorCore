/**
 * @class Colors
 * Actualiza los colores predeterminados para linea y boundary del shape
 * @return		undefined										"No devuelve ningun valor"
 */
﻿var Colors = new function()
{

	/**
	 * @function ColorFromHSV
	 * funcion para crear una nueva instancia de la clase Color 
	 * con tono (hue), saturacion y valor
	 * @param   Integer    hue       "tono del color"
	 * @param   Integer    sat       "saturacion"
	 * @param   Integer    val       "valor"
	 * @return  Color      color     "instacia de la clase Color"
	 */
	this.ColorFromHSV = function(hue, sat, val)
	{
		addTrackFunction("colorpicker","ColorFromHSV");
		var color = new Color();
		color.SetHSV(hue,sat,val);
		return color;
	}

	/**
	 * @function ColorFromRGB
	 * funcion para crear una nueva instancia de la clase Color con los valores RGB
	 * @param   Integer    r       "valor del color rojo"
	 * @param   Integer    g       "valor del color verde"
	 * @param   Integer    b       "valor del color azul"
	 * @return  Color      color   "instacia de la clase Color"
	 */
	this.ColorFromRGB = function(r, g, b)
	{
		addTrackFunction("colorpicker","ColorFromRGB");
		var color = new Color();
		color.SetRGB(r,g,b);
		return color;
	}

	/**
	 * @function ColorFromHex
	 * funcion para crear una nueva instancia de la clase Color a partir de una secuencia hexadecimal
	 * @param   String     hexStr  "valor hexadecimal"
	 * @return  Color      color   "instacia de la clase Color"
	 */
	this.ColorFromHex = function(hexStr)
	{
		addTrackFunction("colorpicker","ColorFromHex");
		var color = new Color();
		color.SetHexString(hexStr);
		return color;
	}

	/**
	 * @class Color
	 * Representa un color con valores RGB, hue y saturacion.
	 * @return		NULL	    "No devuelve ningun valor"
	 */
	function Color()
	{
		//Stored as values between 0 and 1
		var red        = 0;
		var green      = 0;
		var blue       = 0;

		//Stored as values between 0 and 360
		var hue        = 0;

		//Strored as values between 0 and 1
		var saturation = 0;
		var value      = 0;

		/**
		 * @function SetRGB
		 * funcion para establecer los valores de rojo, verde y azul del color
		 * @param   Integer    r       "valor del color rojo"
		 * @param   Integer    g       "valor del color verde"
		 * @param   Integer    b       "valor del color azul"
		 * @return  Boolean            "si se logro asignar los valores correctamente"
		 */
		this.SetRGB = function(r, g, b)
		{
			addTrackFunction("colorpicker","SetRGB");

			if (isNaN(r) || isNaN(g) || isNaN(b))
				return false;

			r = r/255.0;
			red = r > 1 ? 1 : r < 0 ? 0 : r;
			g = g/255.0;
			green = g > 1 ? 1 : g < 0 ? 0 : g;
			b = b/255.0;
			blue = b > 1 ? 1 : b < 0 ? 0 : b;

			calculateHSV();
			return true;
		}

		/**
		 * @function Red
		 * funcion para obtener el valor rojo del objeto
		 * @return 		Integer 		"rojo del objeto Color"
		 */
		this.Red = function()
		{ 
			addTrackFunction("colorpicker","Red");
			return Math.round(red*255); 
		}

		/**
		 * @function Green
		 * funcion para obtener el valor verde del objeto
		 * @return 		Integer 		"verde del objeto Color"
		 */
		this.Green = function()
		{ 
			addTrackFunction("colorpicker","Green");
			return Math.round(green*255); 
		}

		/**
		 * @function Blue
		 * funcion para obtener el valor azul del objeto
		 * @return 		Integer 		"azul del objeto Color"
		 */
		this.Blue = function()
		{ 
			addTrackFunction("colorpicker","Blue");
			return Math.round(blue*255);
		}

		/**
		 * @function SetHSV
		 * funcion para establecer los valores de tono (hue), saturacion y valor
		 * @param     Integer    h    "tono del color"
		 * @param     Integer    s    "saturacion"
		 * @param     Integer    v    "valor"
		 * @return 		Boolean    	    "si se logro asignar los valores correctamente"
		 */
		this.SetHSV = function(h, s, v)
		{
			addTrackFunction("colorpicker","SetHSV");

			if (isNaN(h) || isNaN(s) || isNaN(v))
				return false;

			hue = (h >= 360) ? 359.99 : (h < 0) ? 0 : h;
			saturation = (s > 1) ? 1 : (s < 0) ? 0 : s;
			value = (v > 1) ? 1 : (v < 0) ? 0 : v;
			calculateRGB();

			return true;
		}

		/**
		 * @function Hue
		 * funcion para obtener el tono del color
		 * @return 		Number 		"tono del objeto Color"
		 */
		this.Hue = function()
		{
			addTrackFunction("colorpicker","Hue");
			return hue; 
		}

		/**
		 * @function Saturation
		 * funcion para obtener el saturacion del color
		 * @return 		Number 		"saturacion del objeto Color"
		 */
		this.Saturation = function()
		{
			addTrackFunction("colorpicker","Saturation");
			return saturation; 
		}

		/**
		 * @function Value
		 * funcion para obtener el valor del color
		 * @return 		Number 		"valor del objeto Color"
		 */
		this.Value = function()
		{
			addTrackFunction("colorpicker","Value");
			return value; 
		}

		/**
		 * @function SetHexString
		 * funcion para actualizar los valores de rgb
		 * @param String     colorName   "nombre del nuevo color, el cual es una llave en la variable colores"
		 * @return           NULL        "no devuelve ningun valor"
		 */
		this.SetHexString = function(hexString)
		{
			addTrackFunction("colorpicker","SetHexString");
			if(hexString == null || typeof(hexString) != "string")
				return false;

			if (hexString.substr(0, 1) == '#')
				hexString = hexString.substr(1);

			if(hexString.length != 6)
				return false;

			var r = parseInt(hexString.substr(0, 2), 16);
			var g = parseInt(hexString.substr(2, 2), 16);
			var b = parseInt(hexString.substr(4, 2), 16);

			return this.SetRGB(r,g,b);
		}

		/**
		 * @function HexString
		 * funcion para generar una cadena hezadecimal con los valores de rojo, verde y azul
		 * @return 		String 		"cadena haxadecimal en mayusculas"
		 */
		this.HexString = function()
		{
			addTrackFunction("colorpicker","HexString");

			var rStr = this.Red().toString(16);
			if (rStr.length == 1)
				rStr = '0' + rStr;
			var gStr = this.Green().toString(16);
			if (gStr.length == 1)
				gStr = '0' + gStr;
			var bStr = this.Blue().toString(16);
			if (bStr.length == 1)
				bStr = '0' + bStr;

			return ('#' + rStr + gStr + bStr).toUpperCase();
		}

		/**
		 * @function Complement
		 * funcion para crear una instancia de la clase color con el 
		 * tono, el valor y la saturacion complemento del objeto actual
		 * @return  Color 		newColor 		"color complemento"
		 */
		this.Complement = function()
		{
			addTrackFunction("colorpicker","Complement");

			var newHue   = (hue>= 180) ? hue - 180 : hue + 180;
			var newVal   = (value * (saturation - 1) + 1);
			var newSat   = (value*saturation) / newVal;
			var newColor = new Color();
			newColor.SetHSV(newHue, newSat, newVal);

			return newColor;
		}

		/**
		 * @function calculateHSV
		 * funcion para calcular el valor, la saturacion y el tono (hue)
		 * de acuerdo a los valores de rojo, verde y azul
		 * @return           NULL        "no devuelve ningun valor"
		 */
		function calculateHSV()
		{
			var max = Math.max(Math.max(red, green), blue);
			var min = Math.min(Math.min(red, green), blue);

			value = max;

			saturation = 0;
			if(max != 0)
				saturation = 1 - min/max;

			hue = 0;
			if(min == max)
				return;

			var delta = (max - min);
			if (red == max)
				hue = (green - blue) / delta;
			else if (green == max)
				hue = 2 + ((blue - red) / delta);
			else
				hue = 4 + ((red - green) / delta);
			hue = hue * 60;
			if(hue <0)
				hue += 360;
		}

		/**
		 * @function calculateRGB
		 * funcion para calcular el valor de rojo, verde y azul en base al tono, valor y saturacion
		 * @return           NULL        "no devuelve ningun valor"
		 */
		function calculateRGB()
		{
			red = value;
			green = value;
			blue = value;

			if(value == 0 || saturation == 0)
				return;

			var tHue = (hue / 60);
			var i = Math.floor(tHue);
			var f = tHue - i;
			var p = value * (1 - saturation);
			var q = value * (1 - saturation * f);
			var t = value * (1 - saturation * (1 - f));
			switch(i)
			{
				case 0:
				red = value; green = t; blue = p;
				break;
				case 1:
				red = q; green = value; blue = p;
				break;
				case 2:
				red = p; green = value; blue = t;
				break;
				case 3:
				red = p; green = q; blue = value;
				break;
				case 4:
				red = t; green = p; blue = value;
				break;
				default:
				red = value; green = p; blue = q;
				break;
			}
		}
	}
}
();

/**
 * funcion depreciada
 */
function colorChanged(source)
{
	addTrackFunction("colorpicker", "colorChanged");

	document.getElementById("hexBox").value = currentColor.HexString();
	document.getElementById("redBox").value = currentColor.Red();
	document.getElementById("greenBox").value = currentColor.Green();
	document.getElementById("blueBox").value = currentColor.Blue();
	document.getElementById("hueBox").value = Math.round(currentColor.Hue());
	var str = (currentColor.Saturation()*100).toString();
	if (str.length > 4)
		str = str.substr(0,4);
	document.getElementById("saturationBox").value = Math.round(parseInt(str));
	str = (currentColor.Value()*100).toString();
	if (str.length > 4)
		str = str.substr(0,4);
	document.getElementById("valueBox").value = Math.round(parseInt(str));

	if (source == "arrows" || source == "box")
	{
		document.getElementById("gradientBox").style.backgroundColor = Colors.ColorFromHSV(currentColor.Hue(), 1, 1).HexString();
	}

	if (source == "box")
	{
		var el = document.getElementById("arrows");
		el.style.top = (256 - currentColor.Hue()*255/359.99 - 4) + 'px';
		var circlex = currentColor.Value()*255;
		var circley = (1-currentColor.Saturation())*255;
		document.getElementById('circle').style.left=(circlex-5)+"px";
		document.getElementById('circle').style.top=(circley-5)+"px";
		endMovement();
	}

	document.getElementById("quickColor").style.backgroundColor = currentColor.HexString();
	document.getElementById("transpslider").style.backgroundColor = currentColor.HexString();
}

/**
 * funcion depreciada
 */
function endMovement()
{
	document.getElementById("staticColor").style.backgroundColor = currentColor.HexString();
}

/**
 * funcion depreciada
 */
function hexBoxChanged(e)
{
	currentColor.SetHexString(document.getElementById("hexBox").value);
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function redBoxChanged(e)
{
	currentColor.SetRGB(parseInt(document.getElementById("redBox").value), currentColor.Green(), currentColor.Blue());
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function greenBoxChanged(e)
{
	currentColor.SetRGB(currentColor.Red(), parseInt(document.getElementById("greenBox").value), currentColor.Blue());
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function blueBoxChanged(e)
{
	currentColor.SetRGB(currentColor.Red(), currentColor.Green(), parseInt(document.getElementById("blueBox").value));
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function hueBoxChanged(e)
{
	currentColor.SetHSV(parseFloat(document.getElementById("hueBox").value), currentColor.Saturation(), currentColor.Value());
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function saturationBoxChanged(e)
{
	currentColor.SetHSV(currentColor.Hue(), parseFloat(document.getElementById("saturationBox").value)/100.0, currentColor.Value());
	colorChanged("box");
}

/**
 * funcion depreciada
 */
function valueBoxChanged(e)
{
	currentColor.SetHSV(currentColor.Hue(), currentColor.Saturation(), parseFloat(document.getElementById("valueBox").value)/100.0);
	colorChanged("box");
}

/**
 * @function fixPNG
 * funcion para arreglar el HTML de la imagen PNG de acuerdo a la version del navegador
 * @param   Object     obj         "referencia a la imagen"
 * @return             NULL        "no devuelve ningun valor"
 */
function fixPNG(myImage)
{
	addTrackFunction("colorpicker", "fixPNG");

	if (!document.body.filters)
		return;
	var arVersion = navigator.appVersion.split("MSIE");
	var version = parseFloat(arVersion[1]);
	if (version < 5.5 || version >= 7)
		return;

	var imgID = (myImage.id) ? "id='" + myImage.id + "' " : ""
	var imgStyle = "display:inline-block;" + myImage.style.cssText
	var strNewHTML = "<span " + imgID
	+ " style=\"" + "width:" + myImage.width
	+ "px; height:" + myImage.height
	+ "px;" + imgStyle + ";"
	+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
	+ "(src=\'" + myImage.src + "\', sizingMethod='scale');\"></span>"
	myImage.outerHTML = strNewHTML
}

/**
 * @function fixGradientImg
 * funcion para llamar a fixPNG con el elemento de ID gradientImg
 * @return 		         NULL 		   "no devuelve ningun valor"
 */
function fixGradientImg()
{
	addTrackFunction("colorpicker", "fixGradientImg");
	fixPNG(document.getElementById("gradientImg"));
}

/**
 * @function setColor
 * funcion para actualizar el color de las figuras seleccionadas
 * @param String     colorName   "nombre del nuevo color, el cual es una llave en la variable colores"
 * @return           NULL        "no devuelve ningun valor"
 */
function setColor(colorName)
{
	addTrackFunction("colorpicker", "setColor");

	coltype = 'F';
	if (coltype=='F')
	{
		var actions = [];
		for (var groupName in SELECTED)
		{
			var group=SELECTED[groupName];

			var shapeNames=group.memberShapes();
			for (var name in shapeNames)
			{
				shape=shapeNames[name];
				var shapeOld = {};
				shapeOld.justfill = shape.justfill;
				shapeOld.fillStyle = shape.fillStyle.slice(0);

				shape.justfill=true;
				shape.fillStyle = COLOR[colorName].slice(0);


				var shapeNew = {};
				shapeNew.justfill = shape.justfill;
				shapeNew.fillStyle = shape.fillStyle.slice(0);

				actions.push(new ChangeColorCommand(shape,shapeOld,shapeNew));
			}
		}
		if (actions.length > 0 )
		{
			window.UndoRedo.executeCommand(actions);
		}
		if (TWEENEDIT)
		{
			CURRENTTWEEN.fillcolour.active=true;
			CURRENTTWEEN.setTweenTimeBox();
		}
	}
}

/**
 * funcion depreciada
 */
function closeColor()
{
	document.getElementById('colorbox').style.visibility='hidden';
	if (document.getElementById('sizebox')) {document.getElementById('sizebox').parentNode.removeChild(document.getElementById('sizebox'))};
}

/**
 * funcion depreciada
 */
function fillcolor()
{
	addTrackFunction("colorpicker", "fillcolor");

	document.getElementById('gradimg').src="assets/gradfilloff.png";
	document.getElementById('colimg').src="assets/colfill.png";
	removeGradLine();
	removeRotate();
	var shape=SELECTEDSHAPE;
	document.getElementById("redBox").value=shape.fillStyle[0];
	redBoxChanged();
	document.getElementById("greenBox").value=shape.fillStyle[1];
	greenBoxChanged();
	document.getElementById("blueBox").value=shape.fillStyle[2];
	blueBoxChanged();
	alphaperct = 100*(1-shape.fillStyle[3]);
	document.getElementById('varrows').style.left=(128*alphaperct/100-4)+"px";
	document.getElementById('transptext').innerHTML ='Transparency '+Math.floor(alphaperct)+'%';
	if (EXCANVASUSED)
	{
		document.getElementById('transpslider').filters.alpha.opacity=100-alphaperct;
	}
	else
	{
		document.getElementById('transpslider').style.opacity=1-alphaperct/100;
	}
	document.getElementById("transpslider").style.backgroundColor = currentColor.HexString();
	document.getElementById('colorheadtext').innerHTML='\u00A0 Fill Colour';
	coltype='F';
	document.getElementById('colorbox').style.visibility='visible';
}

/**
 * funcion depreciada
 */
function linecolor()
{
	addTrackFunction("colorpicker", "linecolor");

	document.getElementById("redBox").value=SELECTEDSHAPE.strokeStyle[0];
	redBoxChanged();
	document.getElementById("greenBox").value=SELECTEDSHAPE.strokeStyle[1];
	greenBoxChanged();
	document.getElementById("blueBox").value=SELECTEDSHAPE.strokeStyle[2];
	blueBoxChanged();
	alphaperct = 100*(1-SELECTEDSHAPE.strokeStyle[3]);
	document.getElementById('varrows').style.left=(256*alphaperct/100-4)+"px";
	document.getElementById('transptext').innerHTML ='Transparency '+alphaperct+'%';
	if (EXCANVASUSED)
	{
		document.getElementById('transpslider').filters.alpha.opacity=100-alphaperct;
	}
	else
	{
		document.getElementById('transpslider').style.opacity=1-alphaperct/100;
	}
	document.getElementById("transpslider").style.backgroundColor = currentColor.HexString();
	document.getElementById('colorheadtext').innerHTML='\u00A0 Line Colour';
	coltype='L';
	document.getElementById('colorbox').style.visibility='visible';
	removeGradLine();
	removeRotate();
}

/**
 * funcion depreciada
 */
function moveCircleTo(cur)
{
	addTrackFunction("colorpicker", "moveCircleTo");

	// CAMBIOS PARA COLOREAR
	cur.x -= (parseInt(document.getElementById('colorbox').style.left) + 21 - PNLLEFT);
	cur.y -= (parseInt(document.getElementById('colorbox').style.top) + 46 - PNLTOP);
	document.getElementById('circle').style.left = cur.x+"px";
	document.getElementById('circle').style.top = cur.y+"px";
	if (parseInt(document.getElementById('circle').style.left)<-5) {document.getElementById('circle').style.left=-5+"px"}
	if (parseInt(document.getElementById('circle').style.left)>250) {document.getElementById('circle').style.left=250+"px"}
	if (parseInt(document.getElementById('circle').style.top)<-5) {document.getElementById('circle').style.top=-5+"px"}
	if (parseInt(document.getElementById('circle').style.top)>250) {document.getElementById('circle').style.top=251+"px"}
	currentColor.SetHSV(currentColor.Hue(), 1-(parseInt(document.getElementById('circle').style.top)+5)/255.0,(parseInt(document.getElementById('circle').style.left)+5)/255.0);
	colorChanged("circle");
}

/**
 * funcion depreciada
 */
function movetransarrwTo(cur)
{
	addTrackFunction("colorpicker", "movetransarrwTo");

	cur.x-=(parseInt(document.getElementById('colorbox').style.left)+21-PNLLEFT);
	document.getElementById('varrows').style.left=cur.x+"px";
	if (parseInt(document.getElementById('varrows').style.left)<-4) {document.getElementById('varrows').style.left=-4+"px"}
	if (parseInt(document.getElementById('varrows').style.left)>252) {document.getElementById('varrows').style.left=252+"px"}
	alphaperct=Math.ceil(100*(parseInt(document.getElementById('varrows').style.left)+4)/256);
	document.getElementById('transptext').innerHTML ='Transparency '+alphaperct+'%';
	if (EXCANVASUSED)
	{
		document.getElementById('transpslider').filters.alpha.opacity=100-alphaperct;
	}
	else
	{
		document.getElementById('transpslider').style.opacity=1-alphaperct/100;
	}
}

/**
 * funcion depreciada
 */
function HueBarmoveTo(cur)
{
	addTrackFunction("colorpicker", "HueBarmoveTo");

	cur.y-=(parseInt(document.getElementById('colorbox').style.top)+46-PNLTOP);
	document.getElementById('arrows').style.top=cur.y+"px"
	if (parseInt(document.getElementById('arrows').style.top)<-4) {document.getElementById('arrows').style.top=-4+"px"}
	if (parseInt(document.getElementById('arrows').style.top)>251) {document.getElementById('arrows').style.top=251+"px"}
	currentColor.SetHSV((256 - parseInt(document.getElementById('arrows').style.top)+4)*359.99/255,
		currentColor.Saturation(), currentColor.Value());
	colorChanged("arrows");;
}
