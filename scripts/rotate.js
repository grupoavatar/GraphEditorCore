/**
* @function removeRotate
* Oculta el elemento con id rotateMove
* @return		undefined						"No devuelve ningun valor"
*/
﻿function removeRotate()
{
	addTrackFunction("rotate","removeRotate");
	document.getElementById("rotateMove").style.visibility="hidden";
}

/**
* @function shaperotate
* Dispone el canvas para realizar la rotacion del Shape
* @return		undefined						"No devuelve ningun valor"
*/
function shaperotate()
{
	addTrackFunction("rotate","shaperotate");
	removeRotate();
	removeGradLine();
	clear(document.getElementById("markerdrop"));
	var shape=SELECTEDSHAPE;
	var group=shape.group;
	document.getElementById("rotateMove").style.left=(group.centreOfRotation.x+PNLLEFT+ROTATIONRADIUS*Math.cos(group.phi)-6)+"px";
	document.getElementById("rotateMove").style.top=(group.centreOfRotation.y+PNLTOP+ROTATIONRADIUS*Math.sin(group.phi)-6)+"px";
	document.getElementById("rotateMove").style.visibility="visible";
	document.getElementById("frontmarkerdrop").style.visibility="visible";
	document.getElementById("rotateMove").left=parseInt(document.getElementById("rotateMove").style.left);
	document.getElementById("rotateMove").top=parseInt(document.getElementById("rotateMove").style.top);
}

/**
* @function updateangle
* Actualiza el ángulo del shape
* @param		Integer			phi		"Valor del ángulo"
* @return		undefined					"No devuelve ningun valor"
*/
function updateangle(phi)
{
	addTrackFunction("rotate","updateangle");
	var shape=SELECTEDSHAPE;
	var mm=document.getElementById("rotateMove");
	if (arguments.length==0)
	{
		var dx=parseInt(mm.style.left)+6-(PNLLEFT+shape.group.centreOfRotation.x);
		var dy=parseInt(mm.style.top)+6-(PNLTOP+shape.group.centreOfRotation.y);
		phi = arctan(dy,dx);
	}
	else
	{
		phi*=Math.PI/180;
	}

	mm.left=PNLLEFT+shape.group.centreOfRotation.x+ROTATIONRADIUS*Math.cos(phi)-6;
	mm.top=PNLTOP+shape.group.centreOfRotation.y+ROTATIONRADIUS*Math.sin(phi)-6;
	mm.style.left=mm.left+"px";
	mm.style.top=mm.top+"px";

	for (var groupName in SELECTED)
	{
		var group=SELECTED[groupName];
		group.groupRotate(phi-group.phi);
		group.phi=phi;
	}
	if(TWEENEDIT)
	{
		CURRENTTWEEN.rotate.active=true;
		CURRENTTWEEN.setTweenTimeBox();
	}
}

/**
* @function getRotateCentrePosition
* Retorna las coordenadas del centro de rotación del Shape
* @return		Object					"Coordenadas del centro del Shape"
*/
function getRotateCentrePosition()
{
	var rotatePosition = {
		x: document.getElementById("rotateCentre").left,
		y: document.getElementById("rotateCentre").top
	}
	return rotatePosition;
}
