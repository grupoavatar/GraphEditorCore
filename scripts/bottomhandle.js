/**
* @class BottomH
*
* @param	Object		parentdiv		"Elemento para asociar BottomH"
* @return	this									"Retorna el objeto"
*/
﻿function BottomH(parentdiv, boundary)
{
	addTrackFunction("bottomhandle","BottomH");
	if(document.getElementById('bottom')) {document.getElementById('bottom').parentNode.removeChild(document.getElementById('bottom'))};
	this.elmRef = document.createElement('div');
	this.elmRef.id  = 'bottom'+boundary.id;
	this.elmRef.className ='resizeElement';
	this.elmRef.style.position='absolute';
	this.elmRef.style.left= (/*parseInt(boundary.style.left) +*/ parseInt(boundary.style.width)/2-MINSTEP)+"px";
	this.elmRef.style.top= (/*parseInt(boundary.style.top) +*/ parseInt(boundary.style.height)-MINSTEP)+"px";
	this.elmRef.style.fontSize=0;
	this.elmRef.style.width=10+"px";
	this.elmRef.style.height=10+"px";
	this.elmRef.style.zIndex=1000;
	this.elmRef.style.cursor='n-resize';
	this.elmRef.parentdiv=parentdiv;
	this.elmRef.style.backgroundColor="white";
	this.elmRef.style.border="solid black 1px";
	this.elmRef.parentdiv.appendChild(this.elmRef);
	return this.elmRef;
}
