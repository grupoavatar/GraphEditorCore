﻿/**
* @function CanvasToText
* Es la función que se utiliza para guardar el avance de la IMAGEN, y el cual se guarda en formato texto, que es propio del proyecto original
* @return		String		params		"Retorna una cadena de texto con el formato del proyecto para guardar la IMAGEN, y poder volver a editarla"
*/
function CanvasToText()
{
	addTrackFunction("totext","CanvasToText");
	var params='canvas@'+parseInt(SCRW)+'|'+parseInt(SCRH)+'^';
	var shape;
	var shapesText="";
	for (var name in SHAPES)
	{
		shape=SHAPES[name];
		shapesText+=shape.ShapeToText()+'*';
	}
	// Si no tiene SHAPES, envía null
	if (shapesText == "")
	{
		return null;
	}
	params+=shapesText;
	params=params.slice(0,-1);
	params+="¬";
	for (var name in GROUPS)
	{
		var group=GROUPS[name];
		for (var i = group.members.length-1; i >= 0 ; i--) {
			if (group.members[i] == undefined)
				group.members.splice(i, 1);
		};
		if (group.members.length > 0)
			params+=group.GroupToText()+'*';
	}
	params=params.slice(0,-1);
	return params;
}

/**
* @function ShapeToText
* Es una función propia de la clase "Shape" que se utiliza para guardar un SHAPE determinado en el formato texto, propio del proyecto
* @return		String		shapeAsText		"Retorna una cadena de texto con el formato propio del proyecto, del SHAPE"
*/
function ShapeToText()
{
	addTrackFunction("totext","ShapeToText");
	var shapeAsText='';

	shapeAsText+=this.name+'|';
	shapeAsText+=this.title+'|';
	if (this.open)
	{
		shapeAsText+=1+'|';
	}
	else
	{
		shapeAsText+=-1+'|';
	}
	if (this.editable)
	{
		shapeAsText+=1+'|';
	}
	else
	{
		shapeAsText+=-1+'|';
	}
	shapeAsText+=this.type+'|';
	shapeAsText+=this.tplftcrnr.x+'|';
	shapeAsText+=this.tplftcrnr.y+'|';
	shapeAsText+=this.btmrgtcrnr.x+'|';
	shapeAsText+=this.btmrgtcrnr.y+'|';
	shapeAsText+= this.strokeStyle.join()+'|';
	shapeAsText+= this.fillStyle.join()+'|';
	shapeAsText+=this.lineWidth+'|';
	shapeAsText+=this.lineCap+'|';
	shapeAsText+=this.lineJoin+'|';
	if (this.justfill)
	{
		shapeAsText+=1+'|';
	}
	else
	{
		shapeAsText+=-1+'|';
	}
	if (this.linearfill)
	{
		shapeAsText+=1+'|';
	}
	else
	{
		shapeAsText+=-1+'|';
	}
	shapeAsText+= this.lineGrad.join()+'|';
	shapeAsText+= this.radGrad.join()+'|';
	var carray=[]; //color array for color stops
	for (j=0;j<this.colorStops.length;j++)
	{
		carray[j]=[];
		for (k=0;k<5;k++)
		{
			carray[j][k]=this.colorStops[j][k];
		}
	}


	for (j=0;j<carray.length;j++)
	{
		carray[j]=carray[j].join();
	}
	carray=carray.join(":");

	shapeAsText+=carray+'|';

	shapeAsText+=this.stopn+'|';

	if (this.shadow)
	{
		shapeAsText+=1+'|';
	}
	else
	{
		shapeAsText+=-1+'|';
	}

	shapeAsText+=this.shadowOffsetX+'|';
	shapeAsText+=this.shadowOffsetY+'|';
	shapeAsText+=this.shadowBlur+'|';
	shapeAsText+= this.shadowColor.join()+'|';
	shapeAsText+=this.zIndex+'|';
	shapeAsText+=this.crnradius+'|';
	var node=this.path.next;
	while(node.point.x!="end")
	{
		shapeAsText+=node.vertex+':';
		shapeAsText+=node.corner+':';
		shapeAsText+=node.point.x+':';
		shapeAsText+=node.point.y+':';
		shapeAsText+=node.ctrl1.x+':';
		shapeAsText+=node.ctrl1.y+':';
		shapeAsText+=node.ctrl2.x+':';
		shapeAsText+=node.ctrl2.y+'!';
		node=node.next;
	}
	shapeAsText=shapeAsText.slice(0,-1);
	shapeAsText+='|';

	shapeAsText+= this.group.name;
	if (this.type=="text" || this.type=="text_bold" || this.type=="text_italic")
	{
		this.draw();
		shapeAsText+='|';
		shapeAsText+= (this.textValue);
	}

	return shapeAsText;
}

/**
* @function GroupToText
* Es la función que se utiliza para guardar un GRUPO de imagenes, y el cual se guarda en formato texto, que es propio del proyecto original
* @return		String		groupAsText		"Retorna una cadena de texto con el formato propio del proyecto del GRUPO, y poder volver a editarlo"
*/
function GroupToText()
{
	addTrackFunction("totext","GroupToText");
	var groupAsText="";
	groupAsText+=this.name+'|';
	groupAsText+=this.title+'|';
	groupAsText+=this.left+'|';
	groupAsText+=this.top+'|';
	groupAsText+=this.width+'|';
	groupAsText+=this.height+'|';
	groupAsText+=this.centreOfRotation.x+'|';
	groupAsText+=this.centreOfRotation.y+'|';
	groupAsText+=this.phi+'|';
	for (var i=0; i<this.members.length; i++)
	{
		if (this.members[i].elType=="_SHAPE")
		{
			groupAsText+="s!";
		}
		else
		{
			groupAsText+="g!";
		}
		groupAsText+=this.members[i].name+":";
	}
	return groupAsText.slice(0,-1);
}
