/**
 * @function removeGradLine
 * Funcion para remover los elementos relacionados a la linea gradiente de colores.
 * @return		NULL 		"No devuelve ningun valor"
 */
﻿function removeGradLine()
{
	addTrackFunction("gradientmarker","removeGradLine");

	BACKDROP.Canvas.ctx.clearRect(0, 0, SCRW, SCRH);
	while (gdmrks > 0)
	{
		if(document.getElementById('gdm'+(--gdmrks))) {document.getElementById('gdm'+gdmrks).parentNode.removeChild(document.getElementById('gdm'+gdmrks))};
	}
	while (rdmrks > 0)
	{
		if(document.getElementById('rdm'+(--rdmrks))) {document.getElementById('rdm'+rdmrks).parentNode.removeChild(document.getElementById('rdm'+rdmrks))};
	}
}
