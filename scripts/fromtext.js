﻿/**
* @function handleDragOver
* Manejador de eventos al pasar sobre el cuadro de dialogo
* @return		NULL		"No devuelve nada"
*/
function handleDragOver(evt)
{
	addTrackFunction("fromtext","handleDragOver");
	evt.stopPropagation();
	evt.preventDefault();
}

/**
* @function handleBodyDrop
* Manejador de eventos al soltar el mouse sobre el cuadro de dialogo
* @return		NULL		"No devuelve nada"
*/
function handleBodyDrop(evt)
{
	addTrackFunction("fromtext","handleBodyDrop");
	evt.stopPropagation();
	evt.preventDefault();
}

/**
* @function fromText
* Una funcion de un cuadro de dialogo en desuso para leer un archivo( en formato del propio proyecto) y luego dibujarlo en el lienzo
* @return		NULL		"No devuelve nada, pero lanza eventos en caso un archivo se desee leer"
*/
function fromText()
{
	addTrackFunction("fromtext","fromText");

	if (window.File && window.FileReader && window.FileList && window.Blob && !iop)
	{
		document.getElementById("filetextbox").style.visibility="visible";
		var dropZone = document.getElementById('drop_zone');
		dropZone.innerHTML="";
		dropZone.addEventListener('dragover', handleDragOver, false);
		dropZone.addEventListener('drop', handleFileSelect, false);
	}
	else
	{
		document.getElementById("fromtextbox").style.visibility="visible";
	}
}

/**
* @function handleFileSelect
* Una funcion de un cuadro de dialogo en desuso para leer un archivo( en formato del propio proyecto) y lanza un eveno para leer el archivo mediante "extract"
* @param		Object	evt			"Objeto con datos de un archivo, propio del formato del propio proyecto para dibujar en el lienzo"
* @return						NULL		"No devuelve nada, pero lanza eventos para leer un archivo y dibujar"
*/
function handleFileSelect(evt)
{
	addTrackFunction("fromtext","handleFileSelect");
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files; // FileList object.

	var f = files[0];
	if (f)
	{
		var r = new FileReader();
		r.onloadend = function(e) {extract(e.target.result) }
		r.readAsText(f);
		document.getElementById('drop_zone').innerHTML +="&nbsp;&nbsp;"+f.name+'<br>';
	}
	else
	{
		alert("Failed to load file");
	}
}

/**
* @function extract
* Funcion importante, toma una cadena de texto en el formato del proyecto y dibuja en el lienzo
* @param		String	a				"Cadena de texto, con un formato propio del proyecto para guardar las imágenes"
* @return						NULL		"No devuelve una cadena de texto, pero en caso de error sale una alerta"
*/
function extract(a)
{
	addTrackFunction("fromtext","extract");
	var t=a.split('@');
	switch (t[0])
	{
		case 'canvas':
		try
		{
			resetcanv(t[1]);
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		case 'scene':
		try
		{
			resetscene(t[1],"basescene");
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		case 'sprite':
		try
		{
			resetsprite(t[1],"baseSprite");
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		case 'tween':
		try
		{
			resettween(t[1],"basetween");
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		case 'track':
		try
		{
			resettrack(t[1],"basetrack");
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		case 'film':
		try
		{
			resetfilm(t[1]);
		}
		catch(e)
		{
			alert(e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
			alert('File data not recognised');
			return;
		}
		break

		default:
		alert('here '+e.name + ": " + e.message + ": " + e.fileName + ": " + e.lineNumber);
		alert('File data not recognised');
		return;
	}
	document.getElementById('bodydiv').onclick=function(e){checkBoundary(shiftdown(e),getPosition(e))};
	document.getElementById('bodydiv').onmousedown=function(e){checkSelector(shiftdown(e),getPosition(e))};
}

/**
* @function resetcanv
* Funcion importante, toma una cadena de texto en el formato del proyecto y dibuja en el lienzo
* @param		Object	canvasfiletxt			"Cadena de texto, con un formato propio del proyecto para guardar las imágenes"
* @return						NULL							"De acuerdo a la cadena dibuja cada figura en el lienzo"
*/
function resetcanv(canvasfiletxt)
{
	addTrackFunction("fromtext","resetcanv");
	var canvasfile=canvasfiletxt.split("^");
	var sizetxt=canvasfile[0];
	var sgtxt=canvasfile[1];
	var zmax=0;
	var zmin=10000000;
	var shape;
	resetshapestage(sizetxt);
	resetshapes(sgtxt,SHAPES,GROUPS,"canvas");

	for (var name in SHAPES)
	{
		var num = parseInt(name.replace("Shape",""));
		SCOUNT = (num>SCOUNT)?num:SCOUNT;
		shape=SHAPES[name];
		SCOUNT++;
		if (shape.zIndex>zmax) {zmax=shape.zIndex};
		if (shape.zIndex<zmin) {zmin=shape.zIndex};
		shape.addTo(document.getElementById("shapestage"));
		if ( shape.type == "text" || shape.type == "text_bold"
			|| shape.type == "text_italic")
		{
			shape.setPath(shape.path.next.point);
		} else
		{
			shape.draw();
		}
	}

	for (var name in GROUPS)
	{
		var num = parseInt(name.replace("Group",""));
		GCOUNT = (num>GCOUNT)?num:GCOUNT;
		GCOUNT++;
	}
	ZPOS=zmax+1;
	ZNEG=zmin-1;
}

/**
* @function resetBoundary
* Limpia el elemento donde se dibujan los boundaries o limites
* @return		NULL		"No devuelve ningun valor"
*/
function resetBoundary()
{
	clear(document.getElementById("boundarydrop"));
	// var boundaryResize = document.getElementsByClassName("resizeElement");
	// var numBoundaryResize = boundaryResize.length;
	// // missing
	// for (var i = 0; i < numBoundaryResize; i++) {
	// 	var elem = boundaryResize[i];
	// 	elem.parentNode.removeChild(elem);
	// };
}

/**
* @function resetBackstage
* Limpia el elemento backstage, que estpa relacionado al z-index de las figuras
* @return		NULL		"No devuelve ningun valor"
*/
function resetBackstage()
{
	clear(document.getElementById("backstage"));
	BACKDROP={};
	BACKDROP.zIndex=0;
	BACKDROP.addTo=addTo;
	BACKDROP.addTo(document.getElementById("backstage"));
}

/**
* @function visibleBackstage
* Muestra el elemento backstage, que estpa relacionado al z-index de las figuras
* @return		NULL		"No devuelve ningun valor"
*/
function visibleBackstage()
{
	document.getElementById("backstage").style.visibility="visible";
}

/**
* @function resetshapestage
* Limpia todo el Panel central, lo deja completamente vacio. Elimina todas las figuras, boundaries, canvas, corners, etc..
* @return		NULL		"No devuelve ningun valor"
*/
function resetshapestage(txt)
{
	addTrackFunction("fromtext","resetshapestage");
	stagewidth = PNLWIDTH;
	stageheight = PNLHEIGHT;
	c = 0;
	clear(document.getElementById("shapestage"));
	resetBoundary();
	clear(document.getElementById("markerdrop"));

	resetBackstage();
	document.getElementById("boundarydrop").style.visibility="visible";
	SHAPES={};
	SELECTED={};
	SELECTEDSHAPE={};
	DELETED={};
	DELETES=[];
	GROUPS={};
	MCOUNT=0;
	BCOUNT=0;
	SCOUNT=0;
	GCOUNT=0;
	NCOUNT=0;
	ZPOS=1;
	ZNEG=-1;
	CURRENT=SHAPES;
	return c;
}

/**
* @function canvasClear
* Limpia solo los canvas de las figuras
* @return		NULL		"No devuelve ningun valor"
*/
function canvasClear()
{
	addTrackFunction("fromtext","canvasClear");
	ungroup();
	var i=0;
	for (var name in CURRENT)
	{
		i++;
	}
	if (i==1 && !(CURRENT===SHAPES))
	{
		return;
	}

	removeRotate();

	var actions = [];
	for (var nameShape in CURRENT)
	{
		var shape=CURRENT[nameShape];
		actions.push(new DeleteShapesCommand(shape));
	}

	if (actions.length > 0)
	{
		window.UndoRedo.executeCommand(actions);
	}
	clear(document.getElementById("markerdrop"));
	resetBoundary();
	resetBackstage()

}

/**
* @function resetShapes
* Dibuja las figuras de acuerdo a la variable "sgtxt"
* @param		String	sgtxt					"Cadena de texto, que contiene elf omrato para dibujar una imagen o un grupo de imagenes relacionados"
* @param		Object	Shape_Store		"Objeto que guarda todos las figuras, globalmente es definido como SHAPES"
* @param		Object	Group_Store		"Objeto que guarda todos los grupos de figuras, globalmente es definido como GROUPS"
* @param		String	type					"..."
* @return	 					NULL					"No devuelve ningun valor"
*/
function resetshapes(sgtxt,Shape_Store,Group_Store,type)
{
	addTrackFunction("fromtext","resetshapes");
	var shape,group;
	var sgp=sgtxt.split("¬")
	var shapetxt=sgp[0];
	var grouptxt=sgp[1];
	var shapeparams=shapetxt.split('*');

	while (shapeparams.length>0)
	{
		shape=paramstoshape(shapeparams.shift(),Shape_Store,type);
	}
	var groupparams=grouptxt.split('*');

	while(groupparams.length>0)
	{
		paramstogroup(groupparams.shift(),Group_Store,type);
	}

	for (var name in Group_Store)
	{
		group=Group_Store[name];
		for (var i=0; i<group.members.length; i++)
		{
			if (group.members[i][0]=="s")
			{
				group.members[i]=Shape_Store[group.members[i][1].trim()];
			}
			else
			{
				group.members[i]=Group_Store[group.members[i][1].trim()];
			}
		}
	}

	for (var name in Shape_Store)
	{
		shape=Shape_Store[name];
		shape.group=Group_Store[shape.group]; //change shape.group from group.name to actual group
	}
}

/**
* @function paramstoshape
* de acuerdo a la variable p, dibuja la(s) figura(s), en este proeso se encuentra los algoritmos donde hace el dibujo exacto de acuerdo a nodos de cada figura
* @param		String	p							"Cadena de texto, que contiene elf omrato para dibujar una imagen o un grupo de imagenes relacionados"
* @param		Object	Shape_Store		"Objeto que guarda todos las figuras, globalmente es definido como SHAPES"
* @param		String	type					"..."
* @return	 					NULL					"No devuelve ningun valor"
*/
function paramstoshape(p,Shape_Store,type)
{
	addTrackFunction("fromtext","paramstoshape");
	var nodedata,point,ctrl1,ctrl2;
	var node;
	p=p.split('|');

	if (type=="canvas")
	{
		var oldname=p[0];
		var newname=oldname;
	}
	else
	{
		var oldname=p[0];
		var newname="SUBSH"+(NCOUNT++);
	}

	var shape=new Shape(p[0],p[1],p[2]=="1",p[3]=="1",p[4],Shape_Store);
	shape.tplftcrnr.x = parseInt(p[5]);
	shape.tplftcrnr.y = parseInt(p[6]);
	shape.btmrgtcrnr.x = parseInt(p[7]);
	shape.btmrgtcrnr.y = parseInt(p[8]);
	shape.strokeStyle = p[9].split(',');

	for (var i=0;i<shape.strokeStyle.length;i++)
	{
		shape.strokeStyle[i]=parseInt(shape.strokeStyle[i]);
	}
	shape.fillStyle = p[10].split(',');
	for (var i=0;i<shape.fillStyle.length;i++)
	{
		if (i==3){
			shape.fillStyle[i]=parseFloat(shape.fillStyle[i])
		}else{
			shape.fillStyle[i]=parseInt(shape.fillStyle[i])
		}
	}

	shape.lineWidth = parseInt(p[11]);
	shape.lineCap = p[12];
	shape.lineJoin = p[13];
	shape.justfill = p[14]=='1';
	shape.linearfill = p[15]=='1';
	shape.lineGrad = p[16].split(',');

	for (var i=0;i<shape.lineGrad.length;i++)
	{
		shape.lineGrad[i]=parseInt(shape.lineGrad[i])
	}
	shape.radGrad = p[17].split(',');
	for (var i=0;i<shape.radGrad.length;i++)
	{
		shape.radGrad[i]=parseInt(shape.radGrad[i])
	}
	var carray=p[18].split(':');
	shape.colorStops = [];
	for (var i=0; i<carray.length; i++)
	{
		shape.colorStops[i]=carray[i].split(',');
		for (var j=0;j<shape.colorStops[i].length;j++)
		{
			shape.colorStops[i][j]=parseFloat(shape.colorStops[i][j])
		}
	}

	shape.stopn = parseInt(p[19]);
	shape.shadow = p[20]=='1';
	shape.shadowOffsetX = parseInt(p[21]);
	shape.shadowOffsetY = parseInt(p[22]);
	shape.shadowBlur = parseFloat(p[23]);
	shape.shadowColor = p[24].split(',');
	for (var i=0;i<shape.shadowColor.length;i++)
	{
		shape.shadowColor[i]=parseInt(shape.shadowColor[i])
	}
	shape.zIndex = parseFloat(p[25]);
	shape.crnrradius = parseInt(p[26]);
	shape.group = p[28]; //name of group
	var path=p[27].split("!");

	while (path.length>0)
	{
		nodedata=path.shift().split(":");
		point=new Point(parseFloat(nodedata[2]),parseFloat(nodedata[3]));
		node=new Node(point);
		node.vertex=nodedata[0];
		node.corner=nodedata[1];
		node.ctrl1.x=nodedata[4];
		if (node.ctrl1.x !="non")
		{
			node.ctrl1.x=parseFloat(node.ctrl1.x)
		}
		node.ctrl1.y=nodedata[5];
		if (node.ctrl1.y !="non")
		{
			node.ctrl1.y=parseFloat(node.ctrl1.y)
		}
		node.ctrl2.x=nodedata[6];
		if (node.ctrl2.x !="non")
		{
			node.ctrl2.x=parseFloat(node.ctrl2.x)
		}
		node.ctrl2.y=nodedata[7];
		if (node.ctrl2.y !="non")
		{
			node.ctrl2.y=parseFloat(node.ctrl2.y)
		}
		shape.addNode(node);
	}
	if ( shape.type == "text" || shape.type=="text_bold" || shape.type=="text_italic" )
	{
		shape.textValue = p["29"];
	}
	return shape;
}

/**
* @function paramstogroup
* de acuerdo a la variable p, setea las variables de los grupos
* @param		String	p							"Cadena de texto, que contiene elf omrato para modificar las variables del grupo"
* @param		Object	Group_Store		"Objeto que guarda todos los Grupos, globalmente es definido como GROUPS"
* @param		String	type					"..."
* @return	 					NULL					"No devuelve ningun valor"
*/
function paramstogroup(p,Group_Store,type)
{
	addTrackFunction("fromtext","paramstogroup");
	var members,member;
	p=p.split('|');
	if (type=="canvas")
	{
		var name=p[0];
	}
	else
	{
		var name="SUBGP"+(NCOUNT++);
	}
	var group=new Group(Group_Store,p[0],p[1]);
	group.left=parseInt(p[2]);
	group.top=parseInt(p[3]);
	group.width=parseInt(p[4]);
	group.height=parseInt(p[5]);
	group.centreOfRotation.x=parseInt(p[6]);
	group.centreOfRotation.y=parseInt(p[7]);
	group.phi=parseFloat(p[8]);
	members=p[9].split(":");
	while(members.length>0)
	{
		member=members.shift();
		member=member.split("!");
		group.members.push(member);
	}
	return group;
}

/**
* @function resetImageCanvas
* Limpia el elemento oculto donde se guarda de manera local el último canvas
* @param		String	txt		"..."
* @return						NULL	"No devuelve ningun valor"
*/
function resetImageCanvas(txt)
{
	addTrackFunction("fromtext","resetImageCanvas");
	clear(document.getElementById("imageCanvasSavedLocal"));
}
