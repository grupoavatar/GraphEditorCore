/**
* @function keyDown
* Asigna funciones especificas para cada evento key/mouse
* @return		undefined		"No devuelve ningun valor"
*/
function keyDown()
{
	// remove drawEnd from right clic
	// document.onmousedown=function (e) {
	// 	if (e.button == 2)
	// 	{
	// 		// drawEndOut();
	// 	}
	// }

	document.onkeydown = function(e) {
		var eventObj = window.event? event : e;

		if (eventObj.keyCode == 90 && eventObj.ctrlKey)//ctrl + z
		{
			eventObj.preventDefault();
			window.UndoRedo.undoCommand();
			return false;
		}

		if (eventObj.keyCode == 89 && eventObj.ctrlKey)//ctrl + y
		{
			eventObj.preventDefault();
			window.UndoRedo.redoCommand();
			return false;
		}

		if (eventObj.keyCode == 86 && eventObj.ctrlKey)//ctrl + v
		{
			noBubble(eventObj);
			cancelEvent(eventObj);
			shapecopy(10);
		}

		if (eventObj.keyCode == 27 || eventObj.keyCode == 13)//Esc o Enter
		{
			DRAWING_SHAPE = -1;
			drawEndOut();
			selector();
		}

		if(eventObj.keyCode == 46)
		{
			if (Object.keys(SELECTED).length == 1) {
				if ( isInputText() )
				{
					return;
				}
			}
			shapedelete();
		}

		// removing event scroll with arrows when is selected by preventDefault
		if (eventObj.keyCode == '38') {
			// up arrow
			if ( (Object.keys(SELECTED).length > 1) || ( (Object.keys(SELECTED).length == 1) && (!isInputText()) ) ) {
				e.preventDefault();
				var dx=0,dy=-5;
				moveShapes(dx,dy);
			}
		}
		else if (eventObj.keyCode == '40') {
			// down arrow
			if ( (Object.keys(SELECTED).length > 1) || ( (Object.keys(SELECTED).length == 1) && (!isInputText()) ) ) {
				e.preventDefault();
				var dx=0,dy=5;
				moveShapes(dx,dy);
			}
		}
		else if (eventObj.keyCode == '37') {
			// left arrow
			if ( (Object.keys(SELECTED).length > 1) || ( (Object.keys(SELECTED).length == 1) && (!isInputText()) ) ) {
				e.preventDefault();
				var dx=-5,dy=0;
				moveShapes(dx,dy);
			}
		}
		else if (eventObj.keyCode == '39') {
			// right arrow
			if ( (Object.keys(SELECTED).length > 1) || ( (Object.keys(SELECTED).length == 1) && (!isInputText()) ) ) {
				e.preventDefault();
				var dx=5,dy=0;
				moveShapes(dx,dy);
			}
		}
	}
}

/**
* @function enableContextMenu
* Habilita el menú del contexto
* @return		undefined		"No devuelve ningun valor"
*/
function enableContextMenu()
{
	document.oncontextmenu = function ()
	{
		return true;
	}
}

/**
* @function disableContextMenu
* Deshabilita el menú del contexto
* @return		undefined		"No devuelve ningun valor"
*/
function disableContextMenu()
{
	document.oncontextmenu = function ()
	{
		return false;
	}
}
