/**
* @function addTrackFunction
* función para conocer el tracking de las funcionalidades macros
* @param  String		a		"1ra cadena a mostrar"
* @param  String		b		"2da cadena a mostrar"
* @return	undefined			"No devuelve ningún valor"
*/
function addTrackFunction (a,b){
	// console.log(a,b);
	return;
}

var GLOBAL1 = {style:{}};

/**
* @class ChangeColorCommand
*
* @param  Object		obj					"Objeto de referencia"
* @param  Object		shapeOld		"Objeto con atributos con los valores anteriores"
* @param  Object		shapeNew		"Objeto con atributos con los nuevos valores"
* @return	this									"Retorna el objeto"
*/
ChangeColorCommand = function(obj, shapeOld, shapeNew) {
	this.obj = obj;
	this.obj_old = shapeOld;
	this.obj_new = shapeNew;
}

ChangeColorCommand.prototype.execute = function() {
	this.obj.justfill = this.obj_new.justfill;
	this.obj.fillStyle[0]=this.obj_new.fillStyle[0];
	this.obj.fillStyle[1]=this.obj_new.fillStyle[1];
	this.obj.fillStyle[2]=this.obj_new.fillStyle[2];
	this.obj.fillStyle[3]=this.obj_new.fillStyle[3];
	this.obj.draw();
}

ChangeColorCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

ChangeColorCommand.prototype.undo = function() {
	this.obj.justfill = this.obj_old.justfill;
	this.obj.fillStyle[0]=this.obj_old.fillStyle[0];
	this.obj.fillStyle[1]=this.obj_old.fillStyle[1];
	this.obj.fillStyle[2]=this.obj_old.fillStyle[2];
	this.obj.fillStyle[3]=this.obj_old.fillStyle[3];
	this.obj.draw();
	undoRedoActionGlobal();
}

/**
* @class CreateShapeCommand
*
* @param  Object	obj		"Objeto de referencia"
* @return	this					"Retorna el objeto"
*/
CreateShapeCommand = function(obj) {
	this.obj = obj;
}

CreateShapeCommand.prototype.execute = function() {
	this.obj.Canvas.style.visibility="visible";
	SHAPES[this.obj.name]=this.obj;
}

CreateShapeCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

CreateShapeCommand.prototype.undo = function() {
	this.obj.Canvas.style.visibility="hidden";
	delete CURRENT[this.obj.name];
	resetBoundary();
	clear(document.getElementById("markerdrop"));
	resetBackstage();
	undoRedoActionGlobal();
}

/**
* @class DeleteShapesCommand
*
* @param 	Object		obj					"Objeto de referencia"
* @return	this									"Retorna el objeto"
*/
DeleteShapesCommand = function(obj) {
	this.obj = obj;
}

DeleteShapesCommand.prototype.execute = function() {
	this.obj.Canvas.style.visibility="hidden";
	delete CURRENT[this.obj.name];
	resetBoundary();
	undoRedoActionGlobal();
}

DeleteShapesCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

DeleteShapesCommand.prototype.undo = function() {
	this.obj.Canvas.style.visibility="visible";
	SHAPES[this.obj.name]=this.obj;
	undoRedoActionGlobal();
}

/**
* @class MoveShapesCommand
*
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		dx					"Valor equivalente al dezplazamiento horizontal"
* @param	Integer		dy					"Valor equivalente al dezplazamiento vertical"
* @param	Integer		l						"Nuevo valor para posición horizontal"
* @param	Integer		t						"Nuevo valor para posición vertical"
* @param	Integer		scalew			"Valor escalar para alinear con la gria"
* @param	Integer		scaleh			"Valor escalar para alinear con la gria"
* @param	Integer		quantity		"Cantidad de Shapes seleccionados"
* @return	this									"Retorna el objeto"
*/
MoveShapesCommand = function(shapeNames, group, dx, dy, l, t, scalew, scaleh, quantity) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.dx = dx;
	this.dy = dy;
	this.l = l;
	this.t = t;
	this.scalew = scalew;
	this.scaleh = scaleh;
	this.shapesQuantity = quantity;
}

MoveShapesCommand.prototype.execute = function() {
	var type = '';
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		type = shape.type;
		node=shape.path.next;
		while(node.point.x!="end")
		{
			node.point.x+=this.dx;
			node.point.y+=this.dy;
			if(node.ctrl1.x!="non") //changes when straight line set on curve
			{
				node.ctrl1.x+=this.dx;
				node.ctrl1.y+=this.dy;
				node.ctrl2.x+=this.dx;
				node.ctrl2.y+=this.dy;
			}
			node=node.next;
		}
		shape.tplftcrnr.x +=this.dx;
		shape.btmrgtcrnr.x+=this.dx;
		shape.tplftcrnr.y +=this.dy;
		shape.btmrgtcrnr.y+=this.dy;
		shape.lineGrad[0] +=this.dx;
		shape.lineGrad[1] +=this.dy;
		shape.lineGrad[2] +=this.dx;
		shape.lineGrad[3] +=this.dy;
		shape.radGrad[0] +=this.dx;
		shape.radGrad[1] +=this.dy;
		shape.radGrad[3] +=this.dx;
		shape.radGrad[4] +=this.dy;
		shape.arccentre.x+=this.dx;
		shape.arccentre.y+=this.dy;
		shape.draw();
	}
	this.group.update(this.l,this.t,this.dx,this.dy,this.scalew,this.scaleh);

	this.group.drawBoundary(type, this.shapesQuantity);
}

MoveShapesCommand.prototype.redo = function() {
	this.execute();
	resetBoundary();
	undoRedoActionGlobal();
}

MoveShapesCommand.prototype.undo = function() {
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		node=shape.path.next;
		while(node.point.x!="end")
		{
			node.point.x-=this.dx;
			node.point.y-=this.dy;
			if(node.ctrl1.x!="non") //changes when straight line set on curve
			{
				node.ctrl1.x-=this.dx;
				node.ctrl1.y-=this.dy;
				node.ctrl2.x-=this.dx;
				node.ctrl2.y-=this.dy;
			}
			node=node.next;
		}
		shape.tplftcrnr.x -=this.dx;
		shape.btmrgtcrnr.x-=this.dx;
		shape.tplftcrnr.y -=this.dy;
		shape.btmrgtcrnr.y-=this.dy;
		shape.lineGrad[0] -=this.dx;
		shape.lineGrad[1] -=this.dy;
		shape.lineGrad[2] -=this.dx;
		shape.lineGrad[3] -=this.dy;
		shape.radGrad[0] -=this.dx;
		shape.radGrad[1] -=this.dy;
		shape.radGrad[3] -=this.dx;
		shape.radGrad[4] -=this.dy;
		shape.arccentre.x-=this.dx;
		shape.arccentre.y-=this.dy;

		shape.draw();
	}

	this.group.disupdate(this.l,this.t,this.dx,this.dy,this.scalew,this.scaleh);
	clear(document.getElementById("markerdrop"));
	resetBackstage();
	undoRedoActionGlobal();
}

/**
* @class ccShapesCommand
* Clase que representa la esquina que permite redimensionar
* Redimensiona los nodos, pero tiene un coportamiento distinto para las formas de tipo flecha y cota
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		l						"Nuevo valor para posición horizontal"
* @param	Integer		t						"Nuevo valor para posición vertical"
* @param	Integer		scale				"Valor escalar para alinear con la gria"
* @return	this									"Retorna el objeto"
*/
ccShapesCommand = function(shapeNames, group, l, t, scale) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.l = l;
	this.t = t;
	this.scale = scale;
}

ccShapesCommand.prototype.execute = function() {
	// to verify just One updatedis executed and no more than it
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		shape.tplftcrnr.x=this.group.left+(shape.tplftcrnr.x-this.group.left)*this.scale;
		shape.tplftcrnr.y=this.group.top+(shape.tplftcrnr.y-this.group.top)*this.scale;

		shape.btmrgtcrnr.x=this.group.left+(shape.btmrgtcrnr.x-this.group.left)*this.scale;
		shape.btmrgtcrnr.y=this.group.top+(shape.btmrgtcrnr.y-this.group.top)*this.scale;
		shape.arcwidth*=this.scale;//Math.abs(shape.btmrgtcrnr.x-shape.tplftcrnr.x);
		shape.archeight*=this.scale;//Math.abs(shape.btmrgtcrnr.y-shape.tplftcrnr.y);
		shape.arccentre.x=this.group.left+(shape.arccentre.x-this.group.left)*this.scale;
		shape.arccentre.y=this.group.top+(shape.arccentre.y-this.group.top)*this.scale;
		shape.radius*=this.scale;
		if (shape.type=='rounded_rectangle')
		{			
			shape.setRndRect();
		}
		else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "xy", null, justOneShapeSelected);
		}
		else
		{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.x=this.group.left+(node.point.x-this.group.left)*this.scale;
				node.point.y=this.group.top+(node.point.y-this.group.top)*this.scale;
				if(node.ctrl1.x!="non")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.x=this.group.left+(node.ctrl1.x-this.group.left)*this.scale;
					node.ctrl1.y=this.group.top+(node.ctrl1.y-this.group.top)*this.scale;
					node.ctrl2.x=this.group.left+(node.ctrl2.x-this.group.left)*this.scale;
					node.ctrl2.y=this.group.top+(node.ctrl2.y-this.group.top)*this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.update(this.l-PNLLEFT,this.t-PNLTOP,0,0,this.scale,this.scale)
		}
		shape.draw();
	}
}

ccShapesCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

ccShapesCommand.prototype.undo = function() {
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		/*scalegroup*/
		shape.tplftcrnr.x=this.group.left+(shape.tplftcrnr.x-this.group.left)/this.scale;
		shape.tplftcrnr.y=this.group.top+(shape.tplftcrnr.y-this.group.top)/this.scale;
		/*scalegroup*/
		shape.btmrgtcrnr.x=this.group.left+(shape.btmrgtcrnr.x-this.group.left)/this.scale;
		shape.btmrgtcrnr.y=this.group.top+(shape.btmrgtcrnr.y-this.group.top)/this.scale;
		shape.arcwidth/=this.scale;//Math.abs(shape.btmrgtcrnr.x-shape.tplftcrnr.x);
		shape.archeight/=this.scale;//Math.abs(shape.btmrgtcrnr.y-shape.tplftcrnr.y);
		shape.arccentre.x=this.group.left+(shape.arccentre.x-this.group.left)/this.scale;
		shape.arccentre.y=this.group.top+(shape.arccentre.y-this.group.top)/this.scale;
		shape.radius/=this.scale;
		if (shape.type=='rounded_rectangle')
		{
			shape.setRndRect()
		}
		else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "xy", "reduce", justOneShapeSelected);
		}
		else
		{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.x=this.group.left+(node.point.x-this.group.left)/this.scale;
				node.point.y=this.group.top+(node.point.y-this.group.top)/this.scale;
				if(node.ctrl1.x!="non")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.x=this.group.left+(node.ctrl1.x-this.group.left)/this.scale;
					node.ctrl1.y=this.group.top+(node.ctrl1.y-this.group.top)/this.scale;
					node.ctrl2.x=this.group.left+(node.ctrl2.x-this.group.left)/this.scale;
					node.ctrl2.y=this.group.top+(node.ctrl2.y-this.group.top)/this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.disupdate(this.l-PNLLEFT,this.t-PNLTOP,0,0,this.scale,this.scale);
		}
		shape.draw();
	}
	undoRedoActionGlobal();
}

/**
* @class rhShapesCommand
* Clase que representa el borde derecho que permite redimensionar
* Redimensiona los nodos, pero tiene un coportamiento distinto para las formas de tipo flecha y cota
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		l						"Nuevo valor para posición horizontal"
* @param	Integer		t						"Nuevo valor para posición vertical"
* @param	Integer		scale				"Valor escalar para alinear con la gria"
* @return	this									"Retorna el objeto"
*/
rhShapesCommand = function(shapeNames, group, l, t, scale) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.l = l;
	this.t = t;
	this.scale = scale;
}

rhShapesCommand.prototype.execute = function() {
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		/*scalegroup*/
		shape.tplftcrnr.x=this.group.left+(shape.tplftcrnr.x-this.group.left)*this.scale;
		/*scalegroup*/
		shape.btmrgtcrnr.x=this.group.left+(shape.btmrgtcrnr.x-this.group.left)*this.scale;
		shape.arcwidth*=this.scale;
		shape.radius*=this.scale;
		shape.arccentre.x=this.group.left+(shape.arccentre.x-this.group.left)*this.scale;
		if (shape.type=='rounded_rectangle')
		{
			shape.setRndRect()
		}
		else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "x", null,justOneShapeSelected);
		}
		else
		{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.x=this.group.left+(node.point.x-this.group.left)*this.scale;
				if(node.ctrl1.x!="non")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.x=this.group.left+(node.ctrl1.x-this.group.left)*this.scale;
					node.ctrl2.x=this.group.left+(node.ctrl2.x-this.group.left)*this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.update(this.l-PNLLEFT,this.t-PNLTOP,0,0,this.scale,1);
		}
		shape.draw();
	}
}

rhShapesCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

rhShapesCommand.prototype.undo = function() {
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		/*scalegroup*/
		shape.tplftcrnr.x=this.group.left+(shape.tplftcrnr.x-this.group.left)/this.scale;
		/*scalegroup*/
		shape.btmrgtcrnr.x=this.group.left+(shape.btmrgtcrnr.x-this.group.left)/this.scale;
		shape.arcwidth/=this.scale;
		shape.radius/=this.scale;
		shape.arccentre.x=this.group.left+(shape.arccentre.x-this.group.left)/this.scale;
		if (shape.type=='rounded_rectangle')
		{
			shape.setRndRect();
		}else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "x", "reduce", justOneShapeSelected);
		}
		else
		{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.x=this.group.left+(node.point.x-this.group.left)/this.scale;
				if(node.ctrl1.x!="non")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.x=this.group.left+(node.ctrl1.x-this.group.left)/this.scale;
					node.ctrl2.x=this.group.left+(node.ctrl2.x-this.group.left)/this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.disupdate(this.l-PNLLEFT,this.t-PNLTOP,0,0,this.scale,1);
		}
		shape.draw();
	}
	undoRedoActionGlobal();
}

/**
* @class bhShapesCommand
* Clase que representa el borde inferior que permite redimensionar
* Redimensiona los nodos, pero tiene un coportamiento distinto para las formas de tipo flecha y cota
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		l						"Nuevo valor para posición horizontal"
* @param	Integer		t						"Nuevo valor para posición vertical"
* @param	Integer		scale				"Valor escalar para alinear con la gria"
* @return	this									"Retorna el objeto"
*/
bhShapesCommand = function(shapeNames, group, l, t, scale) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.l = l;
	this.t = t;
	this.scale = scale;
}

bhShapesCommand.prototype.execute = function() {
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		/*scalegroup*/
		shape.tplftcrnr.y=this.group.top+(shape.tplftcrnr.y-this.group.top)*this.scale;
		/*scalegroup*/
		shape.btmrgtcrnr.y=this.group.top+(shape.btmrgtcrnr.y-this.group.top)*this.scale;
		shape.archeight*=this.scale;//Math.abs(shape.btmrgtcrnr.y-shape.tplftcrnr.y);
		shape.arccentre.y=this.group.top+(shape.arccentre.y-this.group.top)*this.scale;
		if (shape.type=='rounded_rectangle')
		{
			shape.setRndRect();
		}
		else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "y", null, justOneShapeSelected);
		}
		else{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.y=this.group.top+(node.point.y-this.group.top)*this.scale;
				if(node.ctrl1.x!="end")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.y=this.group.top+(node.ctrl1.y-this.group.top)*this.scale;
					node.ctrl2.y=this.group.top+(node.ctrl2.y-this.group.top)*this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.update(this.l-PNLLEFT,this.t-PNLTOP,0,0,1,this.scale);
		}
		shape.draw();
	}
}

bhShapesCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

bhShapesCommand.prototype.undo = function() {
	var justOneTime = true;
	var countShapes = 0;
	for(var name in this.shapeNames){
		countShapes++;
	}
	var justOneShapeSelected = ( countShapes == 1 );
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		justOneShapeSelected = ( justOneShapeSelected && (shape.type=='cota' || shape.type=='flecha') );// IT HAS TO UPDATE
		/*scalegroup*/
		shape.tplftcrnr.y=this.group.top+(shape.tplftcrnr.y-this.group.top)/this.scale;
		/*scalegroup*/
		shape.btmrgtcrnr.y=this.group.top+(shape.btmrgtcrnr.y-this.group.top)/this.scale;
		shape.archeight/=this.scale;
		shape.arccentre.y=this.group.top+(shape.arccentre.y-this.group.top)/this.scale;
		if (shape.type=='rounded_rectangle')
		{
			shape.setRndRect();
		}else if (shape.type=='cota' || shape.type=='flecha')
		{
			changeDimensionCotaFlecha(shape, this, "y", "reduce", justOneShapeSelected);
		}
		else{
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.y=this.group.top+(node.point.y-this.group.top)/this.scale;
				if(node.ctrl1.x!="end")// will also this.scale when curve has been set as straight line
				{
					node.ctrl1.y=this.group.top+(node.ctrl1.y-this.group.top)/this.scale;
					node.ctrl2.y=this.group.top+(node.ctrl2.y-this.group.top)/this.scale;
				}
				node=node.next;
			}
		}
		if( justOneTime && (!justOneShapeSelected) ){
			justOneTime = false;
			this.group.disupdate(this.l-PNLLEFT,this.t-PNLTOP,0,0,1,this.scale);
		}
		shape.draw();
	}
	undoRedoActionGlobal();
}

/**
* @class crtl1NodeCommand
*
* @param	Object		node					"Referencia al objeto Node"
* @param	Object		positionIni		"Par ordenado(x,y) para la posición inicial"
* @param	Object		positionFin		"Par ordenado(x,y) para la posición inicial"
* @return	this									"Retorna el objeto"
*/
crtl1NodeCommand = function(node, positionIni, positionFin) {
	this.node = node;
	this.positionIni = positionIni;
	this.positionFin = positionFin;
}

crtl1NodeCommand.prototype.execute = function() {
	this.node.updateCtrl1Node(this.positionFin);
}

crtl1NodeCommand.prototype.redo = function() {
	clear(document.getElementById("markerdrop"));
	document.getElementById("backstage").style.visibility="hidden";
	this.execute();
	undoRedoActionGlobal();
}

crtl1NodeCommand.prototype.undo = function() {
	clear(document.getElementById("markerdrop"));
	document.getElementById("backstage").style.visibility="hidden";
	this.node.updateCtrl1Node(this.positionIni);
	undoRedoActionGlobal();
}

/**
* @class crtl2NodeCommand
*
* @param	Object		node					"Referencia al objeto Node"
* @param	Object		positionIni		"Par ordenado(x,y) para la posición inicial"
* @param	Object		positionFin		"Par ordenado(x,y) para la posición inicial"
* @return	this									"Retorna el objeto"
*/
crtl2NodeCommand = function(node, positionIni, positionFin) {
	this.node = node;
	this.positionIni = positionIni;
	this.positionFin = positionFin;
}

crtl2NodeCommand.prototype.execute = function() {
	this.node.updateCtrl2Node(this.positionFin);
}

crtl2NodeCommand.prototype.redo = function() {
	clear(document.getElementById("markerdrop"));
	document.getElementById("backstage").style.visibility="hidden";
	this.execute();
	undoRedoActionGlobal();
}

crtl2NodeCommand.prototype.undo = function() {
	clear(document.getElementById("markerdrop"));
	document.getElementById("backstage").style.visibility="hidden";
	this.node.updateCtrl2Node(this.positionIni);
	undoRedoActionGlobal();
}

/**
* @class pointNodeCommand
*
* @param	Object		node					"Referencia al objeto Node"
* @param	Object		positionIni		"Par ordenado(x,y) para la posición inicial"
* @param	Object		positionFin		"Par ordenado(x,y) para la posición inicial"
* @return	this									"Retorna el objeto"
*/
pointNodeCommand = function(node, positionIni, positionFin) {
	this.node = node;
	this.positionIni = positionIni;
	this.positionFin = positionFin;
	this.selectedShape = SELECTEDSHAPE;
}

pointNodeCommand.prototype.execute = function() {
	this.node.updatePointNode(this.positionFin);
}

pointNodeCommand.prototype.redo = function() {
	SELECTEDSHAPE = this.selectedShape;
	markLine();
	this.node.updatePointNode(this.positionFin);
	clear(document.getElementById("markerdrop"));
	resetBackstage();
	undoRedoActionGlobal();
}

pointNodeCommand.prototype.undo = function() {
	SELECTEDSHAPE = this.selectedShape;
	markLine();
	this.node.updatePointNode(this.positionIni);
	clear(document.getElementById("markerdrop"));
	resetBackstage();
	undoRedoActionGlobal();
}

/**
* @class flipVertCommand
*
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		mirrorX			"Valor referencial para eje Horizontal"
* @param	Integer		quantity		"Cantidad de Shapes seleccionados"
* @return	this									"Retorna el objeto"
*/
flipVertCommand = function(shapeNames, group, mirrorX, quantity) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.mirrorX = mirrorX;
	this.shapesQuantity = quantity;
}

flipVertCommand.prototype.execute = function() {
	var type = '';
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		type = shape.type;
		var node=shape.path.next;
		while(node.point.x!="end")
		{
			node.point.y=this.mirrorX-(node.point.y-this.mirrorX);
			if(node.ctrl1.x!="non")
			{
				node.ctrl1.y=this.mirrorX-(node.ctrl1.y-this.mirrorX);
				node.ctrl2.y=this.mirrorX-(node.ctrl2.y-this.mirrorX);
			}
			node=node.next;
		}
		var tplftcrnr=this.mirrorX-(shape.btmrgtcrnr.y-this.mirrorX);
		shape.btmrgtcrnr.y=this.mirrorX-(shape.tplftcrnr.y-this.mirrorX);
		shape.tplftcrnr.y=tplftcrnr;
		shape.lineGrad[1]=this.mirrorX-(shape.lineGrad[1]-this.mirrorX);
		shape.lineGrad[3]=this.mirrorX-(shape.lineGrad[3]-this.mirrorX);
		shape.radGrad[1]=this.mirrorX-(shape.radGrad[1]-this.mirrorX);
		shape.radGrad[4]=this.mirrorX-(shape.radGrad[4]-this.mirrorX);
		shape.shadowOffsetY=-shape.shadowOffsetY;
		shape.draw();shape.draw();//doing this twice gets rid of a stroke around the shadow do not know why?
	}
	this.group.drawBoundary(type, this.shapesQuantity);
}

flipVertCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

flipVertCommand.prototype.undo = function() {
	this.execute();
	undoRedoActionGlobal();
}

/**
* @class flipHorzCommand
*
* @param	Array			shapeNames	"Conjunto de objetos Shape"
* @param	Object		group				"Grupo al que pertenece un conjunto de Shapes"
* @param	Integer		mirrorY			"Valor referencial para eje vertical"
* @param	Integer		quantity		"Cantidad de Shapes seleccionados"
* @return	this									"Retorna el objeto"
*/
flipHorzCommand = function(shapeNames, group, mirrorY, quantity) {
	this.shapeNames = shapeNames;
	this.group = group;
	this.mirrorY = mirrorY;
	this.shapesQuantity = quantity;
}

flipHorzCommand.prototype.execute = function() {
	var type = '';
	for(var name in this.shapeNames)
	{
		shape=this.shapeNames[name];
		type = shape.type;
		var node=shape.path.next;
		while(node.point.x!="end")
		{
			node.point.x=this.mirrorY-(node.point.x-this.mirrorY);
			if(node.ctrl1.x!="non")
			{
				node.ctrl1.x=this.mirrorY-(node.ctrl1.x-this.mirrorY);
				node.ctrl2.x=this.mirrorY-(node.ctrl2.x-this.mirrorY);
			}
			node=node.next;
		}
		var tplftcrnr=this.mirrorY-(shape.btmrgtcrnr.x-this.mirrorY);
		shape.btmrgtcrnr.x=this.mirrorY-(shape.tplftcrnr.x-this.mirrorY);
		shape.tplftcrnr.x=tplftcrnr;
		shape.lineGrad[0]=this.mirrorY-(shape.lineGrad[0]-this.mirrorY);
		shape.lineGrad[2]=this.mirrorY-(shape.lineGrad[2]-this.mirrorY);
		shape.radGrad[0]=this.mirrorY-(shape.radGrad[0]-this.mirrorY);
		shape.radGrad[3]=this.mirrorY-(shape.radGrad[3]-this.mirrorY);
		shape.shadowOffsetX=-shape.shadowOffsetX;
		shape.draw();shape.draw();//doing this twice gets rid of a stroke around the shadow do not know why?
	}
	this.group.drawBoundary(type, this.shapesQuantity);
}

flipHorzCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

flipHorzCommand.prototype.undo = function() {
	this.execute();
	undoRedoActionGlobal();
}

/**
* @class shapeRotateCommand
*
* @param	Integer		phiIni	"Ángulo inicial"
* @param	Integer		phiFin	"Ángulo final"
* @return	this									"Retorna el objeto"
*/
shapeRotateCommand = function(phiIni, phiFin) {
	this.selectedShape = SELECTEDSHAPE;
	this.selected = SELECTED;
	this.phiIni = phiIni;
	this.phiFin = phiFin
}

shapeRotateCommand.prototype.execute = function() {
	SELECTEDSHAPE = this.selectedShape;
	SELECTED = this.selected;
	updateangle(this.phiFin);
}

shapeRotateCommand.prototype.redo = function() {
	this.execute();

	removeRotate();
	resetBoundary();
	undoRedoActionGlobal();
}

shapeRotateCommand.prototype.undo = function() {
	SELECTEDSHAPE = this.selectedShape;
	SELECTED = this.selected;
	updateangle(this.phiIni);

	removeRotate();
	resetBoundary();
	undoRedoActionGlobal();
}

/**
* @class editInputTextCommand
*
* @param	Object		textShape		"Objecto referencial"
* @return	this									"Retorna el objeto"
*/
editInputTextCommand = function(textShape) {
	this.text = textShape;
	this.textPrevValue = textShape.textPrevValue;
	this.textNewValue = textShape.inputText.value();
}

editInputTextCommand.prototype.execute = function() {
	this.text.textValue = this.textNewValue;
	this.text.inputText.value(this.textNewValue);
	this.text.textPrevValue = this.text.inputText.value();
}

editInputTextCommand.prototype.redo = function() {
	this.execute();
	undoRedoActionGlobal();
}

editInputTextCommand.prototype.undo = function() {
	this.text.textValue = this.textPrevValue;
	this.text.inputText.value(this.textPrevValue);
	this.text.textPrevValue = this.text.inputText.value();
	undoRedoActionGlobal();
}

/**
* @function undoRedoActionGlobal
* Remueve elementos del canvas necesarios para evitar errores
* @return	 undefined		"No devuelve ningun valor"
*/
function undoRedoActionGlobal()
{
	SELECTED = {};
	resetBoundary();
	removeRotate();
	if( document.getElementById("right-menu-secction") != null && document.getElementById("right-menu-secction") != undefined){
		document.getElementById("right-menu-secction").style.visibility = "hidden";
	}
}

/**
* @class ChangeZIndexCommand
* Cambia de z-index los elementos seleccionados puede ser sendBack o bringFront
* @param	Array		shapeList		"Arreglo de cada shape con: name y z-index"
* @param	String		type			"Typo de ejecucion: type=-1 sendBack o type=1 bringFront"
* @return	undefined					"No retorna"
*/
ChangeZIndexCommand = function(shapeList, type) {
	this.shapeList = shapeList;
	this.type = type;
}

ChangeZIndexCommand.prototype.execute = function() {
	var self=this;
	var shapeList=self.shapeList;
	for (var i=0; i<shapeList.length;i++)
	{
		shape = CURRENT[ shapeList[i][0] ];
		shape.redozIndex.push( shape.zIndex );
		if(self.type=="-1"){
			shape.zIndex=ZNEG--;
		}else if(self.type=="+1"){
			shape.zIndex=ZPOS++;
		}
		shape.Canvas.style.zIndex=shape.zIndex;
	}
}

ChangeZIndexCommand.prototype.redo = function() {
	this.execute();
	resetBoundary();
	undoRedoActionGlobal();
}

ChangeZIndexCommand.prototype.undo = function() {
	var self=this;
	var shapeList=self.shapeList;
	for (var i=0; i<shapeList.length;i++)
	{
		shape = CURRENT[ shapeList[i][0] ];
		shape.zIndex=shape.redozIndex[shape.redozIndex.length-1];
		shape.redozIndex.splice(-1);
		if(self.type=="-1"){
			ZNEG++;
		}else if(self.type=="+1"){
			ZPOS--;
		}
		shape.Canvas.style.zIndex=shape.zIndex;
	}
}

/**
* @function getCornersCota
* Para los casos de cota permite recalcular los nuevos nodos ya redimensionados tomando en cuenta
* solo los nodos de las esquinas del shape cota
* @param		Shape			shape		            "Forma determinada"
* @param		Group			parameter		        "Valor del boundary a redimensionar"
* @param		String			type		            "val: x o val:y, permite escalar las absisas x o y o ambas"
* @param		String			reduce_or_increase		"valor: reduce para reducir el nodo, caso contrario incrementa"
* @return		Object					                "Devuelve los puntos referentes a las esquinas del objeto"
*/
function getCornersCota(shape, parameter, type, reduce_or_increase){
	var scale = {x:1,y:1};
	if( type=="x" )
		scale.x = parameter.scale;
	else if ( type == "y"){
		scale.y = parameter.scale;
	}else if (type == "xy" ){
		scale.x = parameter.scale;
		scale.y = parameter.scale;
	}

	var node  = shape.path.next;
	var cornerPoint= node.next.next.next.next.point;
	var start = shape.path.next;
	var point1= start.point;
	if( reduce_or_increase == "reduce" ){
		cornerPoint.x  = parameter.group.left + (cornerPoint.x-parameter.group.left) / scale.x;
		cornerPoint.y  = parameter.group.top  + (cornerPoint.y-parameter.group.top)  / scale.y;
		point1.x  = parameter.group.left + (point1.x-parameter.group.left) / scale.x;
		point1.y  = parameter.group.top  + (point1.y-parameter.group.top)  / scale.y;
	}else{
		cornerPoint.x  = parameter.group.left + (cornerPoint.x-parameter.group.left) * scale.x;
		cornerPoint.y  = parameter.group.top  + (cornerPoint.y-parameter.group.top)  * scale.y;
		point1.x  = parameter.group.left + (point1.x-parameter.group.left) * scale.x;
		point1.y  = parameter.group.top  + (point1.y-parameter.group.top)  * scale.y;
	}

	return {
		point1: point1,
		point2: cornerPoint
	};
}

/**
* @function getCornersArrow
* Para los casos de flecha permite recalcular los nuevos nodos ya redimensionados tomando en cuenta
* solo los nodos de las esquinas del shape flecha
* @param		Shape			shape		            "Forma determinada"
* @param		Group			parameter		        "Valor del boundary a redimensionar"
* @param		String			type		            "val: x o val:y, permite escalar las absisas x o y o ambas"
* @param		String			reduce_or_increase		"valor: reduce para reducir el nodo, caso contrario incrementa"
* @return		Object					                "Devuelve los puntos referentes a las esquinas del objeto"
*/
function getCornersArrow(shape, parameter, type, reduce_or_increase){
	// var node       = shape.path.next;
	// var nodesNStack =[];
	// while(node.point.x!="end"){
	// 	var next = node.next;
	// 	if(next.point.x!="end"){
	// 		var difxs = node.point.x - next.point.x;
	// 		var difys = node.point.y - next.point.y;
	// 		var distancia = Math.sqrt( Math.pow(difxs,2) + Math.pow(difys,2) );
	// 		nodesNStack.push({
	// 			ini  : {x:node.point.x,y:node.point.y},
	// 			dist : distancia,
	// 			fin  : {x:next.point.x,y:next.point.y}
	// 		});
	// 	}
	// 	node = next;
	// }
	// 
	var scale = {x:1,y:1};
	if( type=="x" )
		scale.x = parameter.scale;
	else if ( type == "y"){
		scale.y = parameter.scale;
	}else if (type == "xy" ){
		scale.x = parameter.scale;
		scale.y = parameter.scale;
	}

	var node       = shape.path.next;
	var cornerPoint= node.next.next.next.point;
	var point1= node.next.next.next.next.next.next.point;
	if( reduce_or_increase == "reduce" ){
		cornerPoint.x  = parameter.group.left + (cornerPoint.x-parameter.group.left) / scale.x;
		cornerPoint.y  = parameter.group.top  + (cornerPoint.y-parameter.group.top)  / scale.y;
		point1.x  = parameter.group.left + (point1.x-parameter.group.left) / scale.x;
		point1.y  = parameter.group.top  + (point1.y-parameter.group.top)  / scale.y;
	}else{
		cornerPoint.x  = parameter.group.left + (cornerPoint.x-parameter.group.left) * scale.x;
		cornerPoint.y  = parameter.group.top  + (cornerPoint.y-parameter.group.top)  * scale.y;
		point1.x  = parameter.group.left + (point1.x-parameter.group.left) * scale.x;
		point1.y  = parameter.group.top  + (point1.y-parameter.group.top)  * scale.y;
	}

	return {
		point1: point1,
		point2: cornerPoint
	};
}

/**
* @function changeDimensionCotaFlecha
* Función general para redimensionar los nodos y que recalcula todos los nodos
* de acuuerdo a si el tipo de shape sea cota o flecha
* @param		Shape			shape		            "Forma determinada"
* @param		Group			parameter		        "Valor del boundary a redimensionar"
* @param		String			type		            "val: x o val:y, permite escalar las absisas x o y o ambas"
* @param		String			reduce_or_increase		"valor: reduce para reducir el nodo, caso contrario incrementa"
* @return		Object					                "Devuelve los puntos referentes a las esquinas del objeto"
*/
function changeDimensionCotaFlecha(shape, parameter, abscissa, reduce_or_increase, justOneShapeSelected){
	var start=shape.path.next;
	var newNodes;
	var cursor     = {x:0,y:0};
	var node       = shape.path.next;
	if (shape.type=='cota'){
		var corners = getCornersCota(shape, parameter, abscissa, reduce_or_increase);
		var puntos = getCotaPoints({point1:corners.point1,point2:corners.point2});
		newNodes = [puntos.point1, puntos.point3, puntos.point4, puntos.point1,
					puntos.point2, puntos.point6, puntos.point5,
					puntos.point2, puntos.point1, puntos.point1 ];
	}else if(shape.type=='flecha'){
		var corners = getCornersArrow(shape, parameter, abscissa, reduce_or_increase);
		var puntos = getFlechaPoints({point1:corners.point1,point2:corners.point2});
		newNodes = [puntos.point1, puntos.pointmedio, puntos.point3, puntos.point2,
					puntos.point4, puntos.pointmedio, puntos.point1, puntos.pointmedio ];
	}
	var i = 0;
	while(node.point.x!="end")
	{
		node.point.x=parameter.group.left+(newNodes[i%newNodes.length].x-parameter.group.left);
		node.point.y=parameter.group.top+(newNodes[i%newNodes.length].y-parameter.group.top);	
		i++;
		node=node.next;
	}
	if( justOneShapeSelected ){
		shape.fixCorners();
		shape.setCorners();
		if (parameter.group!=undefined && parameter.group!=null)
		{
			parameter.group.left= shape.tplftcrnr.x;
			parameter.group.top = shape.tplftcrnr.y;
			parameter.group.width=shape.btmrgtcrnr.x-shape.tplftcrnr.x;
			parameter.group.height=shape.btmrgtcrnr.y-shape.tplftcrnr.y;
			parameter.group.centreOfRotation.x=parameter.group.left+parameter.group.width/2;
			parameter.group.centreOfRotation.y=parameter.group.top+parameter.group.height/2;
		}
	}
}
