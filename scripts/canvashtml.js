﻿/**
* @function exportshapetext
* Función para obtener el script que dibujará el shape[name] en el canvas
* @param		Object		name				"Hace referencia al nombre del Shape"
* @return		String		scriptCanvas		"Es el resultado en forma de texto para que luego sea ejecutado junto con los otros scripts de los canvas"
*/
function exportshapetext(name, boundariesPNG)
{
	addTrackFunction("canvashtml","exportshapetext");
	var shape       = SHAPES[name];
	var scriptText  = "";
	scriptText += ( ' ' +"\n" );
	scriptText += ( SPACES.substr(0,15)+'//'+shape.name+';' +"\n" );
	if( shape.type == "text" || shape.type == "text_bold" || shape.type == "text_italic"){
		scriptText += getScriptText(shape, boundariesPNG);
	}else{
		scriptText += getScriptShape(shape, boundariesPNG);
	}
	return scriptText;
}

/**
* @function getBoundariestoBuildPNG
* Calcula los limites del área a dibujar
*  1. la esquina superior izquierda y la esquina inferior derecha del Canvas
*  2. La razon por la que se agrándara o achicará la imagen en el PNG
*  3. El padding o relleno del dibujo del PNG
* Esta funcion luego será usada para recalcular la imagen
* @return		Object		...				"Objeto con 5 atributos con las esquinas, la razon, padding"
*/
function getBoundariestoBuildPNG(){
	var topLeft     = {
		x: PNLWIDTH*100000,
		y: PNLHEIGHT*100000
	};
	var bottomRight = {
		x: -1,
		y: -1
	};
	
	// Cambios para no dibujar los nodos fuera del canvas en el PNG, topLeft minimo 0
	// bottomRight maximo PNLHEIGHT,PNLWIDTH
	for (var name in SHAPES)
	{
		var shape = SHAPES[name];
		if( ( shape.type=="text" || shape.type=="text_bold" || shape.type=="text_italic" ) &&
			( shape.textValue == "" || shape.textValue == undefined ) ) {
			continue;
		}
		topLeft.x = Math.max( Math.min( topLeft.x , shape.tplftcrnr.x) , 0 );
		topLeft.y = Math.max( Math.min( topLeft.y , shape.tplftcrnr.y) , 0 );
		
		bottomRight.x = Math.min( Math.max( bottomRight.x , shape.btmrgtcrnr.x) , PNLWIDTH );
		bottomRight.y = Math.min( Math.max( bottomRight.y , shape.btmrgtcrnr.y) , PNLHEIGHT );
	}
	var visibleArea    = 0.90;
	var paddingArea    = 0.05;

	var widthNodes = bottomRight.x - topLeft.x;
	var heightNodes = bottomRight.y - topLeft.y;
	var razonX   = EDITOR_CANVAS.widthImage / widthNodes;
	var razonY   = EDITOR_CANVAS.heightImage / heightNodes;
	var razon    = ( ( razonX > razonY ) ? razonY : razonX ) * visibleArea;
	var paddingX = ( EDITOR_CANVAS.widthImage * visibleArea - widthNodes * razon ) / 2 
					+ paddingArea * EDITOR_CANVAS.widthImage;
	var paddingY = ( EDITOR_CANVAS.heightImage * visibleArea - heightNodes * razon ) / 2 
					+ paddingArea * EDITOR_CANVAS.heightImage;
	
	var razonTextoX = EDITOR_CANVAS.widthImage / PNLWIDTH;
	var razonTextoY = EDITOR_CANVAS.widthImage / PNLHEIGHT;
	var razonTexto  = ( ( razonTextoX > razonTextoY ) ? razonTextoY : razonTextoX ) * visibleArea;
	return {
		topLeft     : topLeft,
		bottomRight : bottomRight,
		razon       : razon, 
		razonTexto  : SCALETEXT,
		paddingX    : paddingX,
		paddingY    : paddingY
	};
}

/**
* @function recalculate
* Función para recalcular la posicion "value" del canvas al pasar al PNG
* tomando como parametro si es de la coordenada X o Y
* El cálculo para recalcular es calcular la distancia de la esquina superior izquierda del shape
* a la esquina superior izquierda del área que se dibujará, luego se multiplica por la razón
* y finalmente se le añade un padding o relleno en el canvas
* Todo este cálculo puede ser para la coordenada X o Y, DEPENDIENDO DEL VALOR DE "coordenada"
* @param		String		coordenada		"valores x o y, representa el nombre de la coordenada"
* @param		Integer		value			"Posición a recalcular"
* @return		Object		...				"Retorna el valor actualizado para poder posicionarlo en el PNG"
*/
function recalculate(boundaries, coordenada, value){
	// return value;
	return Math.floor( (value - boundaries["topLeft"][coordenada]) * boundaries.razon + 
			boundaries["padding"+coordenada.toUpperCase()] );
}

/**
* @function getScriptText
* Función para obtener el script que dibujará el inputText en el canvas
* Este código es una adaptación del método render de la clase CanvasInput, 
* el cual permite dibujar en un canvas con las propiedades de la peopia clase
* @param		Object		shape				"Es el objeto Shape que hace referencia al texto guardado en el Shape"
* @param		Object		boundariesPNG		"Es la salida de la funcion getBoundariestoBuildPNG(), que permite obtener los valores para la redimensión"
* @return		String		scriptCanvas		"Es el resultado en forma de texto para que luego sea ejecutado junto con los otros scripts de los canvas"
*/
function getScriptText(shape, boundariesPNG){
	// initializing value to be needed
	var input          = shape.inputText;
	var canvasAuxiliar = document.createElement('canvas');
	var scriptCanvas   = '';
	var originalProperiesCanvas = {
		canvas       : input.canvas(),
		x            : input.x(),
		y            : input.y(),
		fontSize     : input.fontSize(),
		width        : input.width(),
		height       : input.height(),
		padding      : input.padding(),
		borderWidth  : input.borderWidth(),
		borderRadius : input.borderRadius()
	}

	canvasAuxiliar.setAttribute('width', EDITOR_CANVAS.widthImage );
	canvasAuxiliar.setAttribute('height', EDITOR_CANVAS.heightImage );
	
	// data to scale the text 
	// based in the horizontal center of the text
	var realWidthInput   = input.width() - STANDARFONTSIZE + 10;
	var posXInputCenter  = recalculate( boundariesPNG, "x", shape.tplftcrnr.x + realWidthInput / 2);
	var totalWidthInput  = realWidthInput * boundariesPNG.razonTexto;
	var posX             = posXInputCenter - totalWidthInput / 2;
	if( originalProperiesCanvas.x > 0 ){
		posX = Math.max( posX,boundariesPNG.paddingX);
	}

	// update the dimensions of the text according to scale
	input.canvas( canvasAuxiliar );
	input.x( posX );
	input.y( recalculate( boundariesPNG, "y", shape.tplftcrnr.y ) );
	input.fontSize( input.fontSize() * boundariesPNG.razonTexto );
	input.width( totalWidthInput );
	input.height( input.height() * boundariesPNG.razonTexto );
	input.padding( input.padding() * boundariesPNG.razon );

	input.borderWidth( input.borderWidth() * boundariesPNG.razon );
	input.borderRadius( input.borderRadius() * boundariesPNG.razon );

	// creating canvas
	input._calcWH();
	scriptCanvas += SPACES.substr(0,15) + "var canvasHidden = {};" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "canvasHidden._renderCanvas = document.createElement('canvas');" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "canvasHidden._renderCanvas.setAttribute('width', " + input.outerW + ");" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "canvasHidden._renderCanvas.setAttribute('height', " + input.outerH + ");" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "canvasHidden._renderCtx = canvasHidden._renderCanvas.getContext('2d');" + "\n";
		
	// Render - after initializing all properties
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden = canvasHidden._renderCtx;" + "\n";

	// drawTextBox
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowOffsetX = 0;" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowOffsetY = 0;" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowBlur = 0;" + "\n";
	var text = input._clipText();
	var paddingBorder = input._padding + input._borderWidth + input.shadowT;
	if (input._selection[1] > 0) {
		var selectOffset = input._textWidth(text.substring(0, input._selection[0])),
		selectWidth = input._textWidth(text.substring(input._selection[0], input._selection[1]));
		scriptCanvas += SPACES.substr(0,15) + "ctxHidden.fillStyle = '" + input._selectionColor + "'; " + "\n";
		scriptCanvas += SPACES.substr(0,15) + "ctxHidden.fillRect(" + paddingBorder + " + " + selectOffset +
						", " + paddingBorder + ", " + selectWidth + ", " + input._height + ");" +  "\n";
	}
	// getValues to write in the correct posistion
	var textX = input._padding + input._borderWidth + input.shadowL,
	textY = Math.round(paddingBorder + input._height / 2);
	text = (text === '' && input._placeHolder) ? input._placeHolder : text;
	
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.fillStyle = '" + ( (input._value !== '' && input._value !== input._placeHolder) ? 
						input._fontColor : input._placeHolderColor ) + "';\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.font = '" + input._fontStyle + ' ' +
						input._fontWeight + ' ' + input._fontSize + 'px ' + input._fontFamily + "';" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowColor = " + (input._fontShadowColor)?input._fontShadowColor:"''" + ";" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowBlur = " + input._fontShadowBlur + ";" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowOffsetX = " + input._fontShadowOffsetX + ";" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.shadowOffsetY = " + input._fontShadowOffsetY + ";" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.textAlign = 'left'; " + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.textBaseline = 'middle'; " + "\n";
	scriptCanvas += SPACES.substr(0,15) + "ctxHidden.fillText('" + text + "'," + textX + ", " + textY + ");" + "\n";

	// draw the textbox in the visible context (ctx)
	scriptCanvas += SPACES.substr(0,15) + "if (ctx) {" + "\n";
	scriptCanvas += SPACES.substr(0,18) + "ctx.drawImage(canvasHidden._renderCanvas," + input._x + "," + input._y + ");" + "\n";
	scriptCanvas += SPACES.substr(0,15) + "}" + "\n";

	// retrieving the original properties and render
	for( var x in originalProperiesCanvas ){
		input[x](originalProperiesCanvas[x])
	}
	input._ctx.clearRect(0,0,input._ctx.canvas.width,input._ctx.canvas.height)
	input._calcWH();
	input.render();
	return scriptCanvas;
}

/**
* @function getScriptShape
* Función para obtener el script que dibujará el shape en el canvas
* Este código es una duplicacion de lo que estaba en la funcion exportshapetext o canvashtml()
* @param		Object		shape				"Es el objeto Shape que hace referencia a la figura guardado en el Shape"
* @param		Object		boundariesPNG		"Es la salida de la funcion getBoundariestoBuildPNG(), que permite obtener los valores para la redimensión"
* @return		String		scriptCanvas		"Es el resultado en forma de texto para que luego sea ejecutado junto con los otros scripts de los canvas"
*/
// CONSTANTE
function adjustLineWidth(){return SCALELINEWIDTH;}
function getScriptShape(shape, boundariesPNG){
	var scriptText = '';
	scriptText += ( SPACES.substr(0,15)+'ctx.shadowColor ="rgba(0,0,0,0)";' +"\n" );
	if(shape.dotted){
		scriptText += ( SPACES.substr(0,15)+'ctx.setLineDash(['+
			shape.linedasha+','+ shape.linedashb+'])' +"\n" );
	}
	else {
		scriptText += ( SPACES.substr(0,15)+'ctx.setLineDash([]);' + "\n" );
	}
	var rule='rgba('
	for (var j=0;j<3;j++)
	{
		rule += shape.strokeStyle[j]+',';
	}
	rule +=shape.strokeStyle[j]+')';
	scriptText += ( SPACES.substr(0,15)+'ctx.strokeStyle ="'+rule+'";' + "\n" );
	scriptText += ( SPACES.substr(0,15)+'ctx.lineWidth = '+ ( shape.lineWidth * adjustLineWidth() ) +';' + "\n" );
	scriptText += ( SPACES.substr(0,15)+'ctx.lineCap = "'+shape.lineCap+'";' + "\n" );
	scriptText += ( SPACES.substr(0,15)+'ctx.lineJoin = "'+shape.lineJoin+'";' + "\n" );

	scriptText += ( SPACES.substr(0,15)+'ctx.beginPath();' + "\n" );

	var node=shape.path.next;
	scriptText += ( SPACES.substr(0,15)+'ctx.moveTo('+( recalculate( boundariesPNG, "x",Math.round(node.point.x) ) )+','+( recalculate( boundariesPNG, "y",Math.round(node.point.y) ) )+');' + "\n" );
	node=node.next;
	while(node.point.x!="end")
	{
		if (node.vertex=="L")
		{
			scriptText += ( SPACES.substr(0,15)+'ctx.lineTo('+( recalculate( boundariesPNG, "x",Math.round(node.point.x) ) )+','+( recalculate( boundariesPNG, "y",Math.round(node.point.y) ) )+');' + "\n" );
		}


		else
		{
			scriptText += ( SPACES.substr(0,15)+'ctx.bezierCurveTo('+( recalculate( boundariesPNG, "x", Math.round(node.ctrl1.x) ) )+','+( recalculate( boundariesPNG, "y", Math.round(node.ctrl1.y) ) )+','+( recalculate( boundariesPNG, "x", Math.round(node.ctrl2.x) ) )+','+( recalculate( boundariesPNG, "y", Math.round(node.ctrl2.y) ) )+','+( recalculate( boundariesPNG, "x", Math.round(node.point.x) ) )+','+( recalculate( boundariesPNG, "y", Math.round(node.point.y) ) )+');' + "\n" );
		}
		node=node.next;
	}
	if (!shape.open) {scriptText += ( SPACES.substr(0,15)+'ctx.closePath();' + "\n" );}
	scriptText += ( SPACES.substr(0,15)+'ctx.stroke();' + "\n" );
	if (!shape.open)
	{
		scriptText += ( SPACES.substr(0,15)+'ctx.shadowOffsetX = '+shape.shadowOffsetX+';' + "\n" );
		scriptText += ( SPACES.substr(0,15)+'ctx.shadowOffsetY = '+shape.shadowOffsetY+';'   + "\n" );
		scriptText += ( SPACES.substr(0,15)+'ctx.shadowBlur = '+shape.shadowBlur+';' + "\n" );
		var rule='rgba('
		for (var j=0;j<3;j++)
		{
			rule += shape.shadowColor[j]+',';
		}
		rule +=shape.shadowColor[j]+')';
		scriptText += ( SPACES.substr(0,15)+'ctx.shadowColor = "'+rule+'";' + "\n" );
		if (shape.justfill)
		{
			var rule='rgba('
			for (var j=0;j<3;j++)
			{
				rule += shape.fillStyle[j]+',';
			}
			rule +=shape.fillStyle[j]+')';
			scriptText += ( SPACES.substr(0,15)+'ctx.fillStyle = "'+rule+'";' + "\n" );
		}
		else
		{
			if (shape.linearfill)
			{
				scriptText += ( SPACES.substr(0,15)+'grad = ctx.createLinearGradient('+(Math.round(shape.lineGrad[0]))+','+(Math.round(shape.lineGrad[1]))+','+(Math.round(shape.lineGrad[2]))+','+(Math.round(shape.lineGrad[3]))+');' + "\n" );
			}
			else
			{
				scriptText += ( SPACES.substr(0,15)+'grad = ctx.createRadialGradient('+(Math.round(shape.radGrad[0]))+','+(Math.round(shape.radGrad[1]))+','+Math.round(shape.radGrad[2])+','+(Math.round(shape.radGrad[3]))+','+(Math.round(shape.radGrad[4]))+','+Math.round(shape.radGrad[5])+');' + "\n" );
			}
			var rule;
			for (var k=0; k<shape.colorStops.length;k++)
			{
				rule='rgba('
				for (var j=1;j<4;j++)
				{
					rule += shape.colorStops[k][j]+',';
				}
				rule +=shape.colorStops[k][j]+')';
				scriptText += ( SPACES.substr(0,15)+'grad.addColorStop('+shape.colorStops[k][0]+',"'+rule+'");' + "\n" );
			}
			scriptText += ( SPACES.substr(0,15)+'ctx.fillStyle = grad;' + "\n" );
		}
		scriptText += ( SPACES.substr(0,15)+'ctx.fill();' + "\n" );
	}
	return scriptText;
}