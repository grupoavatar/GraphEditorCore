/**
* @class Group
*
* @param		Array			COLLECTION	"Lista de Shapes"
* @param		String		name				"Nombre del grupo"
* @param		String		title				"Titulo del grupo"
* @param		Object		shape				"Shape para agregar al grupo"
* @return		this									"Retorna el objeto"
*/
﻿function Group(COLLECTION,name,title,shape)   //Group object contains shapes and groups in group
{
	addTrackFunction("group","Group");

	this.elType="_GROUP";
	this.title=title;
	this.name=name;
	this.members=[];
	this.left;
	this.top;
	this.width;
	this.height;
	p=new Point(0,0);
	this.centreOfRotation=p;
	this.phi=0;  //angle of rotation
	this.clockwise=true;

	COLLECTION[this.name]=this;

	if (arguments.length > 3)
	{
		this.members.push(shape);
	}
	//methods
	this.memberShapes=memberShapes;
	this.drawBoundary=drawBoundary;
	this.removeBoundary=removeBoundary;
	this.groupRotate=groupRotate;
	this.update=update;
	this.disupdate=disupdate;
	this.GroupToText=GroupToText;
}

/**
* @function memberShapes
* Retorna la lista de Shapes del grupo
* @return		Array		"Lista de Shape"
*/
function memberShapes()
{
	addTrackFunction("group","memberShapes");
	var ms={};
	var list=separate(this.members);
	for (var i=0; i<list.length; i++)
	{
		ms[list[i].name]=list[i]
	}

	return ms;
}

/**
* @function separate
* Separa los Shapes que pertenecen al mismo grupo
* @param		Array		list	"Lista de Shapes"
* @return		Array					"Lista de Shapes"
*/
function separate(list)
{
	addTrackFunction("group","separate");

	var set=[];
	var collection;
	for (var i=0;i<list.length; i++)
	{
		collection=list[i];
		if (collection.elType=="_GROUP")
		{
			set=Union(set,separate(collection.members));
		}
		else  //collection is a shape
		{
			set.push(collection);
		}
	}
	return set;
}

/**
* @function Union
* Une los Shapes que pertenecen al mismo grupo
* @param		Array		A	"Lista de Shapes"
* @param		Array		B	"Lista de Shapes"
* @return		Array		S	"Lista de Shapes"
*/
function Union(A,B)
{
	var S=[];
	for(var i=0; i<A.length; i++)
	{
		S.push(A[i]);
	}
	for(var i=0; i<B.length; i++)
	{
		S.push(B[i]);
	}
	return S;
}

/**
* @function update
* Actualiza atributos del Shape luego de la rotación
* @param	Integer		l					"Nuevo valor para posición horizontal"
* @param	Integer		t					"Nuevo valor para posición vertical"
* @param	Integer		dx				"Valor equivalente al dezplazamiento horizontal"
* @param	Integer		dy				"Valor equivalente al dezplazamiento vertical"
* @param	Integer		scalew		"Valor escalar para alinear con la gria"
* @param	Integer		scaleh		"Valor escalar para alinear con la gria"
* @return		undefined					"No devuelve valor"
*/
function update(l,t,dx,dy,scalew,scaleh)
{
	addTrackFunction("group","update");
	this.left+=dx;
	this.top+=dy;
	this.left=l+(this.left-l)*scalew;
	this.top=t+(this.top-t)*scaleh;
	this.width*=scalew;
	this.height*=scaleh;
	this.centreOfRotation.x+=dx;
	this.centreOfRotation.y+=dy;
	this.centreOfRotation.x=l+(this.centreOfRotation.x-l)*scalew;
	this.centreOfRotation.y=t+(this.centreOfRotation.y-t)*scaleh;
	for (var i=0;i<this.members.length;i++)
	{
		if (this.members[i].elType=="_GROUP")
		{
			this.members[i].update(l,t,dx,dy,scalew,scaleh);
		}
	}
}

/**
* @function disupdate
* Actualiza atributos del Shape (vuelve al estado anterior)
* @param	Integer		l					"Nuevo valor para posición horizontal"
* @param	Integer		t					"Nuevo valor para posición vertical"
* @param	Integer		dx				"Valor equivalente al dezplazamiento horizontal"
* @param	Integer		dy				"Valor equivalente al dezplazamiento vertical"
* @param	Integer		scalew		"Valor escalar para alinear con la gria"
* @param	Integer		scaleh		"Valor escalar para alinear con la gria"
* @return		undefined					"No devuelve valor"
*/
function disupdate(l,t,dx,dy,scalew,scaleh)
{
	addTrackFunction("group","disupdate");
	this.left-=dx;
	this.top-=dy;
	this.left = (this.left-l)/scalew + l;
	this.top = (this.top-t)/scaleh + t;
	this.width/=scalew;
	this.height/=scaleh;
	this.centreOfRotation.x-=dx;
	this.centreOfRotation.y-=dy;
	this.centreOfRotation.x=(this.centreOfRotation.x-l)/scalew + l;
	this.centreOfRotation.y=(this.centreOfRotation.y-t)/scaleh + t;

	for (var i=0;i<this.members.length;i++)
	{
		if (this.members[i].elType=="_GROUP")
		{
			this.members[i].disupdate(l,t,dx,dy,scalew,scaleh);
		}
	}
}

/**
* @function groupRotate
* Rotación de Shapes grupal
* @param	Integer		phi			"Angulo de rotación"
* @return		undefined				"No devuelve valor"
*/
function groupRotate(phi)
{
	addTrackFunction("group","groupRotate");
	var members=this.memberShapes();  // all shapes within the group
	var cx=this.centreOfRotation.x;
	var cy=this.centreOfRotation.y;
	var p;
	var mnx=1000000;
	var mny=1000000;
	var mxx=-1000000;
	var mxy=-1000000;
	var type = '';

	for (var name in members)
	{
		shape=members[name];
		type = shape.type;
		//rotate gradients
		p=new Point(shape.lineGrad[0]-cx,shape.lineGrad[1]-cy);
		p=p.pointRotate(phi);
		shape.lineGrad[0]=p.x+cx;
		shape.lineGrad[1]=p.y+cy;
		p=new Point(shape.lineGrad[2]-cx,shape.lineGrad[3]-cy);
		p=p.pointRotate(phi);
		shape.lineGrad[2]=p.x+cx;
		shape.lineGrad[3]=p.y+cy;
		p=new Point(shape.radGrad[0]-cx,shape.radGrad[1]-cy);
		p=p.pointRotate(phi);
		shape.radGrad[0]=p.x+cx;
		shape.radGrad[1]=p.y+cy;
		p=new Point(shape.radGrad[3]-cx,shape.radGrad[4]-cy);
		p=p.pointRotate(phi);
		shape.radGrad[3]=p.x+cx;
		shape.radGrad[4]=p.y+cy;
		//rotate nodes
		node=shape.path.next;
		while (node.point.x!="end")
		{
			p=new Point(node.point.x-cx,node.point.y-cy);
			p=p.pointRotate(phi);
			node.point.x=p.x+cx;
			node.point.y=p.y+cy;
			if (node.ctrl1.x!="non") //changes when straight line set on curve
			{
				p=new Point(node.ctrl1.x-cx,node.ctrl1.y-cy);
				p=p.pointRotate(phi);
				node.ctrl1.x=p.x+cx;
				node.ctrl1.y=p.y+cy;
				p=new Point(node.ctrl2.x-cx,node.ctrl2.y-cy);
				p=p.pointRotate(phi);
				node.ctrl2.x=p.x+cx;
				node.ctrl2.y=p.y+cy;
			}
			node=node.next;
		}
		shape.setCorners();
		shape.draw();
		if (mnx>shape.tplftcrnr.x){mnx=shape.tplftcrnr.x};
		if (mny>shape.tplftcrnr.y){mny=shape.tplftcrnr.y};
		if (mxx<shape.btmrgtcrnr.x){mxx=shape.btmrgtcrnr.x};
		if (mxy<shape.btmrgtcrnr.y){mxy=shape.btmrgtcrnr.y};
	}

	this.left=mnx;
	this.top=mny;
	this.width=mxx-mnx;
	this.height=mxy-mny;
	resetBoundary();
	this.drawBoundary(type, 1);
}

/**
* @function copyGroup
* Realiza la copia del grupo
* @param		Object		group				"Grupo"
* @param		Integer		offset			"valor escalar para nuevo punto de origen del Shape"
* @param		Object		theatre			"Elemento capa"
* @param		Array			STORE				"Lista de Shapes"
* @param		Array			COLLECTION	"Lista de Shapes"
* @return		undefined				"No devuelve valor"
*/
function copyGroup(group,offset,theatre,STORE,COLLECTION)
{
	addTrackFunction("group","copyGroup");
	if (theatre.id=="shapestage")
	{
		var name="Group"+(GCOUNT++);
	}
	else
	{
		var name="SUBGP"+(NCOUNT++);
	}
	var groupcopy=new Group(COLLECTION,name,group.title);
	groupcopy.left=group.left+offset;
	groupcopy.top=group.top+offset;
	groupcopy.width=group.width;
	groupcopy.height=group.height;
	groupcopy.centreOfRotation.x=group.centreOfRotation.x+offset;
	groupcopy.centreOfRotation.y=group.centreOfRotation.y+offset;
	groupcopy.phi=group.phi;

	for (var i=0; i<group.members.length; i++)
	{
		if (group.members[i].elType=="_GROUP")
		{
			groupcopy.members.push(copyGroup(group.members[i],offset,theatre,STORE,COLLECTION));
		}
		else
		{
			var shape=group.members[i];
			var copy=makeCopy(shape,offset,theatre,STORE);
			groupcopy.members.push(copy);
			copy.group=groupcopy;
			if (theatre.id=="shapestage")
			{
				copy.draw();
			}
			// if (group.members[i].type=="text" || group.members[i].type=="text_bold" || group.members[i].type=="text_italic"){
			// 	var shape=group.members[i];
			// 	var copy=makeCopy(shape,offset,theatre,STORE);

			// 	groupcopy.members.push(copy);
			// 	copy.group=groupcopy;
			// 	if (theatre.id=="shapestage")
			// 	{
			// 		copy.draw();
			// 	}
			// }else{
			// 	var shape=group.members[i];
			// 	var copy=makeCopy(shape,offset,theatre,STORE);

			// 	groupcopy.members.push(copy);
			// 	copy.group=groupcopy;
			// 	if (theatre.id=="shapestage")
			// 	{
			// 		copy.draw();
			// 	}
			// }
		}
	}

	return groupcopy;
}

/**
* @function inAgroup
* Verifica si el objeto pertene a más de 1 grupo
* @return		undefined		"No devuelve valor"
*/
function inAgroup()
{
	addTrackFunction("group","inAgroup");
	return (this.group.members.length>1)
}

/**
* @function groupJoin
* Permite agrupar un conjunto de shapes, con la finalidad deresacalarlos, es la unica forma
* Sin emabrgo, es una funcion en stand by, no se ha completado, existen bugs
* por ello hace return desde un inicio
* @return		undefined		"No devuelve valor, pero cambia los estados de los shapes"
*/
function groupJoin()  //groups together array of  groups  place $("boundarydrop").childNodes[].group into any array SELgroup before using for group by boundaries
{
	// return;
	if( isInputText(SELgroup) ) return;
	addTrackFunction("group","groupJoin");
	var group=new Group(GROUPS,"Group"+GCOUNT,"Group"+(GCOUNT++));
	var SELgroup;
	var left=1000000;
	var top=1000000;
	var right=-1000000;
	var bottom=-1000000;
	for(var i=0;i<document.getElementById("boundarydrop").childNodes.length;i++)  //child nodes are drawn boundaries
	{
		 SELgroup=document.getElementById("boundarydrop").childNodes[i].group;  //gets group surrounded by boundary
		 group.members.push(SELgroup);                  // adds this to new group
		 left=Math.min(left,SELgroup.left);            // checks boundary position and size to ensure boundary of new group 
		 top=Math.min(top,SELgroup.top);
		 right=Math.max(right,SELgroup.left+SELgroup.width);
		 bottom=Math.max(bottom,SELgroup.top+SELgroup.height);
	}
	group.left=left;
	group.top=top;
	group.width=right-left;
	group.height=bottom-top;
	group.centreOfRotation.x=group.left+group.width/2;
	group.centreOfRotation.y=group.top+group.height/2;
	group.phi=0;
	var members=group.memberShapes();  // all shapes within the group 
	for (var shape in members)
	{
		members[shape].group=group;   //replace the group of each shape with the new group;
	}
	resetBoundary();
	document.getElementById('ungroup').style.visibility="visible";
	document.getElementById('editlines').style.visibility="hidden";
	document.getElementById('group').style.visibility="hidden";
	document.getElementById('alntop').style.visibility='hidden';
	document.getElementById('alnbot').style.visibility='hidden';
	document.getElementById('alnleft').style.visibility='hidden';
	document.getElementById('alnright').style.visibility='hidden';
	document.getElementById("sname").style.visibility="visible";
	document.getElementById("sname").alt="group name";
	document.getElementById("sname").title="group name";
	SELECTED={};
	SELECTED[group.name]=group;
	drawEndSelector.SELECTED={};//add shapecopy.SELECTED
	drawEndSelector.SELECTED[group.name]=group;
	group.drawBoundary();
}

/**
* @function ungroup
* Desagrupa los Shapes que pertenecen al mismo grupo
* La finalidad es desagrupar al terminar la seleccion de los shapes
* Sin emabrgo, es una funcion en stand by, no se ha completado, existen bugs
* por ello hace return desde un inicio 
* @return		undefined			"Ningún valor, cambia los estados de los shapes"
*/
function ungroup()
{
	// return; // en caso no se desee la funcionalidad de redimension en grupo
	addTrackFunction("group","ungroup");
	var members;
	// case for drawEndSelector
	if( ! ( drawEndSelector.isSelector ) && !(shapecopy.isselector) ) return;//add shapecopy.isselector
	if( document.getElementById("boundarydrop").childNodes[0] == undefined ) return;
	// addTrackFunction("true");
	var grouped=document.getElementById("boundarydrop").childNodes[0].group;
	if( ungroup.changeShapes ){
		var shapesBoundaries = document.getElementById("boundarydrop").childNodes;
		for (var i = 0; i < shapesBoundaries.length; i++) {
			if( shapesBoundaries[i] != ungroup.newShape.boundary ){
				shapesBoundaries[i].parentNode.removeChild(shapesBoundaries[i]);
			}
		};
		ungroup.changeShapes = false;
		ungroup.newShape = undefined;
	}
	else{ resetBoundary(); }
	SELECTED={};
	for(var i=0; i<grouped.members.length; i++)
	{
		group=grouped.members[i];
		SELECTED[group.name]=group;
		if (group.elType=="_SHAPE") {
			continue;
		};
		members=group.memberShapes();
		for(var shapename in members)
		{
			shape=members[shapename];
			shape.group=group;
		}
		group.drawBoundary();
	}
	document.getElementById('ungroup').style.visibility="hidden";
	document.getElementById('editlines').style.visibility="hidden";
	document.getElementById('group').style.visibility="visible";
	document.getElementById('alntop').style.visibility='visible';
	document.getElementById('alnbot').style.visibility='visible';
	document.getElementById('alnleft').style.visibility='visible';
	document.getElementById('alnright').style.visibility='visible';
	document.getElementById("sname").style.visibility="hidden";
	document.getElementById("sname").alt="shape name";
	document.getElementById("sname").title="shape name";
}
ungroup.changeShapes=false;