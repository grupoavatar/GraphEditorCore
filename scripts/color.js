/**
* @var COLOR
* contiene los colores del editor
*/
COLOR = {
	"yellow"	:	[255, 194, 0, 1],
	"pink"		:	[180, 84, 150, 1],
	"purple"	:	[122, 43, 126, 1],
	"blue"		: 	[8, 85, 161, 1],
	"black"		:	[46, 22, 85, 1],
	"white"		:	[255, 255, 255, 1],
	"transparent"	:  [0, 0, 0, 0],
	"question_grid"	:  [255,255,255,0.2],
	"answer_grid"	:  [0,0,0,0.1],
	"black_hexa"	:	"#040404",
	"white_hexa"	:	"#EFEFEF",
};

EDITOR_CANVAS  	= {};
LINECOLOR      	= [255,255,255,1];
BOUNDARYCOLOR		= "#E7F4F2";

FILLCOLORS = ["yellow","pink","purple","blue"];

// Constantes para reescalar la imagen
STANDARFONTSIZE = 28;
SCALETEXT = 3;
SCALELINEWIDTH = 6;

/**
* @function changeLineColor
* Actualiza los colores predeterminados para linea y boundary del shape
* @param		Array				lineColor				"Color de las lineas del Shape"
* @param		String			boundaryColor		"Color del boundary del Shape"
* @return		undefined										"No devuelve ningun valor"
*/
function changeLineColor(lineColor = null,boundaryColor = null){
	addTrackFunction("color","changeLineColor");
	if(lineColor != null){
		LINECOLOR = lineColor.slice(0);
	}
	if(boundaryColor != null){
		BOUNDARYCOLOR = boundaryColor.slice(0);
	}
}

/**
* @function initializeCanvas
* asigna propiedades al canvas para inicializar
* @param		Object			canvasProperties	"Objeto con los valores para inicializar canvas"
* @return		undefined											"No devuelve ningun valor"
*/
function initializeCanvas( canvasProperties ){
	addTrackFunction("color","initializeCanvas");
	EDITOR_CANVAS.type = canvasProperties.type;
	EDITOR_CANVAS.widthImage = canvasProperties.widthImage;
	EDITOR_CANVAS.heightImage = canvasProperties.heightImage;
	setStage(canvasProperties.width, canvasProperties.height);
	changeLineColor(canvasProperties.lineColor, canvasProperties.boundaryColor);
}
