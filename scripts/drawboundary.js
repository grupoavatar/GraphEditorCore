
var MINSIZE = 5;   // Shape min left and top
var MINSTEP = 10;      // drag step
var DRAGBORDER = 5;      // min space between node and border!!!!!

/**
* @function drawBoundary
* Dibuja el boundary del Shape
* @return		undefined		"No devuelve ningun valor"
*/
function drawBoundary(shapeType, shapesQuantity)
{
	addTrackFunction("drawboundary","drawBoundary");
	var self = this;
	this.boundary = document.createElement('div');
	this.boundary.id="b"+BCOUNT++;
	this.boundary.group=this;
	this.boundary.group.boundary=this.boundary;
	
	// Deseable de boundaries anchos
	// Descripcion: 
		// Boundaries en el contorno de la figura, a una distancia de 4px,
		// Salio un error al redimensionarlo y al hacer doble clic, sobre la figura
	// this.boundary.style.left= (this.left-4)+"px";
	// this.boundary.style.top= (this.top-4)+"px";
	// this.boundary.style.width=(8+this.width)+"px";
	// this.boundary.style.height=(8+this.height)+"px";

	this.boundary.style.left= (this.left)+"px";
	this.boundary.style.top= (this.top)+"px";
	this.boundary.style.width=(this.width)+"px";
	this.boundary.style.height=(this.height)+"px";

	this.boundary.style.position = "absolute";
	
	// Elige el zIndex mayor de los shapes del group a dibujar los boundaries
	var memberShapes = this.memberShapes();
	var zIndex = -1;
	for( var nameShape in memberShapes ){
		shapezIndex = memberShapes[nameShape].zIndex
		zIndex = (zIndex < shapezIndex ) ? shapezIndex : zIndex;
	}
	this.boundary.style.zIndex = zIndex;
	
	// Deseable de boundaries anchos
	// Descripcion:
		// Ancho del boundary 2px, width del shape 3px y selector 1 px
		// Pero sucedio error
	// this.boundary.style.border="dashed " + BOUNDARYCOLOR + " 2px";
	
	if (shapeType == 'text' || shapeType == 'text_italic' || shapeType == 'text_bold') {
		this.boundary.style.border="dashed " + BOUNDARYCOLOR + " 1px";
	}else{
		this.boundary.style.border="dashed " + BOUNDARYCOLOR + " 1px";
	}

	// In case the shapes have been selected
	// draw the corners be able to resize it
	if( this.boundary.group.name in SELECTED ){
		if (shapeType != 'text' && shapeType != 'text_italic' && shapeType != 'text_bold' 
			/* && shapesQuantity <= 1*/ ){
			var itHasText = isInputText(SELECTED);
			if( !itHasText )
				newCornersDD(this.boundary);
		}
	}

	// put always first the boundary in order to 
	// select it, its related to z-index
	var boundarydropHTML = document.getElementById("boundarydrop");
	boundarydropHTML.appendChild( this.boundary );
	
	this.boundary.DD=new YAHOO.util.DD(this.boundary.id);

	// to resize it in case of a group of shapes
	// if( drawEndSelector.isSelector && this.boundary.group.name in drawEndSelector.SELECTED ){
	// 	newCornersDD(this.boundary);
	// }
	
	// initialize the SELECTED Shapes
	this.boundary.onmousedown=function(e) {
		addTrackFunction("drawboundary","boundarydrop onmousedown1");
		// unable to move anothers shapes
		clearAllMouseEvent();
		if( ! this.group.name in SELECTED ){
			// ungroup();
			if (shapeType != 'text' && shapeType != 'text_italic' && shapeType != 'text_bold' &&
				/*shapesQuantity <= 1 &&*/ (!drawEndSelector.isSelector) && (!shapecopy.isSelector) ){
				var itHasText = isInputText(SELECTED);
				if( !itHasText )
					newCornersDD(this);
			}
		}
		if( (drawEndSelector.isSelector && this.group.name in drawEndSelector.SELECTED) ||
				(shapecopy.isSelector && this.group.name in shapecopy.SELECTED) ){
			;
		}else{
			ungroup.changeShapes = true;
			ungroup.newShape = this;
			ungroup();
			for( var x in SELECTED){
				delete SELECTED[x];
			}
			if( checkToMove.last == this.group) {
				var checkBoundaryAuxiliar = checkBoundary.last;
				checkBoundary.last = checkToMove.last;
				checkToMove.last = checkBoundaryAuxiliar;
			}
			if( checkToMove.last ){
				checkToMove.last.removeBoundary();
				checkToMove.last = undefined;
			}
			if( drawEndSelector.SELECTED ){
				for( var nameGroup in drawEndSelector.SELECTED ){
					drawEndSelector.SELECTED[nameGroup].removeBoundary();
					delete drawEndSelector.SELECTED[nameGroup];
				}
				drawEndSelector.isSelector = false;
			}
			if( shapecopy.SELECTED ){
				for( var nameGroup in shapecopy.SELECTED ){
					shapecopy.SELECTED[nameGroup].removeBoundary();
					delete shapecopy.SELECTED[nameGroup];
				}
				shapecopy.isSelector = false;
			}
			SELECTED[this.group.name] = this.group;
		}
		SELECTEDSHAPE = this.group.members[0];
	}
	
	this.boundary.DD.onDrag=function (e)
	{
		noBubble(e);
		
		addTrackFunction("drawboundary","boundary.DD.onDrag");
		if( document.getElementById(this.id) == null ) { 
			self.boundary.DD.onDrag=function(e){};
			return;
		}
		var newX = Math.round(parseInt(document.getElementById(this.id).style.left)/xgrid)*xgrid+"px";
		var newY = Math.round(parseInt(document.getElementById(this.id).style.top)/ygrid)*ygrid+"px";
		var dx=parseInt(newX)-document.getElementById(this.id).group.left;
		var dy=parseInt(newY)-document.getElementById(this.id).group.top;
		justSelectedBoundaries();

		document.getElementById(this.id).style.left=newX;
		document.getElementById(this.id).style.top=newY;
		var boundary;
		for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++)
		{
			boundary=document.getElementById("boundarydrop").childNodes[i];
			boundary.style.left=(boundary.group.left+dx)+"px";
			boundary.style.top=(boundary.group.top+dy)+"px";
		}
		if (TWEENEDIT)
		{
			CURRENTTWEEN.translate.active=true;
			CURRENTTWEEN.setTweenTimeBox();
		}
	}

	this.boundary.DD.onMouseUp=function(e)
	{
		addTrackFunction("drawboundary","boundary.DD.onMouseUp");
		noBubble(e);
		var re;
		// alinear a una posicion valida
		document.getElementById(this.id).style.left = Math.round(parseInt(document.getElementById(this.id).style.left)/xgrid)*xgrid+"px";
		document.getElementById(this.id).style.top = Math.round(parseInt(document.getElementById(this.id).style.top)/ygrid)*ygrid+"px";

		// calcular la diferencia entre la posicion futura y la actual
		var dx = parseInt(document.getElementById(this.id).style.left)-document.getElementById(this.id).group.left;
		var dy = parseInt(document.getElementById(this.id).style.top)-document.getElementById(this.id).group.top;
		// var dx = parseInt(document.getElementById(this.id).style.left)-document.getElementById(this.id).group.left + 5;
		// var dy = parseInt(document.getElementById(this.id).style.top)-document.getElementById(this.id).group.top + 5;

		// left y top del boundary futuro
		var l = parseInt(document.getElementById(this.id).style.left);
		var t = parseInt(document.getElementById(this.id).style.top);
		// right y bottom del grupo
		var r = parseInt(l + document.getElementById(this.id).group.width);
		var b = parseInt(t + document.getElementById(this.id).group.height);

		var verifyExceed = ! ( (l<-100) || (t<-100) || (r>(PNLWIDTH+100)) || (b>(PNLHEIGHT+100)) );

		var shape,node;
		resetBoundary();

		if (checkShapesIn(dx, dy, r, b) && verifyExceed)
		{
			var actions = [];
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				var shapeNames=group.memberShapes();

				if (dx != 0 || dy != 0)
				{
					actions.push(new MoveShapesCommand(shapeNames,group,dx-PNLLEFT,dy-PNLTOP,l,t,1,1, Object.keys(SELECTED).length));
				}
				else {
					var type = '';
					for (var name in shapeNames)
					{
						shape= shapeNames[name];
						type = shape.type;
						node = shape.path.next;
						while (node.point.x!="end")
						{
							node.point.x+=(dx-PNLLEFT);
							node.point.y+=(dy-PNLTOP);
							if (node.ctrl1.x!="non") //changes when straight line set on curve
							{
								node.ctrl1.x+=(dx-PNLLEFT);
								node.ctrl1.y+=(dy-PNLTOP);
								node.ctrl2.x+=(dx-PNLLEFT);
								node.ctrl2.y+=(dy-PNLTOP);
							}
							node=node.next;
						}
						shape.tplftcrnr.x +=(dx-PNLLEFT);
						shape.btmrgtcrnr.x+=(dx-PNLLEFT);
						shape.tplftcrnr.y +=(dy-PNLTOP);
						shape.btmrgtcrnr.y+=(dy-PNLTOP);
						shape.lineGrad[0] +=(dx-PNLLEFT);
						shape.lineGrad[1] +=(dy-PNLTOP);
						shape.lineGrad[2] +=(dx-PNLLEFT);
						shape.lineGrad[3] +=(dy-PNLTOP);
						shape.radGrad[0] +=(dx-PNLLEFT);
						shape.radGrad[1] +=(dy-PNLTOP);
						shape.radGrad[3] +=(dx-PNLLEFT);
						shape.radGrad[4] +=(dy-PNLTOP);
						shape.arccentre.x+=(dx-PNLLEFT);
						shape.arccentre.y+=(dy-PNLTOP);
						shape.draw();
					}
					group.update(l,t,dx-PNLLEFT,dy-PNLTOP,1,1);
					group.drawBoundary(type, Object.keys(SELECTED).length);
				}
			};

			if (actions.length > 0)
			{
				window.UndoRedo.executeCommand(actions);
			}
		}
		else {
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				group.drawBoundary(group.members[0].type, Object.keys(SELECTED).length);
			}
		}
		// to be able to move anothers shapes
		toMoveEvent();
	};
}

/**
* @function checkSelector
* Busca el Shape que coincida con las coordenadas del cursor.
* @param		Bollean		shiftdown		"true: evento key shift activado"
* @param		Object		cursor			"Objeto con las coordenadas del cursor"
* @return		undefined		"No devuelve ningun valor"
*/
function checkSelector(shiftdown,cursor)
{
	addTrackFunction("drawboundary","checkSelector");
	ungroup();
	// remove drawEndSelector shapes boundaries
	if( drawEndSelector.SELECTED ){
		for( var nameGroup in drawEndSelector.SELECTED ){
			drawEndSelector.SELECTED[nameGroup].removeBoundary();
			delete drawEndSelector.SELECTED[nameGroup];
		}
		drawEndSelector.isSelector = false;
	}
	if( shapecopy.SELECTED ){
		for( var nameGroup in shapecopy.SELECTED ){
			shapecopy.SELECTED[nameGroup].removeBoundary();
			delete shapecopy.SELECTED[nameGroup];
		}
		shapecopy.isSelector = false;
	}
	
	if (cursor.x < 0 || cursor.y < 0) return;
	removeGradLine();
	closeStops();
	removeRotate();

	if (!shiftdown)
	{
		SELECTED={};
		BCOUNT=0;
		clear(document.getElementById("markerdrop"));
		resetBoundary();
	}

	var shape, foundshape;
	var shapefound=false;
	for (var name in CURRENT)
	{
		shape=CURRENT[name];
		if ((shape.tplftcrnr.x-4)<=cursor.x && cursor.x<=(shape.btmrgtcrnr.x+4) && (shape.tplftcrnr.y-4)<=cursor.y && cursor.y<=(shape.btmrgtcrnr.y+4))
		{
			if (!shapefound || (shapefound && shape.zIndex>foundshape.zIndex))
			{
				if (shape.open)
				{
					if (shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
				else
				{
					if (shape.isIn(cursor) || shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
			}
		}
	}
	if (shapefound)
	{
		toMoveEvent();
		if (shiftdown)
		{
			if (foundshape.group.name in SELECTED)
			{
				foundshape.group.removeBoundary();
				delete SELECTED[foundshape.group.name];
			}
			else
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				SELECTED[foundshape.group.name]=foundshape.group;
				SELECTEDSHAPE=foundshape;
			}
		}
		else
		{
			if (!(foundshape.group.name in SELECTED)) //if already drawn do not redraw
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				SELECTED[foundshape.group.name]=foundshape.group;
				SELECTEDSHAPE=foundshape;
			}
		}
	}else{
		checkBoundary.last = undefined;
		checkToMove.last = undefined;
		shapeSelector(cursor);
		addMouseEventToSelect();
	}

	var workArea = document.getElementById("workArea");
	fireEvent(workArea,"click");
}

/**
* @function isIn
* Verifica si el clic se dio dentro de un Shape cerrado
* @param		Object			cursor		"Objeto con las coordenadas del cursor"
* @return		Bollean								"true: clic dentro de un shape cerrado"
*/
function isIn(cursor)
{
	addTrackFunction("drawboundary","isIn");
	var crosses=0;
	var steps=10; //number of segments per curve;
	var A,B,P,Q;
	var node=this.path.next;
	node=node.next;
	while (node.point.x!="end")
	{
		if (node.vertex  == 'L')
		{
			A=new Point(node.prev.point.x-cursor.x,node.prev.point.y-cursor.y);
			B=new Point(node.point.x-cursor.x,node.point.y-cursor.y);
			if (A.y==B.y)
				{B.y +=0.01};
			if (A.y==0)
				{A.y +=0.01};
			if (B.y==0)
				{B.y +=0.01};
			if (A.y==B.y)
				{B.y +=0.01};
			updatecrosses(A,B);
		}
		else
		{
			A=new Point(node.prev.point.x-cursor.x,node.prev.point.y-cursor.y);
			B=new Point(node.point.x-cursor.x,node.point.y-cursor.y);
			C=new Point(node.ctrl1.x-cursor.x,node.ctrl1.y-cursor.y);
			D=new Point(node.ctrl2.x-cursor.x,node.ctrl2.y-cursor.y);
			P=new Point(node.prev.point.x-cursor.x,node.prev.point.y-cursor.y);
			Q=new Point(0,0);

			for (var i=1;i<=steps; i++)
			{
				t=i/steps;
				Q.x = (1-t)*(1-t)*(1-t)*A.x + 3*(1-t)*(1-t)*t*C.x + 3*(1-t)*t*t*D.x + t*t*t*B.x;
				Q.y = (1-t)*(1-t)*(1-t)*A.y + 3*(1-t)*(1-t)*t*C.y + 3*(1-t)*t*t*D.y + t*t*t*B.y;
				if (P.y==Q.y)
					{Q.y +=0.01};
				if (P.y==0)
					{P.y +=0.01};
				if (Q.y==0)
					{Q.y +=0.01};
				if (P.y==Q.y)
					{Q.y +=0.01};
				updatecrosses(P,Q);
				P.x=Q.x;
				P.y=Q.y;
			}
		}
		node=node.next;
	}
	if ((crosses % 2)==0)
	{
		return false;
	}
	else
	{
		return true;
	}

	function updatecrosses(P,Q)
	{
		addTrackFunction("drawboundary","updatecrosses");
		if (P.y*Q.y<0)
		{
			if (P.x>0 && Q.x>0)
			{
				crosses +=1;
			}
			else if (P.x*Q.x<0)
			{
				if ((P.y*Q.x-Q.y*P.x)/(P.y-Q.y)>0)
				{
					crosses +=1;
				}
			}
		}
	}
}

/**
* @function checkBoundary
* funcion del evento estado 1
* Busca el Shape que coincida con las coordenadas del curso para dibujar su boundary.
* @param		Bollean		shiftdown		"true: evento key shift activado"
* @param		Object		cursor			"Objeto con las coordenadas del cursor"
* @return		undefined		"No devuelve ningun valor"
*/
function checkBoundary(shiftdown,cursor)
{
	addTrackFunction("drawboundary","checkBoundary");
	ungroup();
	removeGradLine();
	closeStops();
	removeRotate();

	if (!shiftdown)
	{
		SELECTED={};
		BCOUNT=0;
		clear(document.getElementById("markerdrop"));
		resetBoundary();
	}
	var shape, foundshape;
	var shapefound=false;
	for (var name in CURRENT)
	{
		shape=CURRENT[name];
		if ((shape.tplftcrnr.x-4)<=cursor.x && cursor.x<=(shape.btmrgtcrnr.x+4) && (shape.tplftcrnr.y-4)<=cursor.y && cursor.y<=(shape.btmrgtcrnr.y+4))
		{
			if (!shapefound || (shapefound && shape.zIndex>foundshape.zIndex))
			{
				if (shape.open)
				{
					if (shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
				else
				{
					if (shape.isIn(cursor) || shape.isOn(cursor))
					{
						shapefound=true;
						foundshape=shape;
					}
				}
			}
		}
	}
	if (shapefound)
	{
		// if find a shape
		checkBoundary.last = foundshape.group;
		if (shiftdown)
		{
			if (foundshape.group.name in SELECTED)
			{
				foundshape.group.removeBoundary();
				delete SELECTED[foundshape.group.name];
			}
			else
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				SELECTED[foundshape.group.name]=foundshape.group;
				SELECTEDSHAPE=foundshape;
			}
		}
		else
		{
			if (!(foundshape.group.name in SELECTED)) //if already drawn do not redraw
			{
				foundshape.group.drawBoundary(foundshape.type, 1);
				// SELECTED[foundshape.group.name]=foundshape.group;
				// SELECTEDSHAPE=foundshape;
			}
		}
	}
}
// parameter to check if checkBoundary find a shape
// initialize with undefined
checkBoundary.last = undefined;

/**
* @function markLine
* Dibuja los point y ctrl del Shape
* @return		undefined		"No devuelve ningun valor"
*/
function markLine()
{
	addTrackFunction("drawboundary","markLine");
	resetBoundary();
	document.getElementById("markerdrop").style.visibility="visible";
	document.getElementById("backstage").style.visibility="visible";
	document.getElementById("boundarydrop").style.visibility="hidden";
	document.getElementById("frontmarkerdrop").style.visibility="hidden";
	removeGradLine();
	removeRotate();
	var node;
	switch (SELECTEDSHAPE.type)
	{
		case "line":
		case "arc":
		node=SELECTEDSHAPE.path.next;
		node.addPointMark();
		node=SELECTEDSHAPE.path.prev;
		node.addPointMark();
		break;
		case "segment":
		node=SELECTEDSHAPE.path.next;
		node.addPointMark();
		node=SELECTEDSHAPE.path.prev.prev;
		node.addPointMark();
		break;
		case "sector":
		node=SELECTEDSHAPE.path.next;
		node.addPointMark();
		node=SELECTEDSHAPE.path.prev.prev.prev;
		node.addPointMark();
		break;
		case "rounded_rectangle":
		node=SELECTEDSHAPE.path.next;
		node.addPointMark();
		break;
		case "curve":
		case "curve_dotted":
		node=SELECTEDSHAPE.path.next;
		node.addPointMark();
			//break;
			case "freeform":
			case "freeform_dotted":
			node=SELECTEDSHAPE.path.next;
			node=node.next;
			while (node.point.x!="end")
			{
				if (node.vertex=="L")
				{
					node.addPointMark();
				}
				else
				{
					node.addFullMarks();
				}
				node=node.next;
			}
			SELECTEDSHAPE.drawBezGuides();
			break;
		}
	}

	function isOn(cursor)
	{
		addTrackFunction("drawboundary","isOn");
		var ontheline=false;
		var step=10;
		var theta;
	var D=2; //extension distance around line so that on cursor is within D pixels of line
	var sp,ep,ip; //extension to start and end points of line and intersection point;
	var x,y; //current position on line when curve split into small points
	var node=this.path.next;
	node=node.next;

	while (node.point.x!="end")
	{
		if (node.vertex == "L")
		{
			theta=arctan(node.point.y-node.prev.point.y,node.point.x-node.prev.point.x);
			sp=new Point(node.prev.point.x-D*Math.cos(theta),node.prev.point.y-D*Math.sin(theta)); //extend start point
			ep=new Point(node.point.x+D*Math.cos(theta),node.point.y+D*Math.sin(theta)); //extend end point
			ip=intersection(cursor,sp,ep);
			if (ip.inRect(sp,ep))
			{
				if (sqdistance(cursor,ip)<=D*D)
				{
					ontheline=true;
				}
			}
		}
		else
		{
			sp=new Point(node.prev.point.x,node.prev.point.y);
			for (var i=1;i<=step; i++)
			{
				t=i/step;
				x = (1-t)*(1-t)*(1-t)*node.prev.point.x + 3*(1-t)*(1-t)*t*node.ctrl1.x + 3*(1-t)*t*t*node.ctrl2.x + t*t*t*node.point.x;
				y = (1-t)*(1-t)*(1-t)*node.prev.point.y + 3*(1-t)*(1-t)*t*node.ctrl1.y + 3*(1-t)*t*t*node.ctrl2.y + t*t*t*node.point.y;
				ep=new Point(x,y);
				ip=intersection(cursor,sp,ep);
				if (ip.inRect(sp,ep))
				{
					if (sqdistance(cursor,ip)<=D*D)
					{
						ontheline=true;
					}
				}
				sp=new Point(ep.x,ep.y);
			}
		}
		node=node.next;
	}
	return ontheline;
}

/**
* @function intersection
* Intersección perpendicular a la linea sp-ep desde el punto c
* @param		Object			c			"Objeto con la posición del punto c"
* @param		Object			sp		"Objeto con la posición del punto sp"
* @param		Object			ep		"Objeto con la posición del punto ep"
* @return		undefined					"No devuelve ningun valor"
*/
function intersection(c,sp,ep)  //from point c perpendicular intersection of line through sp and ep
{
	addTrackFunction("drawboundary","intersection");
	var k = ((ep.y-sp.y) * (c.x-sp.x) - (ep.x-sp.x) * (c.y-sp.y)) / ((ep.y-sp.y)*(ep.y-sp.y) + (ep.x-sp.x)*(ep.x-sp.x))
	var x = c.x - k * (ep.y-sp.y)
	var y = c.y + k * (ep.x-sp.x);
	var i={x:x, y:y};
	i.inRect=inRect;
	return i;
}

/**
* @function inRect
* Verifica que el objeto sea co-lineal con la linea sp-ep
* @param		Object			sp		"Objeto con la posición del punto sp"
* @param		Object			ep		"Objeto con la posición del punto ep"
* @return		Boolean						"true: es co-lineal"
*/
function inRect(sp,ep)
{
	addTrackFunction("drawboundary","inRect");
	var t=new Point(0,0); //top left corner of rectangle
	var b=new Point(0,0); //bottom right corner of rectangle
	t.x=Math.min(sp.x,ep.x);
	t.y=Math.min(sp.y,ep.y);
	b.x=Math.max(sp.x,ep.x);
	b.y=Math.max(sp.y,ep.y);
	return (t.x<=this.x && this.x<=b.x && t.y<=this.y && this.y<=b.y);
}

/**
* @function sqdistance
* Obtiene la distancia cuadrada entre 2 puntos
* @param		Object		p		"Objeto con la posición del punto p"
* @param		Object		q		"Objeto con la posición del punto q"
* @return		Integer				"distancia cuadrada entre p y q"
*/
function sqdistance(p,q)
{
	addTrackFunction("drawboundary","sqdistance");
	return (p.x-q.x)*(p.x-q.x)+(p.y-q.y)*(p.y-q.y)
}

/**
* @function removeBoundary
* Remueve el boundary del Shape
* @return		undefined					"No devuelve ningun valor"
*/
function removeBoundary()
{
	addTrackFunction("drawboundary","removeBoundary");
	// if (this.boundary)
	if (this.boundary && this.boundary.parentNode)
		{this.boundary.parentNode.removeChild(this.boundary)};
}

/**
* @function checkShapesIn
* Verifica si la nueva posición el shape está dentro de los limites del canvas
* @param		Integer		dx			"Valor equivalente al dezplazamiento horizontal"
* @param		Integer		dy			"Valor equivalente al dezplazamiento vertical"
* @param		Object		right		"valor del limite derecho del boundary del grupo"
* @param		Object		bottom	"valor del limite inferior del boundary del grupo"
* @return		Boolean						"true: se encuentra dentro del canvas"
*/
function checkShapesIn(dx, dy, right = 0, bottom = 0)
{
	addTrackFunction("drawboundary","checkShapesIn");

	// cc, rh, bh
	if (dx + dy == 0)
	{
		return (right - DRAGBORDER > 0) && (bottom - DRAGBORDER > 0);
	}

	// dd
	var shapesIn = true;

	// para cada grupo seleccionado
	for (var groupName in SELECTED)
	{
		shapeNames=SELECTED[groupName].memberShapes();
		// cada figura en ese grupo
		for (var name in shapeNames)
		{
			nodeIn = false;
			node = shapeNames[name].path.next;
			// debe tener al menos un nodo en el canvas
			while (node.point.x!="end")
			{
				if ((node.point.x + dx - DRAGBORDER) >= 0 && (node.point.x + dx + DRAGBORDER) < SCRW
					&& (node.point.y + dy - DRAGBORDER) >= 0 && (node.point.y + dy + DRAGBORDER) < SCRH)
					nodeIn = true;
				node = node.next;
			}
			shapesIn = shapesIn && nodeIn;
		}
	}

	return shapesIn;
}

/**
* @function closeStops
* Dispone las capas frontmarkerdrop, backstage, boundarydrop para continuar la actividad del editor
* @return		undefined						"No devuelve ningun valor"
*/
function closeStops()
{
	addTrackFunction("dialogue","closeStops");
	document.getElementById("frontmarkerdrop").style.visibility="hidden";
	document.getElementById("backstage").style.visibility="hidden";
	document.getElementById("boundarydrop").style.visibility="visible";
}

/**
* @function checkNodeAlert
* Verifica si los nodos del shape están dentro de los limites del canvas
* @return		Boolean		"true: está dentro del canvas"
*/
function checkNodeAlert()
{
	addTrackFunction("drawboundary","checkNodeAlert");

	// cada figura en el canvas
	for (var name in SHAPES)
	{
		node   = SHAPES[name].path.next;
		// debe tener al menos un nodo en el canvas
		while (node.point.x!="end")
		{
			if ((node.point.x - DRAGBORDER< 0) || (node.point.x - DRAGBORDER >= SCRW)
				|| (node.point.y - DRAGBORDER < 0) || (node.point.y - DRAGBORDER >= SCRH))
				return true;
			node = node.next;
		}
	}
	return false;
}

/**
* @function newCornersDD
* FOR CHANGE DIMENSIONS,
* 1. DRAW THE CORNERS RECTANGLES
* 2. events to capture and rescale it
* @param		Shape			self		"shape"
* @return		undefined					"Solo cambia el estado de los corner del shape para poder capturar el evento para reescalar"
*/
function newCornersDD(self){
	addTrackFunction("drawboundary","newCornersDD");
	self.workArea = document.getElementById("workArea");

	// self.group.boundary.cc = new Corner(self.group.boundary);
	// self.group.boundary.cc = new Corner(self.workArea, self.group.boundary);
	self.group.boundary.cc = new Corner(self.group.boundary, self.group.boundary);
	self.group.boundary.cc.DD = new YAHOO.util.DD(self.group.boundary.cc.id);
	// self.group.boundary.rh = new RightH(self.group.boundary);
	// self.group.boundary.rh = new RightH(self.workArea, self.group.boundary);
	self.group.boundary.rh = new RightH(self.group.boundary, self.group.boundary);
	self.group.boundary.rh.DD = new YAHOO.util.DD(self.group.boundary.rh.id);
	// self.group.boundary.bh = new BottomH(self.group.boundary);
	// self.group.boundary.bh = new BottomH(self.workArea, self.group.boundary);
	self.group.boundary.bh = new BottomH(self.group.boundary, self.group.boundary);
	self.group.boundary.bh.DD = new YAHOO.util.DD(self.group.boundary.bh.id);
	self.group.boundary.cc.DD.onDrag =function(e)
	{
		addTrackFunction("drawboundary","boundary.cc.DD.onDrag");
		clearAllMouseEvent();
		noBubble(e);
		removeRotate();
		var boundary=document.getElementById(this.id).parentNode;
		var group=boundary.group;
		justSelectedBoundaries();
		// Evalua la posicion del cursor relativa a la figura e impide que se disminuya el ancho/alto de la figura a menos de 5px
		if (parseInt(document.getElementById(this.id).style.left) > MINSIZE)
		{
			var width=parseInt(document.getElementById(this.id).style.left)+MINSTEP;
			var scale=width/group.width;
			var boundary;
			for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++)
			{
				boundary=document.getElementById("boundarydrop").childNodes[i];
				boundary.style.width=Math.round(boundary.group.width*scale/xgrid)*xgrid+"px";
				boundary.style.height=Math.round(boundary.group.height*scale/ygrid)*ygrid+"px";
				boundary.cc.style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
				boundary.cc.style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
				boundary.rh.style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
				boundary.rh.style.top=(parseInt(boundary.style.height)/2-MINSTEP)+"px";
				boundary.bh.style.left=(parseInt(boundary.style.width)/2-MINSTEP)+"px";
				boundary.bh.style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
			}
		}
		else
		{
			document.getElementById(this.id).style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
			document.getElementById(this.id).style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
		}

		if (TWEENEDIT)
		{
			CURRENTTWEEN.nodeTweening.active=true;
			CURRENTTWEEN.pointTweening=true;
			CURRENTTWEEN.setTweenTimeBox();
		}
	};
	self.group.boundary.cc.DD.onMouseUp=function(e)
	{
		addTrackFunction("drawboundary","boundary.cc.DD.onMouseUp");
		toMoveEvent();
		noBubble(e);
		var boundary = document.getElementById(this.id).parentNode;
		var group = boundary.group;
		var scale = parseInt(boundary.style.width)/group.width;
		if( scale == 1 ) return;
		var l = parseInt(boundary.style.left);
		var t = parseInt(boundary.style.top);
		var r = l + parseInt(boundary.style.width);
		var b = t + parseInt(boundary.style.height);
		var verifyExceed = ! ( (l<-100) || (t<-100) || (r>(PNLWIDTH+100)) || (b>(PNLHEIGHT+100)) );

		if (checkShapesIn(0, 0, r, b) && verifyExceed)
		{
			var actions = [];
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				var shapeNames=group.memberShapes();
				actions.push(new ccShapesCommand(shapeNames,group,l,t,scale));
			}
			if (actions.length > 0 ){
				window.UndoRedo.executeCommand(actions);
			}
		}
		else {
			resetBoundary();
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				group.drawBoundary(group.type,Object.keys(SELECTED).length);
			}
		}
	}

	self.group.boundary.rh.DD.onDrag =function(e)
	{
		addTrackFunction("drawboundary","boundary.rh.DD.onDrag");
		clearAllMouseEvent();
		noBubble(e);
		removeRotate();
		var boundary=document.getElementById(this.id).parentNode;
		var group=boundary.group;
		justSelectedBoundaries();
		if (parseInt(document.getElementById(this.id).style.left) > MINSIZE)
		{
			var width=parseInt(document.getElementById(this.id).style.left)+MINSTEP;
			var scale=width/group.width;
			var boundary;
			for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++)
			{
				boundary=document.getElementById("boundarydrop").childNodes[i];
				boundary.style.width=Math.round(boundary.group.width*scale/xgrid)*xgrid+"px";
				boundary.cc.style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
				boundary.rh.style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
				boundary.rh.style.top=(parseInt(boundary.style.height)/2-MINSTEP)+"px";
				boundary.bh.style.left=(parseInt(boundary.style.width)/2-MINSTEP)+"px";
			}
		}
		else
		{
			document.getElementById(this.id).style.left=(parseInt(boundary.style.width)-MINSTEP)+"px";
		}
		if (TWEENEDIT)
		{
			CURRENTTWEEN.nodeTweening.active=true;
			CURRENTTWEEN.pointTweening=true;
			CURRENTTWEEN.setTweenTimeBox();
		}
	};

	self.group.boundary.rh.DD.onMouseUp=function(e)
	{
		addTrackFunction("drawboundary","boundary.rh.DD.onMouseUp");
		toMoveEvent();
		noBubble(e);
		var boundary = document.getElementById(this.id).parentNode;
		var group = boundary.group;
		var scale = parseInt(boundary.style.width)/group.width;
		if( scale == 1 ) return;
		var l = parseInt(boundary.style.left);
		var t = parseInt(boundary.style.top);
		var r = l + parseInt(boundary.style.width);
		var b = t + parseInt(boundary.style.height);
		var verifyExceed = ! ( (l<-100) || (t<-100) || (r>(PNLWIDTH+100)) || (b>(PNLHEIGHT+100)) );

		if (checkShapesIn(0, 0, r, b) && verifyExceed)
		{
			var actions = [];
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				var shapeNames=group.memberShapes();
				actions.push(new rhShapesCommand(shapeNames,group,l,t,scale));
			}
			if (actions.length > 0 ){
				window.UndoRedo.executeCommand(actions);
			}
		}
		else {
			resetBoundary();
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				group.drawBoundary(group.type, Object.keys(SELECTED).length);
			}
		}
	}

	self.group.boundary.bh.DD.onDrag =function(e)
	{
		addTrackFunction("drawboundary","boundary.bh.DD.onDrag");
		clearAllMouseEvent();
		noBubble(e);
		removeRotate();
		var boundary=document.getElementById(this.id).parentNode;
		var group=boundary.group;
		justSelectedBoundaries();
		if (parseInt(document.getElementById(this.id).style.top) > MINSIZE)
		{
			var height=parseInt(document.getElementById(this.id).style.top)+MINSTEP;
			var scale=height/group.height;
			var boundary;
			for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++)
			{
				boundary=document.getElementById("boundarydrop").childNodes[i];
				boundary.style.height=Math.round(boundary.group.height*scale/ygrid)*ygrid+"px";
				boundary.cc.style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
				boundary.rh.style.top=(parseInt(boundary.style.height)/2-MINSTEP)+"px";
				boundary.bh.style.left=(parseInt(boundary.style.width)/2-MINSTEP)+"px";
				boundary.bh.style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
			}
		}
		else
		{
			document.getElementById(this.id).style.top=(parseInt(boundary.style.height)-MINSTEP)+"px";
		}
		if (TWEENEDIT)
		{
			CURRENTTWEEN.nodeTweening.active=true;
			CURRENTTWEEN.pointTweening=true;
			CURRENTTWEEN.setTweenTimeBox();
		}
	};

	self.group.boundary.bh.DD.onMouseUp=function(e)
	{
		addTrackFunction("drawboundary","boundary.bh.DD.onMouseUp");
		toMoveEvent();
		noBubble(e);
		var boundary = document.getElementById(this.id).parentNode;
		var group = boundary.group;
		var scale = parseInt(boundary.style.height)/group.height;
		if( scale == 1 ) return;
		var l = parseInt(boundary.style.left);
		var t = parseInt(boundary.style.top);
		var r = l + parseInt(boundary.style.width);
		var b = t + parseInt(boundary.style.height);
		var verifyExceed = ! ( (l<-100) || (t<-100) || (r>(PNLWIDTH+100)) || (b>(PNLHEIGHT+100)) );

		if (checkShapesIn(0, 0, r, b) && verifyExceed)
		{
			var actions = [];
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				var shapeNames=group.memberShapes();
				actions.push(new bhShapesCommand(shapeNames,group,l,t,scale));
			}
			if (actions.length > 0 ){
				window.UndoRedo.executeCommand(actions);
			}
		}
		else {
			resetBoundary();
			for (var groupName in SELECTED)
			{
				var group=SELECTED[groupName];
				group.drawBoundary(group.type, Object.keys(SELECTED).length);
			}
		}
	}
}

/**
* @function justSelectedBoundaries
* It checks if a boundary div is selected or not
* In case is not selected, remove it
* its really important is used in a several functions to resize shapes
* @return		undefined						"No devuelve ningun valor"
*/
function justSelectedBoundaries(){
	addTrackFunction("drawboundary","justSelectedBoundaries");
	for (var i=0; i<document.getElementById("boundarydrop").childNodes.length; i++) {
		boundary=document.getElementById("boundarydrop").childNodes[i];
	
		if( 
			! ( boundary.group.name in SELECTED )  ){
			boundary.group.removeBoundary();
		}
	}
}


/**
* @function justSelectedBoundaries
* funcion adaptada de MoveShapesCommand.prototype.execute para que sea posible
* mover con el teclado down, up, left, right
* @param		Integer			dx		"valor x a mover"
* @param		Integer			dy		"valor y a mover"
* @return		undefined						"No devuelve ningun valor"
*/
function moveShapes(dx, dy){
	var type = '';
	var quantity = Object.keys(SELECTED).length;
	var canItMove = true;
	for(var name in SELECTED)
	{
		var group = SELECTED[name];
		
		var boundary = group.boundary;
		var l = parseInt(boundary.style.left) + dx;
		var t = parseInt(boundary.style.top) + dy;
		var r = l + parseInt(boundary.style.width) + dx;
		var b = t + parseInt(boundary.style.height) + dy;
		var verifyExceed = ! ( (l<-100) || (t<-100) || (r>(PNLWIDTH+100)) || (b>(PNLHEIGHT+100)) );

		if( !( checkShapesIn(dx, dy, r, b) && verifyExceed) ){
			canItMove = false;
			return;
		}
	}

	// after verify then move
	resetBoundary();
	for(var name in SELECTED)
	{
		var group = SELECTED[name];	

		var memberShapes = group.memberShapes();
		for(var nameShape in memberShapes){
			shape=memberShapes[nameShape];
			type = shape.type;
			node=shape.path.next;
			while(node.point.x!="end")
			{
				node.point.x+= dx;
				node.point.y+= dy;
				if(node.ctrl1.x!="non") //changes when straight line set on curve
				{
					node.ctrl1.x+= dx;
					node.ctrl1.y+= dy;
					node.ctrl2.x+= dx;
					node.ctrl2.y+= dy;
				}
				node=node.next;
			}
			shape.tplftcrnr.x += dx;
			shape.btmrgtcrnr.x+= dx;
			shape.tplftcrnr.y += dy;
			shape.btmrgtcrnr.y+= dy;
			shape.lineGrad[0] += dx;
			shape.lineGrad[1] += dy;
			shape.lineGrad[2] += dx;
			shape.lineGrad[3] += dy;
			shape.radGrad[0] += dx;
			shape.radGrad[1] += dy;
			shape.radGrad[3] += dx;
			shape.radGrad[4] += dy;
			shape.arccentre.x+= dx;
			shape.arccentre.y+= dy;
			shape.draw();
		}
		var l = parseInt(group.boundary.style.left);
		var t = parseInt(group.boundary.style.top)
		group.update(l, t, dx, dy, 1, 1);
		group.drawBoundary(type, quantity);
	}
	fireAlert();
}